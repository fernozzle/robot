#pragma config(Hubs,  S1, HTMotor,  HTMotor,  none,     none)
#pragma config(Hubs,  S2, HTServo,  HTServo,  HTMotor,  none)
#pragma config(Sensor, S1,     ,               sensorI2CMuxController)
#pragma config(Sensor, S2,     ,               sensorI2CMuxController)
#pragma config(Sensor, S3,     HTMUX1,         sensorI2CCustom)
#pragma config(Sensor, S4,     HTMUX2,         sensorI2CCustom)
#pragma config(Motor,  mtr_S1_C1_1,     spinnerMotor,  tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S1_C1_2,     rightMotor,    tmotorTetrix, PIDControl, reversed, encoder)
#pragma config(Motor,  mtr_S1_C2_1,     liftMotor,     tmotorTetrix, PIDControl, reversed, encoder)
#pragma config(Motor,  mtr_S1_C2_2,     leftMotor,     tmotorTetrix, PIDControl, encoder)
#pragma config(Motor,  mtr_S2_C3_1,     brushMotor,    tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S2_C3_2,     motorI,        tmotorTetrix, openLoop)
#pragma config(Servo,  srvo_S2_C1_1,    rightSpinServo,       tServoStandard)
#pragma config(Servo,  srvo_S2_C1_2,    leftSpinServo,        tServoStandard)
#pragma config(Servo,  srvo_S2_C1_3,    servo3,               tServoNone)
#pragma config(Servo,  srvo_S2_C1_4,    servo4,               tServoNone)
#pragma config(Servo,  srvo_S2_C1_5,    servo5,               tServoNone)
#pragma config(Servo,  srvo_S2_C1_6,    servo6,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_1,    servo7,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_2,    servo8,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_3,    servo9,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_4,    servo10,              tServoNone)
#pragma config(Servo,  srvo_S2_C2_5,    servo11,              tServoNone)
#pragma config(Servo,  srvo_S2_C2_6,    scoopDumpServo,       tServoStandard)

#include "JoystickDriver.c"  //Include file to "handle" the Bluetooth messages.
#include "drivers/common.h"
#include "drivers/hitechnic-sensormux.h"
#include "drivers/hitechnic-irseeker-v2.h"
#include "drivers/hitechnic-compass.h"
#include "drivers/lego-touch.h"
#include "drivers/lego-light.h"
#include "drivers/lego-ultrasound.h"
//////
//	Mux sensors
//  MUX 1 is connected to S3
//
// Left IR Sensor is connected to 1st port of the MUX
const tMUXSensor HTRightIRS = msensor_S3_1;
//	Ultrasonic sensor is connected to the 2nd port of the MUX
const tMUXSensor HTUltra = msensor_S3_2;
//////
//	Mux sensors
//  MUX 2 is connected to S4
//
// Left IR Sensor is connected to 1st port of the MUX
const tMUXSensor HTLeftIRS = msensor_S4_1;
// Compass sensor is connected to the 2nd port of the MUX
const tMUXSensor HTCompass = msensor_S4_2;
// Light sensor is connected to the 3rd port of the MUX
const tMUXSensor HTLight = msensor_S4_3;

void TurnRight(int Power, int Angle, int MaxTime){
	int startAngle = HTMCreadHeading(HTCompass);
	int destAngle = startAngle + Angle;

  ClearTimer(T1);

  int passTimes = 0;
	int curAngle = startAngle;
	while (passTImes < 10) {
		curAngle = HTMCreadHeading(HTCompass);
		if (curAngle < startAngle - 10) {
			curAngle += 360;	// if the compass "looped over", offset it so we treat it as linear
		}
    if (curAngle > destAngle) {
    	passTimes++;
    } else {
      passTImes = 0;
    }
		writeDebugStreamLine("we're going right. I'm at %d. I'm going to %d.", curAngle, destAngle);
		//motor[rightMotor] = -Power;
		//motor[leftMotor] = Power;
      if (false & (time1[T1] > MaxTime)){
           break;
      }
	}
	writeDebugStreamLine("yay I'm done going right.");

  //StopDriveMotors(); //turn both motors off
}
void TurnLeft(int Power, int Angle, int MaxTime){
	int startAngle = HTMCreadHeading(HTCompass);
	int destAngle = startAngle - Angle;

  ClearTimer(T1);

  int passTimes = 0;
	int curAngle = startAngle;
	while (passTImes < 10) {
		curAngle = HTMCreadHeading(HTCompass);
		if (curAngle > startAngle + 10) {
			curAngle -= 360;	// if the compass "looped over", offset it so we treat it as linear
		}
    if (curAngle < destAngle) {
    	passTimes++;
    } else {
      passTImes = 0;
    }
		writeDebugStreamLine("we're going left. I'm at %d. I'm going to %d.", curAngle, destAngle);
		//motor[rightMotor] = -Power;
		//motor[leftMotor] = Power;
      if (false & (time1[T1] > MaxTime)){
           break;
      }
	}
	writeDebugStreamLine("yay I'm done going left.");

  //StopDriveMotors(); //turn both motors off
}

task main()
{
	while (true) {
		int lightRaw = LSvalRaw(HTLight);	// raw value of light sensor
		int lightNorm = LSvalNorm(HTLight);	// normalized value of light sensor
		writeDebugStreamLine("LRaw:%4d N:%4d",lightRaw,lightNorm);
	}
}
