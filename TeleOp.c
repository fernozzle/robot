#pragma config(Hubs,  S1, HTMotor,  HTMotor,  none,     none)
#pragma config(Sensor, S1,     ,               sensorI2CMuxController)
#pragma config(Sensor, S2,     gyro,           sensorI2CHiTechnicGyro)
#pragma config(Sensor, S3,     light,          sensorLightActive)
#pragma config(Sensor, S4,     sonar,          sensorSONAR)
#pragma config(Motor,  mtr_S1_C1_1,     rightMotor,    tmotorTetrix, openLoop, reversed, encoder)
#pragma config(Motor,  mtr_S1_C1_2,     leftMotor,     tmotorTetrix, openLoop, encoder)
#pragma config(Motor,  mtr_S1_C2_1,     armMotor,      tmotorTetrix, openLoop, encoder)
#pragma config(Motor,  mtr_S1_C2_2,     intakeMotor,   tmotorTetrix, openLoop, encoder)

task main(){

	nMotorEncoder(rightMotor) = 0;
	nMotorEncoder(leftMotor)  = 0;

	// turn clockwise to be parallel to the boxes
	while (nMotorEncoder(leftMotor) > -700) {
		motor[rightMotor] = -40;
		motor[leftMotor] = 40;
	}

	// move forward until IR seeker points to left (region ___)
	while (true) {
		motor[leftMotor] = 50;
		motor[rightMotor] = 50;


	}

}
