#pragma config(Hubs,  S1, HTMotor,  HTMotor,  none,     none)
#pragma config(Sensor, S1,     ,               sensorI2CMuxController)
#pragma config(Sensor, S2,     IRSeeker,       sensorHiTechnicIRSeeker1200)
#pragma config(Sensor, S3,     light,          sensorLightActive)
#pragma config(Sensor, S4,     sonar,          sensorSONAR)
#pragma config(Motor,  motorA,          gripperMotor,  tmotorNXT, PIDControl, encoder)
#pragma config(Motor,  mtr_S1_C1_1,     rightMotor,    tmotorTetrix, openLoop, encoder)
#pragma config(Motor,  mtr_S1_C1_2,     leftMotor,     tmotorTetrix, openLoop, reversed, encoder)
#pragma config(Motor,  mtr_S1_C2_1,     armMotor,      tmotorTetrix, openLoop, encoder)
#pragma config(Motor,  mtr_S1_C2_2,     spinnerMotor,  tmotorTetrix, openLoop, encoder)
//#pragma config(StandardModel, "RVW REMBOT")
#pragma debuggerWindows("joystickSimple")
#include "JoyStickDriver.c"
task main(){
	nMotorEncoder[leftMotor] = 0;
	nMotorEncoder[rightMotor] = 0;
	bool finishedPlacingBlock = false;
	while(true){
		getJoystickSettings(joystick);
		//motor[leftMotor]    = joystick.joy1_y1 + joystick.joy1_x1;
		//motor[rightMotor]   = joystick.joy1_y1 - joystick.joy1_x1
		short q = SensorValue(sonar);
		short releaseDist = 27;
		short throwDist = 39;
		if(abs(q-throwDist) < 10){
			motor[armMotor]     = 100;
			motor[gripperMotor] = 0;
		}else if(q < releaseDist){
			motor[armMotor]     = -100;
				motor[gripperMotor] = -127;
				finishedPlacingBlock = true;
		}else if(q < throwDist){
			motor[armMotor]     = -100;
				motor[gripperMotor] = 0;
		}else{
			motor[armMotor]     = 0;
			motor[gripperMotor] = 0;
		}
		//motor[armMotor]     = ((joystick.joy1_Buttons & 0b100000) * 2 - 1) * 127;
		//motor[gripperMotor] = (joystick.joy1_Buttons & 0b100000) ? 127 : -128;
		//motor[spinnerMotor] = (joystick.joy1_Buttons & 0b10000) * 127;

		short armPosition = nMotorEncoder[armMotor];

	//	bool shouldOpen = !(joystick.joy1_Buttons & 0b100000) && (100 < armPosition && armPosition < 900);
		//motor[gripperMotor] = shouldOpen ? -127 : 127;
		short t = SensorValue(IRSeeker);
		if(!finishedPlacingBlock){
			if(t == 4){
				motor[leftMotor]    = 60;
				motor[rightMotor]   = 60;
			}else if(t >= 1 && t <= 3){
				motor[leftMotor]    = 0;
				motor[rightMotor]   = 60;
			}else if(t >= 5 && t <= 9){
				motor[leftMotor]    = 60;
				motor[rightMotor]   = 0;
			}else if(t == 0){
				motor[leftMotor]    = -50;
				motor[rightMotor]   = 50;
			}
		}else{
			motor[leftMotor] = -60;
			motor[rightMotor] = -60;
		}
	}
}
