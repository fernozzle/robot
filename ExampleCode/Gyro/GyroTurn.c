/////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                    GyroTurn
//
// This is a generic movement routine turns the robot a specific number of degrees using a gyro
// sensor to measure angular velocity
//
/////////////////////////////////////////////////////////////////////////////////////////////////////
void GyroTurn(int vspeed, int vdegrees, cmdState cmd)
  {
    float vcurrposition = 0;
    int vprevtime = nPgmTime;
    int vcurrtime;
    float vcurrRate;
    int voffset;
    float deltaSecs;
    float degChange;

    if (cmd == LEFT)
    {
      motor[DriveLeft]  = -1*vspeed;
      motor[DriveRight] = vspeed;
      voffset = -1;
    }
    else
    {
        motor[DriveLeft]  = vspeed;
      motor[DriveRight] = -1*vspeed;
      voffset = 1;
    }

    while (vcurrposition < vdegrees)
    {
      // This tells us the current rate of rotation in degrees per
      // second.
      vcurrRate = HTGYROreadRot(gyro)*voffset;

      // How much time has elapsed since we last checked, which we use
      // to determine how far we've turned
      vcurrtime = nPgmTime;

      deltaSecs = (vcurrtime - vprevtime) / 1000.0;
      if (deltaSecs < 0)
      {
        deltaSecs = ((float)((vcurrtime + 1024) - (vprevtime + 1024))) / 1000.0;
      }

      // Calculate how many degrees the heading changed.
      degChange = vcurrRate * deltaSecs;
      vcurrposition = vcurrposition + degChange;
                        vprevtime = vcurrtime;
     }

    motor[DriveLeft]  = 0;
    motor[DriveRight] = 0;

    //The following line is used to pause the robot in between movements
    wait1Msec(100);
  }
