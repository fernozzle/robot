//    _        _             ____       _
//   / \   ___| |_ _ __ ___ |  _ \ _ __(_)_   _____
//  / _ \ / __| __| '__/ _ \| | | | '__| \ \ / / _ \
// / ___ \\__ \ |_| | | (_) | |_| | |  | |\ V /  __/
///_/   \_\___/\__|_|  \___/|____/|_|  |_| \_/ \___|
//
//PROGRAM NAME: AstroDriveUtility2013.c
//VERSION: 1.0
//UPDATED: Feb. 23, 2014
//ORIGINAL UPLOAD SITE: ftc3409.org
//
// Copyright 2014 Matt Pugsley and FTC team 3409 (The Astromechs)
// This code is free to use.  If you use this code in your program
// please make sure to use it in it's entirety.  If used in pieces,
// please cite me as a contributor to your code.
//
/********************************************************************\
WHAT IS THE ASTRODRIVE?

The AstroDrive is intended to help FTC teams reduce the effects of:
1. Damage caused to motors by disconnections between the Samantha
module and the field control system during the teleop portion of
competition runs.
2. Lag on the NXT caused by the processor having to translate motor
commands to I2C communications.

HOW DOES THE ASTRODRIVE WORK?
1.	The AstroDrive montitors the messages recieved by the joystick
driver from the field control system. When it detects a large
delay or disconnection then it will safely shut down any
registered motors.
2.	The AstroDrive reduces the number of I2C messages sent to motor
controllers. It does this by normalizing the power of the motors
and only sending commands to the controllers when the desired
power changes.

\********************************************************************/

/***************************How To Use***************************\
This program was written as a utility for other programs to reduce lag
and preform unique functions. This program WILL have errors at the
beginning when you try to compile it in RobotC because it doesn't
have a task main. To use the library of functions you should put
'#include "AstroDriveUtility2013.c";' at the begining of your program
(remember to compensate for the file's location in reference to your
program). When you import AstroDrive, your program will no longer need
to have '#include "JoystickDriver.c";' at the beginning.
\****************************************************************/

#include "JoystickDriver.c";

int normalizedPower = 0;
int m_motorPower[35];
int failSafeMotors[35];
int failSafeMotorsIndex = 0;
int _messageCount = 0;
int FSThreshold = 500;

task automaticFailSafe();


/**
* initializeAstroDrive: Run the initializeAstroDrive function to set up many
* of the aspects of the Astromech's drive utility all in one place.
*
* @param useAutoFailSafe boolean to decide whether to use the automatic fail safe
* @param useDiagnostics boolean to turn on or off the default dianostics
* 				display included by "JoystickDriver.c"
* @param failSafeThreshold integer in milliseconds of the threshold of the fail
*				safe (default = 500)
*/
void initializeAstroDrive(bool useAutoFailSafe, bool useDiagnostics, int failSafeThreshold){


	if(useAutoFailSafe){
		StartTask(automaticFailSafe);
	}
	if(!useDiagnostics){
		disableDiagnosticsDisplay();
		eraseDisplay();
	}
	FSThreshold = failSafeThreshold;
}

/**
* addFailSafeMotor: Adds motors to the list of motors to shutoff if the fail
* safe is engaged.
*
* @param thisMotor Pass the motor name that you set up in the pragmas of your
*				program
*/
void addFailSafeMotor(int thisMotor){
	failSafeMotors[failSafeMotorsIndex] = thisMotor;
	failSafeMotorsIndex++;
}

/**
* checkFailSafe: Checks the fail safe once to see if the last message was recieved
* more than the threshold number of milliseconds ago.
*/
void checkFailSafe (){
	if(_messageCount==0){
		ClearTimer(T4);
		_messageCount = ntotalMessageCount;
	}
	else{
		getJoystickSettings(joystick);
		if(ntotalMessageCount==_messageCount){

			if(time1[T4] > FSThreshold){

				for(int i=0; i<= failSafeMotorsIndex; i++){

					motor[failSafeMotors[i]] = 0;
				}
			}
		}
		else{
			ClearTimer(T4);
		}
		_messageCount = ntotalMessageCount;
	}
}

/**
* automaticFailSafe: Automatically checks the fail safe to see if the last
* message was recieved more than the threshold number of milliseconds ago.
* It aborts its time slice when it is finished so that it takes effectively
* nothing from the speed of the main program
*/
task automaticFailSafe(){
	_messageCount = 0;
	while(true){
		if(_messageCount==0){
			ClearTimer(T4);
			_messageCount = ntotalMessageCount;
		}
		else{
			getJoystickSettings(joystick);
			if(ntotalMessageCount==_messageCount){

				if(time1[T4] > FSThreshold){

					for(int i=0; i<= failSafeMotorsIndex; i++){

						motor[failSafeMotors[i]] = 0;

					}

				}
			}
			else{
				ClearTimer(T4);
			}
			_messageCount = ntotalMessageCount;
		}
		abortTimeslice();
	}
}

/**
* normalizeMotorPower: Normalizes a motor power to a set interval and tries to
* reduce lag by not setting the power again if it doesn't have to. This function
* is best used for setting the powers of motors in a teleop program because it
* allows finite power control even from a human driver.
*
* @param thisMotor motor to power (pass the name you set in the pragmas for the
*				motor)
* @param motorPower integer of the power you want to set the motor to (-100 to
* 				100)
* @param normalizeInterval positive integer that sets the resolution of the
*				normalization power (lower numbers mean higher resolutions)
* @param maxPower positive integer that is the absolute value of the maximum
*				higher or lower from zero the power can be set to
*/
void normalizeMotorPower (int thisMotor, int motorPower, int normalizeInterval, int maxPower){
	if(motorPower > maxPower){
		motorPower = maxPower;
	}
	if(motorPower < -maxPower){
		motorPower = -maxPower;
	}
	normalizedPower = (((abs(motorPower)+(normalizeInterval/2))/normalizeInterval)*normalizeInterval)*(motorPower/abs(motorPower));

	if(m_motorPower[thisMotor] != normalizedPower){

		motor[thisMotor] = normalizedPower;
		m_motorPower[thisMotor] = normalizedPower;

	}

}

/**
* normalizeMotorPower: Basic version of the full normalizeMotorPower function.
*
* @param thisMotor motor to power (pass the name you set in the pragmas for
*  			the motor)
* @param motorPower integer of the power you want to set the motor to (-100 to
*				100)
*/
void normalizeMotorPower (int thisMotor, int motorPower){
	normalizeMotorPower(thisMotor,motorPower,25, 100);
}

/**
* setMotorPosition: Moves a motor in the direction of a set encoder position.
* By calling this function over and over you can hold a motor in place even if
* it is moved by outside forces. TETRIX motors must have an encoder on them and
* be wired correctly to work (nxt motors work by default).
*
* @param thisMotor motor to power (pass the name you set in the pragmas for
*				the motor)
* @param motorPower power to set the motor to when moving to the set position
* @param targetPosition integer that sets the position that you want the motor
*				to move towards
* @param deadBand integer that sets the amount the motor encoder can be away
*				from the target position to not move
*/
void setMotorPosition (int thisMotor, int motorPower, int targetPosition, int deadBand){
	if(nMotorEncoder[thisMotor] < targetPosition-deadBand){

		normalizeMotorPower(thisMotor, motorPower, 25, 100);

		}else if(nMotorEncoder[thisMotor] > targetPosition+deadBand){

		normalizeMotorPower(thisMotor, -motorPower, 25, 100);

		}else{
		normalizeMotorPower(thisMotor, 0);
	}
}
