#pragma config(Hubs,  S1, HTMotor,  HTMotor,  none,     none)
#pragma config(Hubs,  S2, HTServo,  HTServo,  HTMotor,  none)
#pragma config(Sensor, S1,     ,               sensorI2CMuxController)
#pragma config(Sensor, S2,     ,               sensorI2CMuxController)
#pragma config(Sensor, S3,     HTMUX1,         sensorI2CCustom)
#pragma config(Sensor, S4,     HTMUX2,         sensorI2CCustom)
#pragma config(Motor,  mtr_S1_C1_1,     spinnerMotor,  tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S1_C1_2,     rightMotor,    tmotorTetrix, PIDControl, reversed, encoder)
#pragma config(Motor,  mtr_S1_C2_1,     liftMotor,     tmotorTetrix, PIDControl, reversed, encoder)
#pragma config(Motor,  mtr_S1_C2_2,     leftMotor,     tmotorTetrix, PIDControl, encoder)
#pragma config(Motor,  mtr_S2_C3_1,     brushMotor,    tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S2_C3_2,     motorI,        tmotorTetrix, openLoop)
#pragma config(Servo,  srvo_S2_C1_1,    rightSpinServo,       tServoStandard)
#pragma config(Servo,  srvo_S2_C1_2,    leftSpinServo,        tServoStandard)
#pragma config(Servo,  srvo_S2_C1_3,    servo3,               tServoNone)
#pragma config(Servo,  srvo_S2_C1_4,    servo4,               tServoNone)
#pragma config(Servo,  srvo_S2_C1_5,    servo5,               tServoNone)
#pragma config(Servo,  srvo_S2_C1_6,    servo6,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_1,    servo7,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_2,    servo8,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_3,    servo9,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_4,    servo10,              tServoNone)
#pragma config(Servo,  srvo_S2_C2_5,    servo11,              tServoNone)
#pragma config(Servo,  srvo_S2_C2_6,    scoopDumpServo,       tServoStandard)
#include "JoystickDriver.c"  //Include file to "handle" the Bluetooth messages.
#include "drivers/common.h"
#include "drivers/hitechnic-sensormux.h"
#include "drivers/hitechnic-irseeker-v2.h"
#include "drivers/hitechnic-compass.h"
#include "drivers/lego-touch.h"
#include "drivers/lego-light.h"
#include "drivers/lego-ultrasound.h"

//////
//	Mux sensors
//  MUX 1 is connected to S3
//
// Left IR Sensor is connected to 1st port of the MUX
const tMUXSensor HTRightIRS = msensor_S3_1;
//	Ultrasonic sensor is connected to the 2nd port of the MUX
const tMUXSensor HTUltra = msensor_S3_2;
//////
//	Mux sensors
//  MUX 2 is connected to S4
//
// Left IR Sensor is connected to 1st port of the MUX
const tMUXSensor HTLeftIRS = msensor_S4_1;
// Compass sensor is connected to the 2nd port of the MUX
const tMUXSensor HTCompass = msensor_S4_2;
// Light sensor is connected to the 3rd port of the MUX
const tMUXSensor HTLight = msensor_S4_3;


//THIS PROGRAM IS FOR WHEN THE BASKETS ARE TO THE RIGHT OF THE ROBOT FROM THE ROBOT'S PERSPECTIVE
	//          |_|   |
	//          |_| <-----BASKETS! (they are to the right)
	//          |_|   |
	//          |_|___|__
	//     ^
	//     |
	//   START

//
const int spinServoStartPos = 127;	// start both servos at the mid point - this is the stowed position
const int leftSpinServoDeployPos = 255;	// left spin servo value when deployed
const int rightSpinServoDeployPos = 0;		// right spin servo value when deployed
const int scoopDumpServerDownPos = 60;	// scoop dump servo down position (start here)
const int scoopDumpServoUpPos = 210;		// scoop dump servo dump position
const int scoopAtBasketPos = 17700;
const int scoopMaxPos = 21521;

bool bUseTimer = false;	// Timer Flag - turn this to true to limit movement to timer
int leftMotorEncoderPos = 0;	// Motor Data - so we can see what these are during debugging
int rightMotorEncoderPos = 0;
int releaseBlockDistance = 40;
float angleUnitToDegreesConstant = 24.4444444444;
int turningWiggleRoom = 90; // how far the robot can turn left if it plans to turn right, wighout looping and thinking it's done
int turningMinPassTimes = 1; // how many times must a robot confirm that it's done turning
const int turningSlowDownThreshold = 10;

float brushSpeed = 1.0;

const float degreesToDuration = 17.26;
const int turningSlowDownThresholdDuration = 200;

void SetMotorEncoderPos(){
	leftMotorEncoderPos = nMotorEncoder[leftMotor];
	rightMotorEncoderPos = nMotorEncoder[rightMotor];
}

void ResetDriveMotorEncoders(){
	nMotorEncoder[leftMotor] = 0;  // reset the motor encoder positions
  nMotorEncoder[rightMotor] = 0;
  SetMotorEncoderPos();
}

int RelativeMotorEncoderPos(int motorID, int startPos){
	return nMotorEncoder[motorID] - startPos;
}
void StopDriveMotors(){
	motor[leftMotor] = 0;	// stop drive motors
	motor[rightMotor] = 0;
}
void DriveForward(int Power, int Duration, int MaxTime, bool testGiraffe){
   ClearTimer(T1);
   int leftMotorEncoderStart = nMotorEncoder[leftMotor];	// save starting position
   int rightMotorEncoderStart = nMotorEncoder[rightMotor];

   while ((RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) < Duration) ||
     			(RelativeMotorEncoderPos(leftMotor,leftMotorEncoderStart) < Duration)){
     	writeDebugStreamLine("Left motor encoder relative pos: %d \\ Right motor encoder relative pos: %d", nMotorEncoder[leftMotor] - leftMotorEncoderStart, nMotorEncoder[rightMotor] - rightMotorEncoderStart);
			SetMotorEncoderPos();
       if (bUseTimer & (time1[T1] > MaxTime)){
          break;
       }
       motor[rightMotor] = Power; // Right motor is reversed. Apply power
       motor[leftMotor] = Power;
       /*if (testGiraffe) {
         int liftMotorEncoderValue = nMotorEncoder[liftMotor];
					if (liftMotorEncoderValue < 1000) {
						motor[liftMotor] =  -50;
					} else if (liftMotorEncoderValue <= 0) {
				  	motor[liftMotor] = 0;
				  	testGiraffe = false;
					} else {
						motor[liftMotor] = -100;
					}
     	 }*/
   }
   StopDriveMotors(); //turn both motors off
}
void DriveBack(int Power, int Duration, int MaxTime)
{
   ClearTimer(T1);
   int leftMotorEncoderStart = nMotorEncoder[leftMotor];	// save starting position
   int rightMotorEncoderStart = nMotorEncoder[rightMotor];
//       while ((nMotorEncoder[motorD] > - Duration) || (nMotorEncoder[motorE] < Duration)) //while the encoder wheel turns one revolution

   while ((RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) > -Duration) ||
     			(RelativeMotorEncoderPos(leftMotor,leftMotorEncoderStart) > -Duration)){ //while the encoder wheel turns one revolution
			SetMotorEncoderPos();
       if (bUseTimer & (time1[T1] > MaxTime)){
            break;
       }
        motor[rightMotor] = -Power; //Right motor is reversed. Apply power in reverse
        motor[leftMotor] = -Power;
   }
   StopDriveMotors(); //turn both motors off
}

void TurnRight(int Power, int Angle, int MaxTime){
	int startAngle = HTMCreadHeading(HTCompass);
	int destAngle = startAngle + Angle;
	if (destAngle > 360)
	{
		destAngle -= 360;	// wrap around
	}

  ClearTimer(T1);

	int curAngle = startAngle;
	int prevAngle = curAngle;
	while (true) {
		curAngle = HTMCreadHeading(HTCompass);
		int diffAngle = abs(curAngle - destAngle);
		if (diffAngle < 3) {
			break;	// done
    }
		writeDebugStreamLine("we're going right. I'm at %d. I'm going to %d.", curAngle, destAngle);
		int adjPower = Power;
		if (diffAngle < 20) {
			adjPower /= 2;	// slow down as we get close
		}
		motor[rightMotor] = -adjPower;
		motor[leftMotor] = adjPower;
      if (bUseTimer & (time1[T1] > MaxTime)){
           break;
      }
	}
	writeDebugStreamLine("yay I'm done going right.");

  StopDriveMotors(); //turn both motors off
}

void TurnLeft(int Power, int Angle, int MaxTime){
	int startAngle = HTMCreadHeading(HTCompass);
	int destAngle = startAngle - Angle;
	if (destAngle < 0)
	{
		destAngle += 360;	// wrap around
	}

  ClearTimer(T1);

	int curAngle = startAngle;
	while (true) {
		curAngle = HTMCreadHeading(HTCompass);
		int diffAngle = abs(curAngle - destAngle);
		if (diffAngle < 3) {
			break;	// done
    }
		writeDebugStreamLine("we're going left. I'm at %d. I'm going to %d.", curAngle, destAngle);
		int adjPower = Power;
		if (diffAngle < 20) {
			adjPower /= 2;	// slow down as we get close
		}
		motor[rightMotor] = adjPower;
		motor[leftMotor] = -adjPower;
      if (bUseTimer & (time1[T1] > MaxTime)){
           break;
      }
	}
	writeDebugStreamLine("yay I'm done going left.");

  StopDriveMotors(); //turn both motors off
}
void TurnRightWithoutCompass(int Power, int Angle, int MaxTime){
	 int Duration = Angle * degreesToDuration;

   ClearTimer(T1);
   int leftMotorEncoderStart = nMotorEncoder[leftMotor];	// save starting position
   int rightMotorEncoderStart = nMotorEncoder[rightMotor];

   while ((RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) > -Duration) ||
     			(RelativeMotorEncoderPos(leftMotor,leftMotorEncoderStart) < Duration)){
     	writeDebugStreamLine("Left motor encoder relative pos: %d \\ Right motor encoder relative pos: %d", nMotorEncoder[leftMotor] - leftMotorEncoderStart, nMotorEncoder[rightMotor] - rightMotorEncoderStart);
			SetMotorEncoderPos();
       if (bUseTimer & (time1[T1] > MaxTime)){
          break;
       }
       if (RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) > -Duration + turningSlowDownThresholdDuration) {
         motor[rightMotor] = -Power; // Right motor is reversed. Apply power
         motor[leftMotor]  =  Power;
       } else {
         motor[rightMotor] = -Power * 0.5; // Right motor is reversed. Apply power
         motor[leftMotor]  =  Power * 0.5;
       }
   }
   StopDriveMotors(); //turn both motors off
}
void TurnLeftWithoutCompass(int Power, int Angle, int MaxTime){
	 int Duration = Angle * degreesToDuration;

   ClearTimer(T1);
   int leftMotorEncoderStart = nMotorEncoder[leftMotor];	// save starting position
   int rightMotorEncoderStart = nMotorEncoder[rightMotor];

   while ((RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) < Duration) ||
     			(RelativeMotorEncoderPos(leftMotor,leftMotorEncoderStart) > -Duration)){
     	writeDebugStreamLine("Left motor encoder relative pos: %d \\ Right motor encoder relative pos: %d", nMotorEncoder[leftMotor] - leftMotorEncoderStart, nMotorEncoder[rightMotor] - rightMotorEncoderStart);
			SetMotorEncoderPos();
       if (bUseTimer & (time1[T1] > MaxTime)){
          break;
       }
       if (RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) < Duration - turningSlowDownThresholdDuration) {
         motor[rightMotor] =  Power; // Right motor is reversed. Apply power
         motor[leftMotor]  = -Power;
       } else {
         motor[rightMotor] =  Power * 0.5; // Right motor is reversed. Apply power
         motor[leftMotor]  = -Power * 0.5;
       }
   }
   StopDriveMotors(); //turn both motors off
}

void initializeRobot(){//	initialize motor encoders so we can keep track of how far we've driven
	ResetDriveMotorEncoders();
	nMotorEncoder[liftMotor] = 0;
}
int sd = 0;
int waitTime = 500;
void driveToReleaseDistance(){
	//sd = SensorValue[sonar];
	//while(SensorValue[sonar] > releaseBlockDistance){
	//	DriveForward(100,5,4000);
	//}
	//while(SensorValue[sonar] < releaseBlockDistance){
	//	DriveBack(100,5,4000);
	//}
	//wait1Msec(waitTime);
}

/*void release(){
	servo[scoopDumpServo] = scoopDumpServoUpPos;
	motor[brushMotor] = -100;	// outward
	wait1Msec(1500);
	servo[scoopDumpServo] = scoopDumpServerDownPos; 	// turn off brush motor and put scoop in down position
  motor[brushMotor] = 0;
}*/
void Dump(){
	servo[scoopDumpServo] = scoopDumpServoUpPos;
  motor[brushMotor] = -100 * brushSpeed;	// outward
	wait1Msec(1000);
	servo[scoopDumpServo] = scoopDumpServerDownPos;
  motor[brushMotor] = 0;	// outward
}
void LiftGiraffe() {
	int liftMotorEncoderValue = nMotorEncoder[liftMotor];
	while (liftMotorEncoderValue < scoopAtBasketPos) {
		if (liftMotorEncoderValue >= scoopAtBasketPos - 1000) {
			motor[liftMotor] =  50;
		} else {
			motor[liftMotor] = 100;
		}
		liftMotorEncoderValue = nMotorEncoder[liftMotor];
	}
	motor[liftMotor] = 0;
}
void LowerGiraffe() {
	int liftMotorEncoderValue = nMotorEncoder[liftMotor];
	while (liftMotorEncoderValue > 0) {
		writeDebugStreamLine("Lift motor encoder: %d", nMotorEncoder[liftMotor]);
		if (liftMotorEncoderValue <= 2000) {
			motor[liftMotor] =  -25;
		} else {
			motor[liftMotor] = -100;
		}
		liftMotorEncoderValue = nMotorEncoder[liftMotor];
	}
	motor[liftMotor] = 0;
	writeDebugStreamLine("Lift motor stopped.");
}
void GoToLine(int Power){
	  int gray = LSvalNorm(HTLight);
	  float lightThreshold = 1.15;
	  while(LSvalNorm(HTLight) < gray*lightThreshold){
	  	writeDebugStreamLine("%d", LSvalNorm(HTLight));
	  	motor[rightMotor] = Power; // Right motor is reversed. Apply power
      motor[leftMotor] = Power;
	  }
  	motor[rightMotor] = 0; // Right motor is reversed. Apply power
    motor[leftMotor] = 0;
}
task main(){
  initializeRobot();

  waitForStart(); // Wait for the beginning of autonomous phase.

  //set up to be perpendicular to baskets
	//getRobotPerpendicularToBaskets();

	//// place robot 58 inches from corner (middle of the third pad), parallel to bins
	motor[liftMotor] = 100;
	DriveForward (40, 1900, 4000, false);	// drive forward ~15in to first basket

	TurnRightWithoutCompass (40, 90, 4000);

	LiftGiraffe();

	DriveForward (30,800,4000,false);	// drive forward into first basket

	Dump();

	DriveBack(30,1440,4000);	// drive forward into first basket

	motor[liftMotor] = -100;

	TurnRightWithoutCompass (40, 35, 4000);

	DriveForward(50, 3000, 4000, false);

	LowerGiraffe();

	TurnLeftWithoutCompass (40, 45, 4000);

	GoToLine(30);

	TurnRightWithoutCompass (40, 80, 4000);

	DriveBack (50, 4400, 4000);
	//int curAngle = HTMCreadHeading(HTCompass);
	//writeDebugStreamLine(">>> we're going left. I'm at %d", curAngle);

	//TurnLeft(20,135,4000);
	//Sleep(500);	// sleep
	//curAngle = HTMCreadHeading(HTCompass);
	//writeDebugStreamLine(">>>I'm done going left. I'm at %d", curAngle);

	//	writeDebugStreamLine(">>> we're going left. I'm at %d", curAngle);

	//TurnLeft(20,180,4000);
	//Sleep(500);	// sleep
	//curAngle = HTMCreadHeading(HTCompass);
	//writeDebugStreamLine(">>>I'm done going left. I'm at %d", curAngle);


	wait1Msec(waitTime);

	/*
	driveToReleaseDistance();
	wait1Msec(waitTime);
	release();

	TurnRight(40,90,4000);
	wait1Msec(waitTime);
	DriveForward(40,2000,4000);
	wait1Msec(waitTime);
	TurnLeft(40,80,4000);
	wait1Msec(waitTime);
	DriveForward(40,7000,4000);
	wait1Msec(waitTime);
	TurnRight(40,100,4000);
	wait1Msec(waitTime);
	DriveBack(40,5000,4000);*/
}
