#pragma config(Hubs,  S1, HTMotor,  HTMotor,  none,     none)
#pragma config(Hubs,  S2, HTServo,  HTServo,  HTMotor,  none)
#pragma config(Sensor, S1,     ,               sensorI2CMuxController)
#pragma config(Sensor, S2,     ,               sensorI2CMuxController)
#pragma config(Sensor, S3,     HTMUX1,         sensorI2CCustom)
#pragma config(Sensor, S4,     HTMUX2,         sensorI2CCustom)
#pragma config(Motor,  mtr_S1_C1_1,     spinnerMotor,  tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S1_C1_2,     rightMotor,    tmotorTetrix, PIDControl, reversed, encoder)
#pragma config(Motor,  mtr_S1_C2_1,     liftMotor,     tmotorTetrix, PIDControl, reversed, encoder)
#pragma config(Motor,  mtr_S1_C2_2,     leftMotor,     tmotorTetrix, PIDControl, encoder)
#pragma config(Motor,  mtr_S2_C3_1,     brushMotor,    tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S2_C3_2,     motorI,        tmotorTetrix, openLoop)
#pragma config(Servo,  srvo_S2_C1_1,    rightSpinServo,       tServoStandard)
#pragma config(Servo,  srvo_S2_C1_2,    leftSpinServo,        tServoStandard)
#pragma config(Servo,  srvo_S2_C1_3,    servo3,               tServoNone)
#pragma config(Servo,  srvo_S2_C1_4,    servo4,               tServoNone)
#pragma config(Servo,  srvo_S2_C1_5,    servo5,               tServoNone)
#pragma config(Servo,  srvo_S2_C1_6,    servo6,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_1,    servo7,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_2,    servo8,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_3,    servo9,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_4,    servo10,              tServoNone)
#pragma config(Servo,  srvo_S2_C2_5,    servo11,              tServoNone)
#pragma config(Servo,  srvo_S2_C2_6,    scoopDumpServo,       tServoStandard)

#include "JoystickDriver.c"  //Include file to "handle" the Bluetooth messages.
#include "drivers/common.h"
#include "drivers/hitechnic-sensormux.h"
#include "drivers/hitechnic-irseeker-v2.h"
#include "drivers/hitechnic-compass.h"
#include "drivers/lego-touch.h"
#include "drivers/lego-light.h"
#include "drivers/lego-ultrasound.h"
//////
//	Mux sensors
//  MUX 1 is connected to S3
//
// Left IR Sensor is connected to 1st port of the MUX
const tMUXSensor HTRightIRS = msensor_S3_1;
//	Ultrasonic sensor is connected to the 2nd port of the MUX
const tMUXSensor HTUltra = msensor_S3_2;
//////
//	Mux sensors
//  MUX 2 is connected to S4
//
// Left IR Sensor is connected to 1st port of the MUX
const tMUXSensor HTLeftIRS = msensor_S4_1;
// Compass sensor is connected to the 2nd port of the MUX
const tMUXSensor HTCompass = msensor_S4_2;
// Light sensor is connected to the 3rd port of the MUX
const tMUXSensor HTLight = msensor_S4_3;

//THIS PROGRAM IS FOR WHEN THE BASKETS ARE TO THE RIGHT OF THE ROBOT FROM THE ROBOT'S PERSPECTIVE
	//          |_|   |
	//          |_| <-----BASKETS! (they are to the right)
	//          |_|   |
	//          |_|___|__
	//     ^
	//     |
	//   START


////////////////////////
//
//	Constants
//
const int spinServoStartPos = 127;	// start both servos at the mid point - this is the stowed position
const int leftSpinServoDeployPos = 255;	// left spin servo value when deployed
const int rightSpinServoDeployPos = 0;		// right spin servo value when deployed
const int scoopDumpServerDownPos = 60;	// scoop dump servo down position (start here)
const int scoopDumpServoUpPos = 210;		// scoop dump servo dump position
bool bUseTimer = false;	// Timer Flag - turn this to true to limit movement to timer
int leftMotorEncoderPos = 0;	// Motor Data - so we can see what these are during debugging
int rightMotorEncoderPos = 0;
int releaseBlockDistance = 40;
float angleUnitToDegreesConstant = 24.4444444444;
int forwardDistance = 0;
int gray = 0;
int colorThreshold = 20;
int turningWiggleRoom = 90; // how far the robot can turn left if it plans to turn right, wighout looping and thinking it's done
int turningMinPassTimes = 10; // how many times must a robot confirm that it's done turning

void SetMotorEncoderPos(){
	leftMotorEncoderPos = nMotorEncoder[leftMotor];
	rightMotorEncoderPos = nMotorEncoder[rightMotor];
}

void ResetDriveMotorEncoders(){
	nMotorEncoder[leftMotor] = 0;  // reset the motor encoder positions
  nMotorEncoder[rightMotor] = 0;
  SetMotorEncoderPos();
}

int RelativeMotorEncoderPos(int motorID, int startPos){
	return nMotorEncoder[motorID] - startPos;
}
void StopDriveMotors(){
	motor[leftMotor] = 0;	// stop drive motors
	motor[rightMotor] = 0;
}
void DriveForward(int Power, int Duration, int MaxTime){
   ClearTimer(T1);
   int leftMotorEncoderStart = nMotorEncoder[leftMotor];	// save starting position
   int rightMotorEncoderStart = nMotorEncoder[rightMotor];

   while ((RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) < Duration) ||
     			(RelativeMotorEncoderPos(leftMotor,leftMotorEncoderStart) > -Duration)){ //while the encoder wheel turns one revolution
     			SetMotorEncoderPos();
       if (bUseTimer & (time1[T1] > MaxTime)){
          break;
       }
       motor[rightMotor] = Power; // Right motor is reversed. Apply power
       motor[leftMotor] = Power;
   }
   StopDriveMotors(); //turn both motors off
}
void DriveBack(int Power, int Duration, int MaxTime)
{
   ClearTimer(T1);
   int leftMotorEncoderStart = nMotorEncoder[leftMotor];	// save starting position
   int rightMotorEncoderStart = nMotorEncoder[rightMotor];
//       while ((nMotorEncoder[motorD] > - Duration) || (nMotorEncoder[motorE] < Duration)) //while the encoder wheel turns one revolution

   while ((RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) > - Duration) ||
     			(RelativeMotorEncoderPos(leftMotor,leftMotorEncoderStart) < Duration)){
			SetMotorEncoderPos();
       if (bUseTimer & (time1[T1] > MaxTime)){
            break;
       }
        motor[rightMotor] = -Power; //Right motor is reversed. Apply power in reverse
        motor[leftMotor] = -Power;
   }
   StopDriveMotors(); //turn both motors off
}

void TurnRight(int Power, int Angle, int MaxTime){
	int startAngle = HTMCreadHeading(HTCompass);
	int destAngle = startAngle + Angle;

  ClearTimer(T1);

  int passTimes = 0;
	int curAngle = startAngle;
	while (passTimes < turningMinPassTimes) {
		curAngle = HTMCreadHeading(HTCompass);
		if (curAngle < startAngle - turningWiggleRoom) {
			curAngle += 360;	// if the compass "looped over", offset it so we treat it as linear
		}
    if (curAngle > destAngle) {
    	passTimes++;
    } else {
      passTimes = 0;
    }
		//writeDebugStreamLine("we're going right. I'm at %d. I'm going to %d.", curAngle, destAngle);
		motor[rightMotor] = -Power;
		motor[leftMotor] = Power;
      if (bUseTimer & (time1[T1] > MaxTime)){
           break;
      }
	}
	//writeDebugStreamLine("yay I'm done going right.");

  StopDriveMotors(); //turn both motors off
}

void TurnLeft(int Power, int Angle, int MaxTime){
	int startAngle = HTMCreadHeading(HTCompass);
	int destAngle = startAngle - Angle;

  ClearTimer(T1);

  int passTimes = 0;
	int curAngle = startAngle;
	while (passTimes < turningMinPassTimes) {
		curAngle = HTMCreadHeading(HTCompass);
		if (curAngle > startAngle + turningWiggleRoom) {
			curAngle -= 360;	// if the compass "looped over", offset it so we treat it as linear
		}
    if (curAngle < destAngle) {
    	passTimes++;
    } else {
      passTimes = 0;
    }
		//writeDebugStreamLine("we're going left. I'm at %d. I'm going to %d.", curAngle, destAngle);
		motor[rightMotor] = Power;
		motor[leftMotor] = -Power;
      if (bUseTimer & (time1[T1] > MaxTime)){
           break;
      }
	}
	//writeDebugStreamLine("yay I'm done going left.");

  StopDriveMotors(); //turn both motors off
}

void initializeRobot(){//	initialize motor encoders so we can keep track of how far we've driven
	ResetDriveMotorEncoders();
}
int waitTime = 500;
void driveToReleaseDistance(){
	while(USreadDist(HTUltra) > releaseBlockDistance){
		DriveForward(100,5,4000);
	}
	while(USreadDist(HTUltra) < releaseBlockDistance){
		DriveBack(100,5,4000);
	}
	wait1Msec(waitTime);
}
void release(){
	servo[scoopDumpServo] = scoopDumpServoUpPos;
	motor[brushMotor] = -100;	// outward
	wait1Msec(1500);
	servo[scoopDumpServo] = scoopDumpServerDownPos; 	// turn off brush motor and put scoop in down position
  motor[brushMotor] = 0;
}
void getRobotPerpendicularToBaskets(){
	//DriveForward(100, 1500, 4000);
	//wait1Msec(waitTime);
	//TurnLeft(100, 90, 4000);
	//wait1Msec(waitTime);
	//DriveForward(100, 4000, 4000);
	//wait1Msec(waitTime);
//	TurnRight(100, 45, 4000);
//	wait1Msec(waitTime);
}
void drivetoIRBasket(){
	int irDir = 0;
	int irStrEnh = 0;
	HTIRS2readEnhanced(HTRightIRS,irDir,irStrEnh);
	while(irDir <= 8){
			HTIRS2readEnhanced(HTRightIRS,irDir,irStrEnh);
			DriveForward(100,5,4000);
	}
	DriveForward(100,1400,4000);
	wait1Msec(waitTime);
}
void goToWhiteLine(){
 while(LSvalRaw(HTLight) < gray+colorThreshold){
			DriveForward(100,5,4000);
	}
}
task main(){
	initializeRobot();

	//set up to be perpendicular to baskets
	getRobotPerpendicularToBaskets();
	int startEncoderValue = nMotorEncoder[leftMotor];
	gray = LSvalRaw(HTLight);
	drivetoIRBasket();
	forwardDistance = startEncoderValue-nMotorEncoder[leftMotor];//backjwards
	TurnRight(100, 90, 4000);

	wait1Msec(waitTime);
	driveToReleaseDistance();
	wait1Msec(waitTime);
	release();

	TurnRight(100,90,4000);
	wait1Msec(waitTime);
	DriveForward(100,forwardDistance,4000);
	wait1Msec(waitTime);
	TurnLeft(100,80,4000);
	wait1Msec(waitTime);
	goToWhiteLine();
	wait1Msec(waitTime);
	TurnRight(100,100,4000);
	wait1Msec(waitTime);
	DriveBack(100,5000,4000);
}
