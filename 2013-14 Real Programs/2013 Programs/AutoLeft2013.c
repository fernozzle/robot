#pragma config(Hubs,  S1, HTMotor,  HTMotor,  none,     none)
#pragma config(Hubs,  S2, HTServo,  HTServo,  HTMotor,  none)
#pragma config(Sensor, S1,     ,               sensorI2CMuxController)
#pragma config(Sensor, S2,     ,               sensorI2CMuxController)
#pragma config(Sensor, S3,     HTMUX1,         sensorI2CCustom)
#pragma config(Sensor, S4,     HTMUX2,         sensorI2CCustom)
#pragma config(Motor,  mtr_S1_C1_1,     spinnerMotor,  tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S1_C1_2,     rightMotor,    tmotorTetrix, PIDControl, reversed, encoder)
#pragma config(Motor,  mtr_S1_C2_1,     liftMotor,     tmotorTetrix, PIDControl, reversed, encoder)
#pragma config(Motor,  mtr_S1_C2_2,     leftMotor,     tmotorTetrix, PIDControl, encoder)
#pragma config(Motor,  mtr_S2_C3_1,     brushMotor,    tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S2_C3_2,     motorI,        tmotorTetrix, openLoop)
#pragma config(Servo,  srvo_S2_C1_1,    rightSpinServo,       tServoStandard)
#pragma config(Servo,  srvo_S2_C1_2,    leftSpinServo,        tServoStandard)
#pragma config(Servo,  srvo_S2_C1_3,    servo3,               tServoNone)
#pragma config(Servo,  srvo_S2_C1_4,    servo4,               tServoNone)
#pragma config(Servo,  srvo_S2_C1_5,    servo5,               tServoNone)
#pragma config(Servo,  srvo_S2_C1_6,    servo6,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_1,    servo7,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_2,    servo8,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_3,    servo9,               tServoNone)
#pragma config(Servo,  srvo_S2_C2_4,    servo10,              tServoNone)
#pragma config(Servo,  srvo_S2_C2_5,    servo11,              tServoNone)
#pragma config(Servo,  srvo_S2_C2_6,    scoopDumpServo,       tServoStandard)
#include "JoystickDriver.c"  //Include file to "handle" the Bluetooth messages.
#include "drivers/common.h"
#include "drivers/hitechnic-sensormux.h"
#include "drivers/hitechnic-irseeker-v2.h"
#include "drivers/hitechnic-compass.h"
#include "drivers/lego-touch.h"
#include "drivers/lego-light.h"
#include "drivers/lego-ultrasound.h"

//THIS PROGRAM IS FOR WHEN THE BASKETS ARE TO THE LEFT OF THE ROBOT FROM THE ROBOT'S PERSPECTIVE
	//     |    |_|
	//     |    |_| <-----BASKETS! (they are to the left)
	//     |    |_|
	//  ________|_|
	//                  ^
	//                  |
	//                START

bool bUseTimer = false;	// Timer Flag - turn this to true to limit movement to timer
int leftMotorEncoderPos = 0;	// Motor Data - so we can see what these are during debugging
int rightMotorEncoderPos = 0;
int releaseBlockDistance = 40;
float angleUnitToDegreesConstant = 24.4444444444;

void SetMotorEncoderPos(){
	leftMotorEncoderPos = nMotorEncoder[leftMotor];
	rightMotorEncoderPos = nMotorEncoder[rightMotor];
}

void ResetDriveMotorEncoders(){
	nMotorEncoder[leftMotor] = 0;  // reset the motor encoder positions
  nMotorEncoder[rightMotor] = 0;
  SetMotorEncoderPos();
}

int RelativeMotorEncoderPos(int motorID, int startPos){
	return nMotorEncoder[motorID] - startPos;
}
void StopDriveMotors(){
	motor[leftMotor] = 0;	// stop drive motors
	motor[rightMotor] = 0;
}
void DriveForward(int Power, int Duration, int MaxTime){
   ClearTimer(T1);
   int leftMotorEncoderStart = nMotorEncoder[leftMotor];	// save starting position
   int rightMotorEncoderStart = nMotorEncoder[rightMotor];

   while ((RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) < Duration) ||
     			(RelativeMotorEncoderPos(leftMotor,leftMotorEncoderStart) > -Duration)){ //while the encoder wheel turns one revolution
			SetMotorEncoderPos();
       if (bUseTimer & (time1[T1] > MaxTime)){
          break;
       }
       motor[rightMotor] = Power; // Right motor is reversed. Apply power
       motor[leftMotor] = Power;
   }
   StopDriveMotors(); //turn both motors off
}
void DriveBack(int Power, int Duration, int MaxTime)
{
   ClearTimer(T1);
   int leftMotorEncoderStart = nMotorEncoder[leftMotor];	// save starting position
   int rightMotorEncoderStart = nMotorEncoder[rightMotor];
//       while ((nMotorEncoder[motorD] > - Duration) || (nMotorEncoder[motorE] < Duration)) //while the encoder wheel turns one revolution

   while ((RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) > - Duration) ||
     			(RelativeMotorEncoderPos(leftMotor,leftMotorEncoderStart) < Duration)){
			SetMotorEncoderPos();
       if (bUseTimer & (time1[T1] > MaxTime)){
            break;
       }
        motor[rightMotor] = -Power; //Right motor is reversed. Apply power in reverse
        motor[leftMotor] = -Power;
   }
   StopDriveMotors(); //turn both motors off
}
void turnRight(int Power, int Angle, int MaxTime){
	int Duration = Angle*angleUnitToDegreesConstant;
   ClearTimer(T1);
   int leftMotorEncoderStart = nMotorEncoder[leftMotor];	// save starting position
   int rightMotorEncoderStart = nMotorEncoder[rightMotor]; //     while ((nMotorEncoder[rightMotor] < Duration) || (nMotorEncoder[leftMotor] < Duration)) //while the encoder wheel turns one revolution
   while ((RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) > -Duration) ||
     			(RelativeMotorEncoderPos(leftMotor,leftMotorEncoderStart) > -Duration)){
			SetMotorEncoderPos();
      if (bUseTimer & (time1[T1] > MaxTime)){
           break;
      }
      motor[rightMotor] = -Power; //Right motor is reversed. Apply power
      motor[leftMotor] = Power;
   }
   StopDriveMotors(); //turn both motors off
}


void turnLeft(int Power, int Angle, int MaxTime){
	int Duration = Angle*angleUnitToDegreesConstant;
   ClearTimer(T1);
   int leftMotorEncoderStart = nMotorEncoder[leftMotor];	// save starting position
   int rightMotorEncoderStart = nMotorEncoder[rightMotor]; //while ((nMotorEncoder[rightMotor] > -  Duration) || (nMotorEncoder[leftMotor] > - Duration))
   while ((RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) < Duration) ||
     			(RelativeMotorEncoderPos(leftMotor,leftMotorEncoderStart)  < Duration)){
		 	SetMotorEncoderPos();
       if (bUseTimer & (time1[T1] > MaxTime)){
           break;
       }
       motor[rightMotor] = Power; //Right motor is reversed. Apply power
       motor[leftMotor] = -Power;
   }
   StopDriveMotors(); //turn both motors off
}

void initializeRobot(){//	initialize motor encoders so we can keep track of how far we've driven
	ResetDriveMotorEncoders();
}
int sd = 0;
int waitTime = 300;
void driveToReleaseDistance(){
	sd = SensorValue[sonar];
	while(SensorValue[sonar] > releaseBlockDistance){
		DriveForward(100,5,4000);
	}
	while(SensorValue[sonar] < releaseBlockDistance){
		DriveBack(100,5,4000);
	}
	wait1Msec(waitTime);
}
void release(){ //THIS WHOLE THING WILL BE DIFFERENT IRL.
	while(nMotorEncoder[armMotor] < 450){
		//Keep raising the arm
		motor[armMotor] = 100;
	}
	//Stop raising the arm
	motor[armMotor] = 0;
	wait1Msec(waitTime);
	motor[gripperMotor]= -50;
	wait1Msec(500);
	motor[gripperMotor]= 0;
}
void getRobotPerpendicularToBaskets(){
	DriveForward(100, 1500, 4000);
	wait1Msec(waitTime);
	turnRight(100, 90, 4000);
	wait1Msec(waitTime);
	DriveForward(100, 4000, 4000);
	wait1Msec(waitTime);
	turnLeft(100, 45, 4000);
	wait1Msec(waitTime);
}
task main(){
	initializeRobot();
	//set up to be perpendicular to baskets
	getRobotPerpendicularToBaskets();

	DriveForward(100,2700,4000);
	wait1Msec(waitTime);
	turnLeft(100, 90, 4000);
	wait1Msec(waitTime);

	driveToReleaseDistance();
	wait1Msec(waitTime);
	release();

	turnLeft(100,90,4000);
	wait1Msec(waitTime);
	DriveForward(100,2000,4000);
	wait1Msec(waitTime);
	turnRight(100,90,4000);
	wait1Msec(waitTime);
	DriveForward(100,7000,4000);
	wait1Msec(waitTime);
	turnLeft(100,90,4000);
	wait1Msec(waitTime);
	DriveBack(100,5000,4000);
}
