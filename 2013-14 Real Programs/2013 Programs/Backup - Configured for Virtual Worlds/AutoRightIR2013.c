#pragma config(Hubs,  S1, HTMotor,  HTMotor,  none,     none)
#pragma config(Sensor, S1,     ,               sensorI2CMuxController)
#pragma config(Sensor, S2,     IRSeeker,       sensorHiTechnicIRSeeker1200)
#pragma config(Sensor, S3,     light,          sensorLightActive)
#pragma config(Sensor, S4,     sonar,          sensorSONAR)
#pragma config(Motor,  motorA,          gripperMotor,  tmotorNXT, PIDControl, encoder)
#pragma config(Motor,  mtr_S1_C1_1,     rightMotor,    tmotorTetrix, openLoop, encoder)
#pragma config(Motor,  mtr_S1_C1_2,     leftMotor,     tmotorTetrix, openLoop, reversed, encoder)
#pragma config(Motor,  mtr_S1_C2_1,     armMotor,      tmotorTetrix, openLoop, encoder)
#pragma config(Motor,  mtr_S1_C2_2,     spinnerMotor,  tmotorTetrix, openLoop, encoder)

//THIS PROGRAM IS FOR WHEN THE BASKETS ARE TO THE RIGHT OF THE ROBOT FROM THE ROBOT'S PERSPECTIVE
	//          |_|   |
	//          |_| <-----BASKETS! (they are to the right)
	//          |_|   |
	//          |_|___|__
	//     ^
	//     |
	//   START

bool bUseTimer = false;	// Timer Flag - turn this to true to limit movement to timer
int leftMotorEncoderPos = 0;	// Motor Data - so we can see what these are during debugging
int rightMotorEncoderPos = 0;
int releaseBlockDistance = 40;
float angleUnitToDegreesConstant = 24.4444444444;
int forwardDistance = 0;
int gray = 0;
int colorThreshold = 20;

void SetMotorEncoderPos(){
	leftMotorEncoderPos = nMotorEncoder[leftMotor];
	rightMotorEncoderPos = nMotorEncoder[rightMotor];
}

void ResetDriveMotorEncoders(){
	nMotorEncoder[leftMotor] = 0;  // reset the motor encoder positions
  nMotorEncoder[rightMotor] = 0;
  SetMotorEncoderPos();
}

int RelativeMotorEncoderPos(int motorID, int startPos){
	return nMotorEncoder[motorID] - startPos;
}
void StopDriveMotors(){
	motor[leftMotor] = 0;	// stop drive motors
	motor[rightMotor] = 0;
}
void DriveForward(int Power, int Duration, int MaxTime){
   ClearTimer(T1);
   int leftMotorEncoderStart = nMotorEncoder[leftMotor];	// save starting position
   int rightMotorEncoderStart = nMotorEncoder[rightMotor];

   while ((RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) < Duration) ||
     			(RelativeMotorEncoderPos(leftMotor,leftMotorEncoderStart) > -Duration)){ //while the encoder wheel turns one revolution
     			SetMotorEncoderPos();
       if (bUseTimer & (time1[T1] > MaxTime)){
          break;
       }
       motor[rightMotor] = Power; // Right motor is reversed. Apply power
       motor[leftMotor] = Power;
   }
   StopDriveMotors(); //turn both motors off
}
void DriveBack(int Power, int Duration, int MaxTime)
{
   ClearTimer(T1);
   int leftMotorEncoderStart = nMotorEncoder[leftMotor];	// save starting position
   int rightMotorEncoderStart = nMotorEncoder[rightMotor];
//       while ((nMotorEncoder[motorD] > - Duration) || (nMotorEncoder[motorE] < Duration)) //while the encoder wheel turns one revolution

   while ((RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) > - Duration) ||
     			(RelativeMotorEncoderPos(leftMotor,leftMotorEncoderStart) < Duration)){
			SetMotorEncoderPos();
       if (bUseTimer & (time1[T1] > MaxTime)){
            break;
       }
        motor[rightMotor] = -Power; //Right motor is reversed. Apply power in reverse
        motor[leftMotor] = -Power;
   }
   StopDriveMotors(); //turn both motors off
}
void TurnRight(int Power, int Angle, int MaxTime){
	int Duration = Angle*angleUnitToDegreesConstant;
   ClearTimer(T1);
   int leftMotorEncoderStart = nMotorEncoder[leftMotor];	// save starting position
   int rightMotorEncoderStart = nMotorEncoder[rightMotor]; //     while ((nMotorEncoder[rightMotor] < Duration) || (nMotorEncoder[leftMotor] < Duration)) //while the encoder wheel turns one revolution
   while ((RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) > -Duration) ||
     			(RelativeMotorEncoderPos(leftMotor,leftMotorEncoderStart) > -Duration)){
			SetMotorEncoderPos();
      if (bUseTimer & (time1[T1] > MaxTime)){
           break;
      }
      motor[rightMotor] = -Power; //Right motor is reversed. Apply power
      motor[leftMotor] = Power;
   }
   StopDriveMotors(); //turn both motors off
}


void TurnLeft(int Power, int Angle, int MaxTime){
	int Duration = Angle*angleUnitToDegreesConstant;
   ClearTimer(T1);
   int leftMotorEncoderStart = nMotorEncoder[leftMotor];	// save starting position
   int rightMotorEncoderStart = nMotorEncoder[rightMotor]; //while ((nMotorEncoder[rightMotor] > -  Duration) || (nMotorEncoder[leftMotor] > - Duration))
   while ((RelativeMotorEncoderPos(rightMotor,rightMotorEncoderStart) < Duration) ||
     			(RelativeMotorEncoderPos(leftMotor,leftMotorEncoderStart)  < Duration)){
		 	SetMotorEncoderPos();
       if (bUseTimer & (time1[T1] > MaxTime)){
           break;
       }
       motor[rightMotor] = Power; //Right motor is reversed. Apply power
       motor[leftMotor] = -Power;
   }
   StopDriveMotors(); //turn both motors off
}

void initializeRobot(){//	initialize motor encoders so we can keep track of how far we've driven
	ResetDriveMotorEncoders();
}
int waitTime = 500;
void driveToReleaseDistance(){
	while(SensorValue[sonar] > releaseBlockDistance){
		DriveForward(100,5,4000);
	}
	while(SensorValue[sonar] < releaseBlockDistance){
		DriveBack(100,5,4000);
	}
	wait1Msec(waitTime);
}
void release(){ //THIS WHOLE THING WILL BE DIFFERENT IRL.
	while(nMotorEncoder[armMotor] < 450){
		//Keep raising the arm
		motor[armMotor] = 100;
	}
	//Stop raising the arm
	motor[armMotor] = 0;
	wait1Msec(waitTime);
	motor[gripperMotor]= -50;
	wait1Msec(waitTime);
	motor[gripperMotor]= 0;
}
void getRobotPerpendicularToBaskets(){
	DriveForward(100, 1500, 4000);
	wait1Msec(waitTime);
	TurnLeft(100, 90, 4000);
	wait1Msec(waitTime);
	DriveForward(100, 4000, 4000);
	wait1Msec(waitTime);
	TurnRight(100, 45, 4000);
	wait1Msec(waitTime);
}
void drivetoIRBasket(){
	while(SensorValue[IRSeeker] <= 8){
			DriveForward(100,5,4000);
	}
	DriveForward(100,1400,4000);
	wait1Msec(waitTime);
}
void goToWhiteLine(){
 while(SensorValue[light] < gray+colorThreshhold){
			DriveForward(100,5,4000);
	}
}
task main(){
	initializeRobot();

	//set up to be perpendicular to baskets
	getRobotPerpendicularToBaskets();
	int startEncoderValue = nMotorEncoder[leftMotor];
	gray = sensorValue[light];
	driveToIRBasket();
	forwardDistance = startEncoderValue-nMotorEncoder[leftMotor];//backjwards
	TurnRight(100, 90, 4000);

	wait1Msec(waitTime);
	driveToReleaseDistance();
	wait1Msec(waitTime);
	release();

	TurnRight(100,90,4000);
	wait1Msec(waitTime);
	DriveForward(100,forwardDistance,4000);
	wait1Msec(waitTime);
	TurnLeft(100,80,4000);
	wait1Msec(waitTime);
	goToWhiteLine();
	wait1Msec(waitTime);
	TurnRight(100,100,4000);
	wait1Msec(waitTime);
	DriveBack(100,5000,4000);
}
