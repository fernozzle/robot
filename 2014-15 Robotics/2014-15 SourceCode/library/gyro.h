#ifndef _GYRO_H
#define _GYRO_H

#include "drivers\hitechnic-gyro.h"
#include "macrodefs.h"

//
// Constants.
//
#define GYROF_USER_MASK         0x00ff

#define GYROF_HTSMUX          0x0080

#define GYRO_NUM_CAL_SAMPLES    50
#define GYRO_CAL_INTERVAL       10

//
// Macros
//
#define GyroGetTurnRate(p)      (p.turnRate)
#define GyroGetHeading(p)       (p.heading)

//
// Type definitions.
//
typedef struct
{
	int   sensorID;
	int   gyroFlags;
	int   zeroOffset;
	int   deadBand;
	long  timestamp;
	int   turnRate;
	float heading;
	bool bInverted;	// true if gyro is mounted upside down such that right/left are swapped
} GYRO;

/**
*  This function calibrates the gyro for zero offset and deadband.
*
*  @param gyro Points to the GYRO structure to be initialized.
*  @param numSamples Specifies the number of calibration samples.
*  @param calInterval Specifies the calibration interval in msec.
*/
void
GYROCal(
GYRO &myGyro,
int numSamples,
int calInterval
)
{
	int i;
	int turnRate;
	int min, max;

	myGyro.zeroOffset = 0;
	myGyro.deadBand = 0;
	min = 1023;
	max = 0;

	for (i = 0; i < numSamples; i++)
	{
#ifdef __HTSMUX_SUPPORT__
		turnRate = (myGyro.gyroFlags & GYROF_HTSMUX)?
	HTGYROreadRot((tMUXSensor)myGyro.sensorID):
		HTGYROreadRot((tSensors)myGyro.sensorID);
#else
		turnRate = HTGYROreadRot((tSensors)myGyro.sensorID);
#endif
		myGyro.zeroOffset += turnRate;

		if (turnRate < min)
		{
			min = turnRate;
		}
		else if (turnRate > max)
		{
			max = turnRate;
		}

		wait1Msec(calInterval);
	}

	myGyro.zeroOffset /= numSamples;
	myGyro.deadBand = max - min;

	return;
}   //GyroCal

/**
*  This function performs the gyro task where it integrates the turn rate
*  into a heading value.
*
*  @param gyro Points to the GYRO structure.
*/
void
GYROTask(
GYRO &myGyro
)
{
	long currTime;

	currTime = nPgmTime;
#ifdef __HTSMUX_SUPPORT__
	myGyro.turnRate = (myGyro.gyroFlags & GYROF_HTSMUX)?
HTGYROreadRot((tMUXSensor)myGyro.sensorID):
	HTGYROreadRot((tSensors)myGyro.sensorID);
#else
	myGyro.turnRate = HTGYROreadRot((tSensors)myGyro.sensorID);
#endif
	myGyro.turnRate -= myGyro.zeroOffset;
	myGyro.turnRate = DEADBAND(myGyro.turnRate, myGyro.deadBand);
	if (myGyro.bInverted)
	{
		myGyro.turnRate = -myGyro.turnRate;
	}
	myGyro.heading += (float)myGyro.turnRate*(currTime - myGyro.timestamp)/1000;
	myGyro.timestamp = currTime;

	return;
}   //GyroTask

/**
*  This function resets the gyro heading.
*
*  @param gyro Points to the GYRO structure to be reset.
*/
void
GYROReset(
GYRO &myGyro
)
{

	GYROTask(myGyro);
	myGyro.heading = 0;

	return;
}   //GyroReset

/**
*  This function initializes the gyro sensor.
*/
void
GYROInit(
GYRO &myGyro,
int sensorID,
bool bInverted,
int gyroFlags
)
{

	myGyro.sensorID = sensorID;
	myGyro.bInverted = bInverted;
	myGyro.gyroFlags = gyroFlags & GYROF_USER_MASK;
#ifdef __HTSMUX_SUPPORT__
	if (myGyro.gyroFlags & GYROF_HTSMUX)
	{
		writeDebugStreamLine("gyro>mux calibration");
		HTGYROstartCal((tMUXSensor)sensorID);
	}
	else
	{
		writeDebugStreamLine("gyro> mux support, but gyro not on mux?");
		HTGYROstartCal((tSensors)sensorID);
	}
#else
	HTGYROstartCal((tSensors)sensorID);
#endif
	GYROCal(myGyro, GYRO_NUM_CAL_SAMPLES, GYRO_CAL_INTERVAL);
	myGyro.timestamp = nPgmTime;
	GYROReset(myGyro);

	return;
}   //GyroInit

#endif  //ifndef _GYRO_H
