// light.h
//			This contains the library functions for the light sensor.


#ifndef _LIGHT_H
#define _LIGHT_H

#include "..\drivers\lego-light.h"
#include "kalman.h"

//
// Constants
//
#ifdef _KALMAN_H
#define LIGHT_FILTER        0x0001
#endif
#define LIGHT_HTSMUX				 0x0001
//
// Macros.
//

/**
*	 This macro returns the value from the LIGHT sensor.
*
*	 @param i Points to the LIGHT structure.
*
*	 @return Returns the LIGHT sensor value.
*/
#ifdef __HTSMUX_SUPPORT__
#define ReadLight(r)								(((r).flags & LIGHT_HTSMUX)?			 \
LSValNorm((tMUXSensor)((r).sensorID)):\
LSValNorm((tSensors)((r).sensorID)))
#else
#define ReadLight(r)								LSValNorm((tSensors)((r).sensorID)):
#endif

//
// Type definitions.
//
typedef struct
{
	tSensors sensorID;
	int      options;
	int      flags;
	int      lightVal;
#ifdef _KALMAN_H
    KALMAN      kalman;
#endif
} LIGHT;

//	2014Dec22> We ran out of subroutines with RobotC 4.27, so change these to a #define hopefully next year this won't be a limit
#define GetLightVal(myLight) myLight.lightVal
/*
int GetLightVal(LIGHT &myLight)	// returns the value of the light sensor
{
	return(myLight.lightVal);
}
*/

#define SetLightON(myLight) LSsetActive((tMUXSensor) myLight.sensorID);
/*
void SetLightON(LIGHT &myLight)	// turns on the light sensor LED
{
	writeDebugStreamLine("Setting Light On");
	LSsetActive((tMUXSensor) myLight.sensorID);
}
*/

#define SetLightOFF(myLight) LSsetInactive((tMUXSensor) myLight.sensorID);
/*
void SetLightOFF(LIGHT &myLight)	// turns off the light sensor led
{
	writeDebugStreamLine("Setting Light Off");
	LSsetInactive((tMUXSensor) myLight.sensorID);
}
*/

#define CalLightSensorLow(myLight) LScalLow((tMUXSensor) myLight.sensorID);
/*
void CalLightSensorLow(LIGHT &myLight)	// calibrate the low (black) light Sensor value
{
	LScalLow((tMUXSensor) myLight.sensorID);
}
*/

#define CalLightSensorHigh(myLight) LScalHigh((tMUXSensor) myLight.sensorID);
/*
void CalLightSensorHigh(LIGHT &myLight)
{
	LScalHigh((tMUXSensor) myLight.sensorID);
}
*/

/**
*	 This function is called periodically to read the LIGHT sensor and update
*	 the various values.
*/
#define LIGHTTask(myLight) \
	myLight.lightVal = ReadLight(myLight); \	// read normalized value
	if (myLight.options &  LIGHT_FILTER) \
	{ \
		myLight.lightVal = ceil((float)KalmanFilter(myLight.kalman,(float) myLight.lightVal)); \
	} \

/*
void
LIGHTTask(
LIGHT &myLight
)
{
	myLight.lightVal = ReadLight(myLight);	// read normalized value
  if (myLight.options & LIGHT_FILTER)
  {
      myLight.lightVal = ceil((float)KalmanFilter(myLight.kalman,
                                           (float) myLight.lightVal));
  }

	return;
}		//LIGHTTask
*/

/**
*	 This function initializes the LIGHT sensor.
*
*/
void
LIGHTInit(
LIGHT &myLight,
tSensors sensorID,
bool bLightOn,
int options = 0
)
{
	myLight.sensorID = sensorID;
	myLight.options  = options;
	myLight.flags    = 0;
	myLight.lightVal     = 0;

	if (bLightOn)
	{
		SetLightON(myLight);	// turn on the light sensor LED
	}
	else
	{
		SetLightOFF(myLight);	// turn off the light sensor LED
	}
#ifdef _KALMAN_H
    KalmanInit(myLight.kalman);
#endif
	LIGHTTask(myLight);

	return;
}		//LIGHTInit

#ifdef __HTSMUX_SUPPORT__
/**
*	 This function initializes the color sensor.
*/
void
LIGHTInit(
LIGHT &myLight,
tMUXSensor sensorID,
bool bLightOn,
int options = 0
)
{
	myLight.sensorID = (tSensors) sensorID;
	myLight.options  = options;
	myLight.flags    = LIGHT_HTSMUX;
	myLight.lightVal     = 0;
	if (bLightOn)
	{
		SetLightON(myLight);	// turn on the light sensor LED
	}
	else
	{
		SetLightOFF(myLight);	// turn off the light sensor LED
	}
#ifdef _KALMAN_H
    KalmanInit(myLight.kalman);
#endif

	LIGHTTask(myLight);

	return;
}		//LIGHTInit


#endif

#endif
