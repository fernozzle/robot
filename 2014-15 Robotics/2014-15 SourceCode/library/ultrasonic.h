// ultrasonic.h
//			This contains the library functions for the ultrasonic sensor.


#ifndef _ULTRASONIC_H
#define _ULTRASONIC_H

#include "drivers\lego-ultrasound.h"

//
// Constants.
//
#define ULTRASONIC_HTSMUX				 0x0001
const int ULTRASONIC_INFINITY = 255;
//
// Macros.
//

/**
*	 This macro returns the value from the ULTRASONIC sensor.
*
*	 @param i Points to the ULTRASONIC structure.
*
*	 @return Returns the ULTRASONIC sensor value.
*/
#ifdef __HTSMUX_SUPPORT__
#define ReadUltrasonic(r)								(((r).flags & ULTRASONIC_HTSMUX)?			 \
																				 USreadDist((tMUXSensor)((r).sensorID)):\
																				 USreadDist((tSensors)((r).sensorID)))
#else
#define ReadUltrasonic(r)								USreadDist((tSensors)((r).sensorID)):
#endif


/**
*	 This macro returns the distance value of the ULTRASONIC sensor.
*
*	 @param i Points to the ULTRASONIC Sensor structure.
*
*	 @return Returns the AC direction value.
*/
#define ultrasonicGetDist(i)				 ((i).dist)

//
// Type definitions.
//
typedef struct
{
	tSensors sensorID;
	int      options;
	int      flags;
	int      dist;
} ULTRASONIC;


/**
*	 This function is called periodically to read the ULTRASONIC sensor and update
*	 the various values.
*
*	 @param myUltrasonic Points to the ULTRASONIC structure.
*/
//	2014Dec22> We ran out of subroutines with RobotC 4.27, so change this to a #define hopefully next year this won't be a limit
#define ULTRASONICTask(myUltrasonic) myUltrasonic.dist = ReadUltrasonic(myUltrasonic)
/*
void
ULTRASONICTask(
ULTRASONIC &myUltrasonic
)
{
	myUltrasonic.dist = ReadUltrasonic(myUltrasonic);

	return;
}		//ULTRASONICTask
*/

/**
*	 This function initializes the ULTRASONIC sensor.
*
*	 @param ultrasonic Points to the ULTRASONIC structure.
*	 @param sensorID Specifies the ID of the ULTRASONIC sensor.
*	 @param options Optionally specifies the ULTRASONIC options:
*/
void
ULTRASONICInit(
ULTRASONIC &myUltrasonic,
tSensors sensorID,
int options = 0
)
{
	myUltrasonic.sensorID = sensorID;
	myUltrasonic.options  = options;
	myUltrasonic.flags    = 0;
	myUltrasonic.dist     = 0;

	ULTRASONICTask(myUltrasonic);

	return;
}		//ULTRASONICInit

#ifdef __HTSMUX_SUPPORT__
/**
*	 This function initializes the ultrasonic sensor.
*
*	 @param ultrasonic Points to the ultrasonic structure.
*	 @param sensorID   Specifies the sensor MUX ID of the ultrasonic sensor.
*	 @param options    Optionally specifies the ultrasonic options:
*/
void
ULTRASONICInit(
ULTRASONIC &myUltrasonic,
tMUXSensor sensorID,
int options = 0
)
{
	myUltrasonic.sensorID = (tSensors) sensorID;
	myUltrasonic.options  = options;
	myUltrasonic.flags    = ULTRASONIC_HTSMUX;
	myUltrasonic.dist     = 0.0;

	ULTRASONICTask(myUltrasonic);

	return;
}		//ULTRASONICInit


#endif

#endif
