// color.h
//			This contains the library functions for the color sensor.


#ifndef _COLOR_H
#define _COLOR_H

#include "..\drivers\ hitechnic-colour-v2.h"

//
// Constants.
//
#define COLOR_HTSMUX				 0x0001
const int COLOR_INFINITY = 255;
//
// Macros.
//

/**
*	 This macro returns the value from the COLOR sensor.
*
*	 @param i Points to the COLOR structure.
*
*	 @return Returns the COLOR sensor value.
*/
#ifdef __HTSMUX_SUPPORT__
#define ReadColor(r)								(((r).flags & COLOR_HTSMUX)?			 \
																				 USreadDist((tMUXSensor)((r).sensorID)):\
																				 USreadDist((tSensors)((r).sensorID)))
#else
#define ReadColor(r)								USreadDist((tSensors)((r).sensorID)):
#endif


/**
*	 This macro returns the distance value of the COLOR sensor.
*
*	 @param i Points to the COLOR Sensor structure.
*
*	 @return Returns the AC direction value.
*/
#define colorGetDist(i)				 ((i).dist)

//
// Type definitions.
//
typedef struct
{
	tSensors sensorID;
	int      options;
	int      flags;
	int      colorVal;
} COLOR;


/**
*	 This function is called periodically to read the COLOR sensor and update
*	 the various values.
*
*	 @param color Points to the COLOR structure.
*/
//	2014Dec22> We ran out of subroutines with RobotC 4.27, so change this to a #define hopefully next year this won't be a limit
#define colorTask(c) c.colorVal = ReadColor(c)
/*
void
colorTask(
COLOR &myColor
)
{
	myColor.colorVal = ReadColor(myColor);

	return;
}		//COLORTask
*/

/**
*	 This function initializes the COLOR sensor.
*
*	 @param color Points to the COLOR structure.
*	 @param sensorID Specifies the ID of the COLOR sensor.
*	 @param options Optionally specifies the COLOR options:
*/
void
COLORInit(
COLOR &myColor,
tSensors sensorID,
int options = 0
)
{
	myColor.sensorID = sensorID;
	myColor.options  = options;
	myColor.flags    = 0;
	myColor.dist     = 0;

	colorTask(myColor);

	return;
}		//COLORInit

#ifdef __HTSMUX_SUPPORT__
/**
*	 This function initializes the color sensor.
*
*	 @param color Points to the color structure.
*	 @param sensorID   Specifies the sensor MUX ID of the color sensor.
*	 @param options    Optionally specifies the color options:
*/
void
COLORInit(
COLOR &myColor,
tMUXSensor sensorID,
int options = 0
)
{
	myColor.sensorID = (tSensors) sensorID;
	myColor.options  = options;
	myColor.flags    = COLOR_HTSMUX;
	myColor.dist     = 0.0;

	colorTask(color);

	return;
}		//COLORInit


#endif

#endif
