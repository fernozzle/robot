//
// eopd.h
//     This  contains the library functions for the IR Seeker sensor.

#ifndef _EOPD_H
#define _EOPD_H

#include "drivers\hitechnic-eopd.h"

//
// Constants.
//
#ifdef _KALMAN_H
#define EOPD_FILTER        0x0001
#endif
#define EOPD_HTSMUX        0x0001
#define EOPDF_LOW_GAIN     1
#define EOPDF_HIGH_GAIN     2


#define DefEOPDCalLowGain  10.0		// default low gain scaling factor
#define DefEOPDCalHiGain 10.0		// default high gain scaling factor
#define KALMAN_FILTER_GAIN_LIMIT 10.0	// don't apply filter unless value is <= this
#define EOPD_INFINITE 100.0			// if EOPD is reading an infinite value, use this instead
//
// Macros.
//

/**
*  This macro returns the raw value from the EOPD sensor.
*
*  @param i Points to the EOPD structure.
*
*  @return Returns the raw EOPD sensor value.
*/
#ifdef __HTSMUX_SUPPORT__
#define EOPDGetRawDist(i) (((i).flags & EOPD_HTSMUX)? \
HTEOPDreadRaw((tMUXSensor)((i).sensorID)): \
HTEOPDreadRaw((i).sensorID))
#else
#define EOPDGetRawDist(i)  HTEOPDreadRaw((i).sensorID)
#endif

/**
*  This macro returns the proccessed value from the EOPD sensor.
*
*  @param i Points to the EOPD structure.
*
*  @return Returns the raw EOPD sensor value.
*/
#ifdef __HTSMUX_SUPPORT__
#define EOPDGetProcDist(i) (((i).flags & EOPD_HTSMUX)? \
HTEOPDreadProcessed((tMUXSensor)((i).sensorID)): \
HTEOPDreadProcessed((i).sensorID))
#else
#define EOPDGetProcDist(i)  HTEOPDreadProcessed((i).sensorID)
#endif

/**
*  This macro returns the distance value of the EOPD sensor.
*
*  @param i Points to the EOPDSensor structure.
*
*  @return Returns the AC direction value.
*/
#define EOPDGetDist(i)         ((i).dist)

//
// Type definitions.
//
typedef struct
{
	tSensors    sensorID;
	int         gain;   	// low or high gain
	float				calLowGain;	// calibration factor for low gain operation
	float				calHiGain;	// calibration factor for high gain operation
	int					options;
	int         flags;
	float       dist;
#ifdef _KALMAN_H
    KALMAN      kalman;
#endif
} EOPD;

/**
*  This function is called to set the gain of the EOPD sensor.
*
*  @param eopd points to the EOPD structure.
*/
void
EOPDSetGain(
EOPD &eopd,
int gain
)
{

	eopd.gain = gain;
	if (EOPDF_LOW_GAIN == gain)
	{
		if (eopd.flags & EOPD_HTSMUX)
		{
			HTEOPDsetShortRange((tMUXSensor) eopd.sensorID);
		}
		else
		{
			HTEOPDsetShortRange(eopd.sensorID);
		}
	}
	else
	{
		if (eopd.flags & EOPD_HTSMUX)
		{
			HTEOPDsetLongRange((tMUXSensor) eopd.sensorID);
		}
		else
		{
			HTEOPDsetLongRange(eopd.sensorID);
		}
	}
	return;
}   //EOPDSetGain

/**
*  This function is called periodically to read the EOPD sensor and update
*  the various values.
*
*  @param eopd Points to the EOPD structure.
*/
void
EOPDTask(
EOPD &eopd
)
{
	float procDist = EOPDGetProcDist(eopd);	// get processed distance
	// check for infinite (i.e. =0)
	if (0 == procDist)
	{
		eopd.dist = EOPD_INFINITE;
	}
  else	if (EOPDF_LOW_GAIN == eopd.gain)
	{
		eopd.dist = eopd.calLowGain / procDist;	// calculate low gain distance
	}
	else
	{
		eopd.dist = eopd.calHiGain / procDist;	// calculate high gain distance
	}
	if ((eopd.options & EOPD_FILTER) && (eopd.dist <= KALMAN_FILTER_GAIN_LIMIT))
	{
		double dblDist = eopd.dist;
		//writeDebugStreamLine("dblDist: %.2f", dblDist);
		eopd.dist = (float)KalmanFilter(eopd.kalman,dblDist);
	}
	return;
}   //EOPDTask

/**
*  This function initializes the EOPD sensor.
*
*  @param eopd Points to the EOPD structure.
*  @param sensorID Specifies the ID of the EOPD sensor.
*  @param options Optionally specifies the EOPD options:
*/
void
EOPDInit(
EOPD &eopd,
tSensors sensorID,
int gain,
float calLowGain,
float calHiGain,
int options = 0
)
{
	eopd.sensorID = sensorID;
	eopd.gain = gain;
	eopd.calLowGain = calLowGain;
	eopd.calHiGain = calHiGain;
	eopd.options = options;
	eopd.flags = 0;
	eopd.dist = 0.0;

	EOPDSetGain(eopd,eopd.gain);
#ifdef _KALMAN_H
    KalmanInit(eopd.kalman);
#endif
	EOPDTask(eopd);

	return;
}   //EOPDInit

#ifdef __HTSMUX_SUPPORT__
/**
*  This function initializes the EOPD sensor.
*
*  @param eopd Points to the EOPD structure.
*  @param sensorID Specifies the sensor MUX ID of the EOPD sensor.
*  @param options Optionally specifies the EOPD options:
*/
void
EOPDInit(
EOPD &eopd,
tMUXSensor sensorID,
int gain,
float calLowGain,
float calHiGain,
int options = 0
)
{
	eopd.sensorID = (tSensors) sensorID;
	eopd.gain = gain;
	eopd.calLowGain = calLowGain;
	eopd.calHiGain = calHiGain;
	eopd.options = options;
	eopd.flags = EOPD_HTSMUX;
	eopd.dist = 0.0;

	EOPDSetGain(eopd,eopd.gain);
#ifdef _KALMAN_H
    KalmanInit(eopd.kalman);
#endif
	EOPDTask(eopd);

	return;
}   //EOPDInit


#endif

#endif  //ifndef _IRSEEKER_H
