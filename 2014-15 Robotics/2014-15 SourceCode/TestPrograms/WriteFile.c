task main()
{
	TFileHandle hFileHandle;
	TFileIOResult nIoResult;
	short nFileSize = 16;	// We are only writing 2 shorts (2 * 2 bytes each), but make this a little bigger
	const string testFilename = "testFile1.dat";
	// Delete the old data file and open a new one for writing
	Delete(testFilename, nIoResult);
	OpenWrite(hFileHandle, nIoResult, testFilename, nFileSize);
	if (nIoResult != ioRsltSuccess) {
		Close(hFileHandle, nIoResult);
		eraseDisplay();
		nxtDisplayTextLine(3, "Err:CreateFile");
		playSound(soundException);
		while(bSoundActive) EndTimeSlice();
		wait1Msec(5000);
		stopAllTasks();
	}
	else
	{
		short liftState = 4;		// robot lift state 0=stowed, 2=60cm 3=90cm 4=120cm
		short tubeStowed = 0;	// =1 if tube is stowed =0 if deployed
		short ballGrabberStowed = 1; // =1 if ball grabber is stowed =0 if deployed
		WriteShort(hFileHandle, nIoResult, liftState);
		WriteShort(hFileHandle, nIoResult, tubeStowed);
		WriteShort(hFileHandle, nIoResult, ballGrabberStowed);
		Close(hFileHandle,nIoResult);
	}

}
