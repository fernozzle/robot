task main()
{
	TFileHandle hFileHandle;
	TFileIOResult nIoResult;
	short nFileSize = 16;	// We are only writing 2 shorts (2 * 2 bytes each), but make this a little bigger
	const string testFilename = "testFile1.dat";
  OpenRead(hFileHandle, nIoResult, testFilename, nFileSize);
  if (nIoResult != ioRsltSuccess) {
    Close(hFileHandle, nIoResult);
    eraseDisplay();
    playSound(soundException);
    while(bSoundActive) EndTimeSlice();
    wait1Msec(5000);
    stopAllTasks();
  }
  else
  {
	  short liftState = -1;	// robot lift state 0=stowed, 2=60cm 3=90cm 4=120cm
	  string liftStateStr = "";
	  short tubeStowed = -1;	// =1 if tube is stowed =0 if deployed
	  string tubeStateStr = "";
	  short ballGrabberStowed= -1; // =1 if ball grabber is stowed =0 if deployed
	  string ballGrabberStateStr = "";
	  ReadShort(hFileHandle, nIoResult, liftState);
	  ReadShort(hFileHandle, nIoResult, tubeStowed);
	  ReadShort(hFileHandle, nIoResult, ballGrabberStowed);
		Close(hFileHandle,nIoResult);
		switch(liftState) {
		case 0:
			liftStateStr = "Lift:Stowed";
			break;
		case 2:
			liftStateStr = "Lift:60cm";
			break;
		case 3:
			liftStateStr = "Lift:90cm";
			break;
		case 4:
			liftStateStr = "Lift:120cm";
			break;
		default:
			liftStateStr = "Lift:Unknown";
			break;
		}
		if (1 == tubeStowed)
		{
			tubeStateStr = "Tube:Stowed";
		}
		else
		{
			tubeStateStr = "Tube:Deployed";
		}
		if (1 == ballGrabberStowed)
		{
			ballGrabberStateStr = "BallGrabber:Stowed";
		}
		else
		{
			ballGrabberStateStr = "BallGrabber:Deployed";
		}
		eraseDisplay();
    nxtDisplayTextLine(3, liftStateStr);
    nxtDisplayTextLine(4, tubeStateStr);
    nxtDisplayTextLine(5, ballGrabberStateStr);

  }
  while(true){}
}
