#pragma config(Sensor, S1,     HTMUX1,         sensorI2CCustom)
#pragma config(Sensor, S2,     HTMUX2,         sensorI2CCustom)
#pragma config(Hubs,  S3, HTMotor,  HTMotor,  HTServo,  none)
#pragma config(Hubs,  S4, HTMotor,  HTMotor,  HTServo,  none)
#pragma config(Sensor, S3,     ,               sensorI2CMuxController)
#pragma config(Sensor, S4,     ,               sensorI2CMuxController)
#pragma config(Motor,  mtr_S3_C1_1,     leftFrontMotorID, tmotorTetrix, PIDControl, reversed, encoder)
#pragma config(Motor,  mtr_S3_C1_2,     leftRearMotorID, tmotorTetrix, PIDControl, reversed, encoder)
#pragma config(Motor,  mtr_S3_C2_1,     tubeReturnMotorID, tmotorTetrix, PIDControl, encoder)
#pragma config(Motor,  mtr_S3_C2_2,     ballGrabberArmMotorID, tmotorTetrix, PIDControl, reversed, encoder)
#pragma config(Motor,  mtr_S4_C1_1,     rightFrontMotorID, tmotorTetrix, PIDControl, encoder)
#pragma config(Motor,  mtr_S4_C1_2,     rightRearMotorID, tmotorTetrix, PIDControl, encoder)
#pragma config(Motor,  mtr_S4_C2_1,     liftMotorID,   tmotorTetrix, PIDControl, encoder)
#pragma config(Motor,  mtr_S4_C2_2,     nomotor,       tmotorTetrix, openLoop)
#pragma config(Servo,  srvo_S3_C3_1,    leftGoalGrabberArmServoID, tServoStandard)
#pragma config(Servo,  srvo_S3_C3_2,    leftGoalStabilizerServoID, tServoStandard)
#pragma config(Servo,  srvo_S3_C3_3,    tubeTipperServoID,    tServoStandard)
#pragma config(Servo,  srvo_S3_C3_4,    parkingBrakeServoID,  tServoStandard)
#pragma config(Servo,  srvo_S3_C3_5,    servo5,               tServoNone)
#pragma config(Servo,  srvo_S3_C3_6,    servo6,               tServoNone)
#pragma config(Servo,  srvo_S4_C3_1,    rightGoalGrabberArmServoID, tServoStandard)
#pragma config(Servo,  srvo_S4_C3_2,    liftBrakeServoID,     tServoStandard)
#pragma config(Servo,  srvo_S4_C3_3,    ballGrabberJawsServoID, tServoStandard)
#pragma config(Servo,  srvo_S4_C3_4,    rightGoalStabilizerServoID, tServoStandard)
#pragma config(Servo,  srvo_S4_C3_5,    servo11,              tServoNone)
#pragma config(Servo,  srvo_S4_C3_6,    servo12,              tServoNone)
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//

// NOTE: Sensors on ports 1&2 instead of motor and servo controllers (now on 3&4) seems to have solved the communication problems with the controllers

/////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////

#include "JoystickDriver.c"	 //Include file to "handle" the Bluetooth messages.
#include "Robot\standardServo.h"		// for standard servo

////////////////////////
//
//	Constants
//

/////////////////////////////////////////////////////////////////////////////////////////////////////
//
//																		initializeRobot
//
// Prior to the start of tele-op mode, you may want to perform some initialization on your robot
// and the variables within your program.
//
// In most cases, you may not have to add any code to this function and it will remain "empty".
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

void initializeRobot()
{
	// Place code here to sinitialize servos to starting positions.
	// Sensors are automatically configured and setup by ROBOTC. They may need a brief time to stabilize.
	return;
}


task main(){

	STANDARD_SERVO parkingBrakeServo;
	initializeRobot();
	//
	// Test each servo on the robot
	//
//
// Brake
//
const int ParkingBrakeOnPos = 216;
const int ParkingBrakeOffPos = 255;

	SERVOInit(parkingBrakeServo,parkingBrakeServoID,ParkingBrakeOffPos,true);

	//waitForStart();		// wait for start of tele-op phase

	while (true){
		getJoystickSettings(joystick);

		if (joy1Btn(Btn8)) {
			int newServoPos = GetServoPosition(parkingBrakeServo) + 1;
			SetServoPosition(parkingBrakeServo,newServoPos);
		}
		if(joy1Btn(Btn6)) {
			int newServoPos = GetServoPosition(parkingBrakeServo) - 1;
			SetServoPosition(parkingBrakeServo,newServoPos);
		}
		if (joy1Btn(Btn7)) {
			SetServoPosition(parkingBrakeServo,ParkingBrakeOnPos);
		}
		if(joy1Btn(Btn5)) {
			SetServoPosition(parkingBrakeServo,ParkingBrakeOffPos);
		}

		SERVOTask(parkingBrakeServo);
		int servoPos = GetServoPosition(parkingBrakeServo);
  	writeDebugStreamLine("parkingBrakeServoPos: %d",servoPos);
		wait1Msec(100);	// wait so we can adjust opening by 1 at a time

	}
}
