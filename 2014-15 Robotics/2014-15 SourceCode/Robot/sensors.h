// Sensors.h
//
//	Include files
//
#include "..\library\macrodefs.h" // macro definitions
#include "..\library\irseeker.h"
#include "..\library\eopd.h"
#include "..\library\ultrasonic.h"
#include "..\library\light.h"
#include "..\library\gyro.h"

//
//	Left Side MUX Sensors
//
const tMUXSensor DownEOPDSensorID  = msensor_S1_1; // EOPD pointing down
const tMUXSensor UltraSonicSensorID = msensor_S1_2; // Ultrasonic Sensor
const tMUXSensor LightSensorID = msensor_S1_3; // light
const tMUXSensor ForwardEOPDSensorID  = msensor_S1_4; // EOPD pointing forward
//
//	Right Side MUX Sensors
//
const tMUXSensor IRSeekerID = msensor_S2_4; // IR sensor on the right side of the robot
const tMUXSensor gyroSensorID = msensor_S2_1; // gyro

//
//	Sensors Object
//
typedef struct
{
	EOPD rsEOPDForward;
	EOPD rsEOPDDown;
	IRSEEKER rsIRSeeker;
	ULTRASONIC rsUltra;
	LIGHT rsLight;
	GYRO rsGyro;
} SENSORS;

//
//	Macros to execute tasks one at a time - use these if you don't need all the sensors updated
//
#define SENSORS_GYROTask(s) GYROTask(s.rsGyro)
#define SENSORS_IRSeekerTask(s) IRSeekerTask(s.rsIRSeeker)
#define SENSORS_EOPDDownTask(s) EOPDTask(s.rsEOPDDown)
#define SENSORS_ULTRASONICTask(s) ULTRASONICTask(s.rsUltra)
#define SENSORS_LIGHTTask(s) LIGHTTask(s.rsLight)
#define SENSORS_EOPDForwardTask(s) EOPDTask(s.rsEOPDForward)

void SENSORSTask(SENSORS &mySensors)
{
	SENSORS_GYROTask(mySensors);
	SENSORS_IRSeekerTask(mySensors);
	SENSORS_EOPDDownTask(mySensors);
	SENSORS_ULTRASONICTask(mySensors);
	SENSORS_LIGHTTask(mySensors);
	SENSORS_EOPDForwardTask(mySensors);
}

void SENSORSInit(SENSORS &mySensors)
{
//
//	Right Side Sensors
//
	writeDebugStreamLine("initializing Gyro");	// do gyro first-> it has to be calibrated, so give it the most chance to stabilize
	GYROInit(mySensors.rsGyro,gyroSensorID,true,GYROF_HTSMUX);	// gyro is mounted upside down, so left & right are reversed
	writeDebugStreamLine("Initializing IR");
	IRSeekerInit(mySensors.rsIRSeeker,IRSeekerID,IRSEEKERO_FILTER);	// use filter
//
//	Left side sensors
//
	writeDebugStreamLine("initializing EOPD Down");
	EOPDInit(mySensors.rsEOPDDown,DownEOPDSensorID,EOPDF_HIGH_GAIN,DefEOPDCalLowGain,DefEOPDCalHiGain,0);	// use high gain and default scaling factors to start
	writeDebugStreamLine("initializing Ultrasonic");
	ULTRASONICInit(mySensors.rsUltra,UltraSonicSensorID,0);
	writeDebugStreamLine("initialzing Light Sensor");
	LIGHTInit(mySensors.rsLight,LightSensorID,true,LIGHT_FILTER);	// light sensor is buried in robot, so turn the LED on & use filter
	writeDebugStreamline("initializing EOPD Forward");
	EOPDInit(mySensors.rsEOPDForward,ForwardEOPDSensorID,EOPDF_HIGH_GAIN,DefEOPDCalLowGain,DefEOPDCalHiGain,0);	// use high gain and default scaling factors to start
}
