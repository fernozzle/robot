// encapsulates properties/functions of a motor
// TODO: motor stalled

#ifndef _STANDARD_MOTOR_H
#define _STANDARD_MOTOR_H

//
// Constants
//
const float DistancePerRev4InchWheel = 4.0 * PI;	// (12.566 inches) - 1:1 ratio
#define TetrixEncoderTicksPerRev 1440.0		// 1440 encoder ticks per revolution of a Tetrix motor
#define AndyMarkEncoderTicksPerRev 1120.0	// 1120 encoder ticks per revoultion for an AndyMark Motor
const long TetrixTicksPerInch4InchWheel = ceil(TetrixEncoderTicksPerRev / DistancePerRev4InchWheel);	// encoder tix per inch for a 4 inch wheel driven at 1:1 ratio with Tetrix motor
const long AndyMarkTicksPerInch4InchWheel = ceil(TetrixEncoderTicksPerRev / AndyMarkEncoderTicksPerRev);	// encoder tix per inch for a 4 inch wheel driven at 1:1 ratio with AndyMark motor
#define DefaultMotorSmoothFactor 50	// Range: 0-100 0 = no smoothing, 100 = no movement
#define NoMotorSmoothing  0						// use this value if you don't want smoothing
#define FullDriveSpeed 1.0					// full speed
#define DefaultSlowDriveSpeed 0.2	// slower driving
#define MaximumMotorPowerTetrix 100				// maximum motor power for a tetrix motor
#define MaximumMotorPowerAndyMark 78				// maximum motor power for a Andy Mark motor - this is only 78 because the encoder for AndyMark motors returns less counts per revolution
#define MoveToTargetSlowDownRange 500		// when we get this close to the target, slow down
#define MoveToTargetSlowPower 15   	// slow down when we get close to target to not go over
#define MoveToTargetSlowPowerAndyMark 10   	// For andyMark motor
#define CheckStallInterval 25				// Don't check for stalling more often than once than this number of ms
#define StallMinDistance 5					// if the distance hasn't changed by more than this, then say the motor is stalled

typedef enum {
	MotorStop,
	MotorForward,
	MotorReverse,
	MotorForwardToTarget,
	MotorReverseToTarget,
	MotorStalled
} MotorState;

typedef struct {
	int motorID;
	long targetEncoderValue;
	long actualEncoderValue;
	long prevActualEncoderValue;
	int targetPower;
	int actualPower;	// smoothed
	int actualSpeed;	// revs/sec
	int prevTime;
	int drivingSmoothFactor;	// Range: 0-100 0 = no smoothing, 100 = no movement
	float driveSpeed;					// driving speed used for slower, more precise driving 0.0 = no movement, 100 = full speed. A good value is DefaultSlowDriveSpeed for slow driving
	int lastDistCheckTime;		// the last time we checked for the distance travelled
	int moveToStallTimeLimit;	// the maximum number of ms to run the motor until stall (0 = don't check this)
	int moveToStallStartTime;	// The start time of trying to run the motor until stalling
	int lastStallCheckTime;		// The last time we checked the stall time
	long prevStallCheckEncoderValue;	// this is the motor encoder value the last time we checked for stalling
	MotorState state;
} STANDARD_MOTOR;

//
//	Distance to Encoder Tick Conversion
//
//long TetrixMotorDistInchToEncoderTick(float distInches)
//{
//	float fltTicks = distInches * TetrixTicksPerInch4InchWheel;
//	return (long) ceil(fltTicks);	// next largest integer
//}

//
//	AndyMark motor power adjustment -> maximum power for an AndyMark motor under PID control (with motor encoder wire) is 78 since
//		it puts out fewer pulses per revolution
//
int AndyMarkPowerAdjust(int power) {
	if (power > MaximumMotorPowerAndyMark)
	{
		power = MaximumMotorPowerAndyMark;	// okay to modify since we got a copy
	}
	return power;
}

//
// Motor Encoder
//
//	2014Dec22> We ran out of subroutines with RobotC 4.27, so change these to a #define hopefully next year this won't be a limit
#define UpdateMotorEncoderPosition(myMotor) myMotor.actualEncoderValue = nMotorEncoder[myMotor.motorID]

/*
void UpdateMotorEncoderPosition(STANDARD_MOTOR &myMotor) {
myMotor.actualEncoderValue = nMotorEncoder[myMotor.motorID];
}
*/

#define GetMotorEncoderPosition(myMotor) myMotor.actualEncoderValue	// this gets updated in MOTORTask

/*
long GetMotorEncoderPosition(STANDARD_MOTOR &myMotor) {
return myMotor.actualEncoderValue;	// this gets updated in MOTORTask
}
*/

void SetMotorEncoderPosition(STANDARD_MOTOR &myMotor, long newPos) {
	myMotor.actualEncoderValue = newPos;
	if (0 == newPos)
	{	// only set this if 0
		nMotorEncoder[myMotor.motorID] = newPos;	// you can't set the encoder to an arbirtary value, if you set it, it is set to zero
	}
	//writeDebugStreamLine("SetMEnPos %d",nMotorEncoder[myMotor.motorID]);
}

#define ResetMotorEncoderPosition(myMotor) SetMotorEncoderPosition(myMotor,0)
/*
void ResetMotorEncoderPosition(STANDARD_MOTOR &myMotor) {
SetMotorEncoderPosition(myMotor,0);
}
*/

/////////////////////////

#define GetMotorState(myMotor) myMotor.state
/*
MotorState GetMotorState(STANDARD_MOTOR &myMotor) {
return myMotor.state;
}
*/

bool IsMotorMoving(STANDARD_MOTOR &myMotor) {
	MotorState state = GetMotorState(myMotor);
	if ((MotorStop == state) || (MotorStalled == state))
	{
		return false;
	}
	else
	{
		return true;
	}
}
bool IsMotorStopped(STANDARD_MOTOR &myMotor) {
	return !IsMotorMoving(myMotor);
}

void UpdateMotorState(STANDARD_MOTOR &myMotor, int power, bool bMovingToTarget)
{
	if (power > 0)
	{
		if (bMovingToTarget)
		{
			myMotor.state = MotorForwardToTarget;
		}
		else
		{
			myMotor.state = MotorForward;
		}
	}
	else if (power < 0)
	{
		if (bMovingToTarget)
		{
			myMotor.state = MotorReverseToTarget;
		}
		else
		{
			myMotor.state = MotorReverse;
		}
	}
	else
	{
		myMotor.state = MotorStop;
	}
}

// stops the motor
void MOTORStop(STANDARD_MOTOR &myMotor) {
	//writeDebugStreamLine("MOTOR>STOP");
	motor[myMotor.motorID] = 0;	// stop the motor the first thing!
	myMotor.targetPower = 0;
	myMotor.actualPower = 0;
	myMotor.actualSpeed = 0;
	myMotor.moveToStallTimeLimit = 0;	// no longer checking for stalling
	myMotor.state = MotorStop;
}

void MOTORSetPower(STANDARD_MOTOR &myMotor, int power) {
	myMotor.actualPower = power;
	motor[myMotor.motorID] = myMotor.actualPower;	// set motor power now
}

// sets the motor to target power
//	If drivingSmoothFactor !=  NoMotorSmoothing, the actual power is calculated and sent to the motor in the MOTORTask
void MOTORMove(STANDARD_MOTOR &myMotor, int targetPower) {
	myMotor.targetPower = targetPower;
	if (NoMotorSmoothing == myMotor.drivingSmoothFactor) {
		int power = myMotor.targetPower;		// no smoothing
		power *= myMotor.driveSpeed;		// modify by the driving speed
		MOTORSetPower(myMotor,power);
	}
	UpdateMotorState(myMotor,myMotor.targetPower,false);	// update state - not moving towards target
}

//
//	Move Motor to Target
//
// 	Sets the motor target encoder value
// 	Once it reaches that target it will shut off
// 	Sets the motor to the target power (-100 to 100)
//	If target is less than current encoder value, the motor will automatically move in reverse
//
void MOTORMoveToTarget(STANDARD_MOTOR &myMotor, int targetPower, long targetPos) {
	myMotor.targetEncoderValue = targetPos;
	UpdateMotorEncoderPosition(myMotor);	// make sure this is up to date
	writeDebugStreamLine("MOTORMoveToTarget> target: %d > current: %d",myMotor.targetEncoderValue,myMotor.actualEncoderValue);
	if (myMotor.targetEncoderValue < myMotor.actualEncoderValue) {
		targetPower = -abs(targetPower);	// this is okay since we got a copy of it; make sure it is negative
	}
	else if(myMotor.targetEncoderValue == myMotor.actualEncoderValue) {
		targetPower = 0;	// already there
	}
	else
	{
		targetPower = abs(targetPower);	// this is okay since we got a copy of it; make sure it is positive
	}
	writeDebugStreamLine("MOTORMoveToTarget> power: %d > pos: %d",targetPower, targetPos);
	MOTORMove(myMotor,targetPower);
	UpdateMotorState(myMotor,myMotor.targetPower,true);	// We need to override the state - we are moving towards target
	if (IsMotorMoving(myMotor))
	{
		//writeDebugStreamLine("MOTORMoveToTarget> Motor is moving!");
	}
}

// updates motor values
void MOTORTask(STANDARD_MOTOR &myMotor) {
	// update encoder status every
	long distTraveled = myMotor.actualEncoderValue - myMotor.prevActualEncoderValue;	// we shouldn't be calculating this too often
	myMotor.prevActualEncoderValue = myMotor.actualEncoderValue;	// save previous value
	myMotor.actualEncoderValue = nMotorEncoder[myMotor.motorID];

	// update move to target status
	if (MotorForwardToTarget == myMotor.state)
	{
		long diff =  myMotor.targetEncoderValue - myMotor.actualEncoderValue;
		if (diff <= 0)
		{
			MOTORStop(myMotor);	// we reached the target
		}
		else if(diff < MoveToTargetSlowDownRange)
		{
			if (NoMotorSmoothing == myMotor.drivingSmoothFactor)
			{
				myMotor.actualPower = MoveToTargetSlowPower;	// slow down when we get close if there is no smoothing
			}
		}
	}
	else if (MotorReverseToTarget == myMotor.state)
	{
		long diff =  myMotor.actualEncoderValue - myMotor.targetEncoderValue;
		//writeDebugStreamLine("Motor>Rev> cur: %d target: %d",myMotor.actualEncoderValue,myMotor.targetEncoderValue);
		if (diff <= 0)
		{
			MOTORStop(myMotor);	// we reached the target
		}
		else if(diff < MoveToTargetSlowDownRange)
		{
			if (NoMotorSmoothing == myMotor.drivingSmoothFactor)
			{
				myMotor.actualPower = -MoveToTargetSlowPower;	// slow down when we get close if there is no smoothing
			}
		}
	}

	// calculate the actual power based on smoothing & drive speed and set the motor power if necessary
	if (myMotor.actualPower != myMotor.targetPower) {
		int power = ((myMotor.actualPower * myMotor.drivingSmoothFactor) + ((myMotor.targetPower)*myMotor.driveSpeed*(100-myMotor.drivingSmoothFactor)))/100;
		MOTORSetPower(myMotor,power);
	}

	// set the motor to stopped if myMotor.actualPower = 0
	if (0 == myMotor.actualPower) {
		//writeDebugStreamLine("MOTOR>ActualPower=0>Stopping");
		MOTORStop(myMotor);	// this will set all our state information
	}

	// see if the motor is stalled
	if (IsMotorMoving(myMotor)) {
		if (0 == distTraveled) {
			// we need to make sure it is not moving for a while (a few seconds) before shutting it down
		}
	}
}

void SetMotorDriveSpeed(STANDARD_MOTOR &myMotor, float driveSpeed) {
	myMotor.driveSpeed = driveSpeed;
}

void SetMotorDrivingSmoothFactor(STANDARD_MOTOR &myMotor, int smoothFactor) {
	myMotor.drivingSmoothFactor = smoothFactor;
}

// initializes the motor
void MOTORInit(STANDARD_MOTOR &myMotor, int motorID, int drivingSmoothFactor, float driveSpeed, bool bResetEncoder = true) {
	myMotor.motorID = motorID;
	MOTORStop(myMotor);	// stop the motor first thing
	myMotor.targetEncoderValue = 0;	// if the motor encoder target is zero, the motor will run forever
	if (bResetEncoder)
	{
		ResetMotorEncoderPosition(myMotor);	// reset motor position if asked
	}
	myMotor.actualEncoderValue = nMotorEncoder[myMotor.motorID];
	myMotor.prevActualEncoderValue = 0;
	myMotor.prevTime = 0;
	SetMotorDrivingSmoothFactor(myMotor,drivingSmoothFactor);
	SetMotorDriveSpeed(myMotor,driveSpeed);
	myMotor.moveToStallTimeLimit = 0;
	MOTORTask(myMotor);
}

/////////////////////////////////////////////////////////
//
// Test Motor
//
/*
void TestMOTOR(int motorID) {
const long TestDuration = 3000;	// run each test this many milliseconds
const long TimeBetweenTests = 1000;	// delay this many milliseconds between tests
STANDARD_MOTOR myMotor;
int drivingSmoothFactor = NoMotorSmoothing;
float driveSpeed = FullDriveSpeed;
int motorPower = 0;


//writeDebugStreamLine("Initializing Motor");
MOTORInit(myMotor,motorID,drivingSmoothFactor,driveSpeed);
writeDebugStreamLine("Stopping Motor");
MOTORStop(myMotor);

wait1Msec(TestDuration);
motorPower = 25;
//writeDebugStreamLine("Motor Forward %d - No Smoothing",motorPower);
MOTORMove(myMotor,motorPower);
clearTimer(T1);
while (time1[T1] < TestDuration) {
MOTORTask(myMotor);
}
MOTORStop(myMotor);

wait1Msec(TimeBetweenTests);
motorPower = 50;
//writeDebugStreamLine("Motor Forward %d - No Smoothing",motorPower);
MOTORMove(myMotor,motorPower);
clearTimer(T1);
while (time1[T1] < TestDuration) {
MOTORTask(myMotor);
}
MOTORStop(myMotor);

wait1Msec(TimeBetweenTests);
motorPower = 75;
//writeDebugStreamLine("Motor Forward %d - No Smoothing",motorPower);
MOTORMove(myMotor,motorPower);
clearTimer(T1);
while (time1[T1] < TestDuration) {
MOTORTask(myMotor);
}
MOTORStop(myMotor);

wait1Msec(TimeBetweenTests);
motorPower = 100;
//writeDebugStreamLine("Motor Forward %d - No Smoothing",motorPower);
MOTORMove(myMotor,motorPower);
clearTimer(T1);
while (time1[T1] < TestDuration) {
MOTORTask(myMotor);
}
MOTORStop(myMotor);
//writeDebugStreamLine("All STANDARD_MOTOR tests done!");
}
*/
#endif	// _STANDARD_MOTOR_H
