#ifndef _TUBE_H
#define _TUBE_H
//
//	Includes
//
#include "standardMotor.h"	// for STANDARD_MOTOR
#include "standardServo.h"	// for STANDARD_SERVO
#include "lift.h"						// for lift state

//
//	Tube Tipper
//
//	Constants
//
#define TubeTipperStowedPos 1
#define TubeTipperTippedPos 200
//
#define TubeTipperMin TubeTipperStowedPos
#define TubeTipperMax 255

typedef enum
{
	TubeTipperStowed,
	TubeTipperTipped,
	TubeTipperManualControl
} TUBETIPPERSTATE;

//
//	Tube Return
//
// Constants
//
#define TubeReturnMotorSmoothFactor NoMotorSmoothing // we don't want any motor smoothing
#define TubeReturnMotorDriveSpeed FullDriveSpeed   //we are going to vary the power
#define TubeReturnMotorManualDriveSpeed 0.3				// slow down when moving the tube manually
#define TubeReturnStowStallCountdownStart 100  					// take this many cycles of the tube return motor not changing its encoder position to say it is stalled
#define TubeReturnStowTolerance 10		// if tube return motor encoder values are within this tolerance, then consider it stalled
//
//	New Tube Return Winch
//
#define TubeReturnLenStowed 0	// stowed. With andymark motor we can tension without burning out the motor, so make this zero
#define TubeReturnLen30cm 404	// this is the same as the lift being stowed, but of course to score we need to deploy the tube
#define TubeReturnLen60cm 650	// NOTE We may be able to make these relative to the current height of the
#define TubeReturnLen90cm 1370	// the lift. If the lift is under manual control, it will not be at these heights
#define TubeReturnLen120cm 2009
#define TubeReturnLenMax 3200	// don't go beyond this
#define TubeReturnLenTipped30cm 650	// the amount of line to let out for the tube to tip for 30cm -> only tips part way to avoid tangles
#define TubeReturnLenTipped60cm 1180	// the amount of line to let out for the tube to tip for 60cm
#define TubeReturnLenTipped90cm 1157	// the amount of line to let out for the tube to tip for 90cm
#define TubeReturnLenTipped120cm 1049	// the amount of line to let out for the tube to tip for 120cm - only tip part way because of backstop
#define TubeReturnMotorDriveMaxPower MaximumMotorPowerAndyMark		// we are using an AndyMark motor for the tube return
#define TubeReturnMotorMovePowerDeploy 24	// don't go too fast to prevent tangling
#define TubeReturnMotorMovePowerStow 30
#define TubeReturnMotorMovePowerTip 19

//
//	Old Tube Return Winch
//
/*
const long TubeReturnLenStowed = 0;	// stowed. With andymark motor we can tension without burning out the motor, so make this zero
const long TubeReturnLen30cm = 782;	// this is the same as the lift being stowed, but of course to score we need to deploy the tube
const long TubeReturnLen60cm = 1975;	// NOTE We may be able to make these relative to the current height of the
const long TubeReturnLen90cm = 2720;	// the lift. If the lift is under manual control, it will not be at these heights
const long TubeReturnLen120cm = 3966;
const long TubeReturnLenMax = 5400;	// don't go beyond this
const long TubeReturnLenTipped30cm = 800;	// the amount of line to let out for the tube to tip for 30cm
const long TubeReturnLenTipped60cm = 800;	// the amount of line to let out for the tube to tip for 60cm
const long TubeReturnLenTipped90cm = 1250;	// the amount of line to let out for the tube to tip for 90cm
const long TubeReturnLenTipped120cm = 1380;	// the amount of line to let out for the tube to tip for 120cm
const int TubeReturnMotorDriveMaxPower = MaximumMotorPowerAndyMark;		// we are using an AndyMark motor for the tube return
const int TubeReturnMotorMovePowerDeploy = 30;
const int TubeReturnMotorMovePowerStow = 39;
const int TubeReturnMotorMovePowerTip =  18; 	// go slow when tipping so we don't let out slack too fast (this is an andy mark motor)
*/

typedef enum
{
	TubeReturnStowed,
	TubeReturnDeployed,
	TubeReturnDeployedTipped,
	TubeReturnMoving,
	TubeReturnManualControl
} TUBERETURNSTATE;

typedef struct {
	STANDARD_SERVO tubeTipperServo;
	TUBETIPPERSTATE tubeTipperState;
	STANDARD_MOTOR tubeReturnMotor;
	int tubeReturnManualPower;			// how much manual power is being sent to the tube return
	TUBERETURNSTATE curTubeReturnState;
	TUBERETURNSTATE targetTubeReturnState;
	int TubeReturnStowStallCountdown;	// used to countdown the number of cycles the tube return motor encoder value hasn't changed
	long prevTubeReturnEncoderValue;	// used to see if we are stalled while moving the tube to stowed
} TUBE;

#define TubeTipperInitStowed(myTube) SERVOInit(myTube.tubeTipperServo,tubeTipperServoID,TubeTipperStowedPos,true);  // tipper stowed

void TubeTipperStow(TUBE &myTube) {	// stow the tube tipper
	SetServoPosition(myTube.tubeTipperServo,TubeTipperStowedPos);
	myTube.tubeTipperState = TubeTipperStowed;
}

void TubeTipperTip(TUBE &myTube) {	// tip the tube tipper
	SetServoPosition(myTube.tubeTipperServo,TubeTipperTippedPos);
	myTube.tubeTipperState = TubeTipperTipped;
}

void ToggleTubeTipper(TUBE &myTube) {	// toggle the tube tipper
	if (TubeTipperStowed == myTube.tubeTipperState)
	{
		TubeTipperTip(myTube);
	}
	else	// tube tipper could be under manual control, but stow it anyway
	{
		TubeTipperStow(myTube);
	}
}

void TUBEStop(TUBE &myTube) {	// stop the tube - this can happen during autonomous when there are problems
	MOTORStop(myTube.tubeReturnMotor);	// turn off tube return motor
	TubeTipperStow(myTube);	// stow the tube tipper
}

void MoveTubeTipper(TUBE &myTube, int move) {	// moves the tube tipper within its limits
	int curPos = ServoValue[myTube.tubeTipperServo];	// get current position and modify it within limits
	int newPos = curPos + move;
	newPos = BOUND(newPos,TubeTipperMin,TubeTipperMax);	// limit
	SetServoPosition(myTube.tubeTipperServo,newPos);
	myTube.tubeTipperState = TubeTipperManualControl;
	//writeDebugStreamLine("Tube tipper manaual pos: %d",newPos);
}

long GetTubeReturnTippingLen(LIFTSTATE curLiftState) {
	switch(curLiftState) {
	case LiftStowed:
		return TubeReturnLenTipped30cm;
		break;
	case LiftPos60cm:
		return TubeReturnLenTipped60cm;
		break;
	case LiftPos90cm:
		return TubeReturnLenTipped90cm;
		break;
	case LiftPos120cm:
		return TubeReturnLenTipped120cm;
		break;
	}
	return TubeReturnLenTipped30cm;	// shouldn't happen

}

TUBERETURNSTATE GetTubeReturnState(TUBE &myTube) {
	return(myTube.curTubeReturnState);
}

TUBETIPPERSTATE GetTubeTipperState(TUBE &myTube) {
	return(myTube.tubeTipperState);
}

void GetTubeState(TUBE &myTube, TUBERETURNSTATE &curTubeReturnState, TUBETIPPERSTATE &curTubeTipperState) {
	curTubeReturnState = GetTubeReturnState(myTube);
	curTubeTipperState = GetTubeTipperState(myTube);
}

void SetTubeReturnStateToStow(TUBE &myTube) {	// used to reset the tube state to stowed when under manual control
	MOTORStop(myTube.tubeReturnMotor);
	ResetMotorEncoderPosition(myTube.tubeReturnMotor);
	myTube.curTubeReturnState = TubeReturnStowed;
	myTube.targetTubeReturnState = TubeReturnStowed;
	//writeDebugStreamLine("Tube Reset to stowed");
}

int GetTubeReturnManualPower(TUBE &myTube) {	// get the manual power level
	return(myTube.tubeReturnManualPower);
}

void SetTubeReturnManualPower(TUBE &myTube, int newPower) {	// set the manual power level
	int adjPower = AndyMarkPowerAdjust(newPower);	// we are using an AndyMark motor for the tube return so ajust this
	myTube.tubeReturnManualPower = adjPower;
}

bool IsTubeMoving(TUBE &myTube) {
	TUBERETURNSTATE curTubeReturnState = GetTubeReturnState(myTube);
	return (TubeReturnMoving == curTubeReturnState);
}

void MoveTube(TUBE &myTube, TUBERETURNSTATE newTubeReturnState, LIFTSTATE curLiftState) {	// we need the lift state to figure out how to move the tube
	SetMotorDrivingSmoothFactor(myTube.tubeReturnMotor,NoMotorSmoothing);	// turn off smoothing while under automatic control for faster moves
	SetMotorDriveSpeed(myTube.tubeReturnMotor,TubeReturnMotorDriveSpeed);	// and drive at normal speed
	switch(newTubeReturnState)
	{
	case TubeReturnStowed:	// we don't care about the lift state when stowing - we can always stow, even when lift is under manual control. It would probably be better not to stow the tube while the lift is moving, but it should work.
		if (TubeReturnStowed != myTube.curTubeReturnState)	// don't do anything if we are already here
		{	// stowing tube. Don't reset tipper until we are done since it will help take up slack

			myTube.prevTubeReturnEncoderValue = GetMotorEncoderPosition(myTube.tubeReturnMotor);	// get the current encoder position so we can check later to see if we are stalled
			myTube.TubeReturnStowStallCountdown = TubeReturnStowStallCountdownStart;	// and initialize the countdown timer
			MOTORMoveToTarget(myTube.tubeReturnMotor,TubeReturnMotorMovePowerStow,TubeReturnLenStowed);	// this will handle the direction for us
			myTube.targetTubeReturnState = TubeReturnStowed;
			myTube.curTubeReturnState = TubeReturnMoving;
			//writeDebugStreamLine("MoveTube>Stowing Tube: starting encoder value: %d",myTube.prevTubeReturnEncoderValue);
		}
		break;
	case TubeReturnDeployed:	// we need to know the lift state to properly deploy the tube
		if ((TubeReturnStowed == myTube.curTubeReturnState) && (LiftManualControl != curLiftState))	// we only deploy from the tube stowed position and lift is not under manual control
		{
			TubeTipperStow(myTube);	// stow tube tipper when deploying tube
			myTube.targetTubeReturnState = TubeReturnDeployed;
			myTube.curTubeReturnState = TubeReturnMoving;
			int tubeReturnTargetLen = TubeReturnLenStowed;	// default to stowed
			switch(curLiftState) {
			case LiftStowed:
				//writeDebugStreamLine("DeployingTube: 30cm");
				tubeReturnTargetLen = TubeReturnLen30cm;
				break;
			case LiftPos60cm:
				//writeDebugStreamLine("DeployingTube: 60cm");
				tubeReturnTargetLen = TubeReturnLen60cm;
				break;
			case LiftPos90cm:
				writeDebugStreamLine("DeployingTube: 90cm");
				tubeReturnTargetLen = TubeReturnLen90cm;
				break;
			case LiftPos120cm:
				//writeDebugStreamLine("DeployingTube: 120cm");
				tubeReturnTargetLen = TubeReturnLen120cm;
				break;
			}
			MOTORMoveToTarget(myTube.tubeReturnMotor,TubeReturnMotorMovePowerDeploy,tubeReturnTargetLen);
		}
		break;
	case TubeReturnDeployedTipped:	// we need to know the lift state to properly tip the tube
		if ((TubeReturnDeployed == myTube.curTubeReturnState) && (LiftManualControl != curLiftState))	// we only tip from the tube deployed position and lift is not under manual control
		{
			myTube.targetTubeReturnState = TubeReturnDeployedTipped;
			myTube.curTubeReturnState = TubeReturnMoving;
			int tubeReturnTargetLen = GetTubeReturnTippingLen(curLiftState);	// start with offset depending on height
			switch(curLiftState) {
			case LiftStowed:
				tubeReturnTargetLen = tubeReturnTargetLen + TubeReturnLen30cm;	// add in lift height
				break;
			case LiftPos60cm:
				tubeReturnTargetLen = tubeReturnTargetLen + TubeReturnLen60cm;	// add in lift height
				break;
			case LiftPos90cm:
				tubeReturnTargetLen = tubeReturnTargetLen + TubeReturnLen90cm;	// add in lift height
				break;
			case LiftPos120cm:
				//writeDebugStreamLine("TippingTube120cm");
				tubeReturnTargetLen = tubeReturnTargetLen + TubeReturnLen120cm;	// add in lift height
				break;
			}
			MOTORMoveToTarget(myTube.tubeReturnMotor,TubeReturnMotorMovePowerTip,tubeReturnTargetLen);
		}
		break;
	}
}

void MoveTubeManual(TUBE &myTube, int power) {	// moves the tube under manual control
	TubeTipperStow(myTube);	// always stow the tube tipper when under manual power.
	int adjPower = AndyMarkPowerAdjust(power);	// we are using an AndyMark motor for the tube return so ajust this
	myTube.targetTubeReturnState = TubeReturnManualControl;
	myTube.curTubeReturnState = TubeReturnMoving;
	myTube.tubeReturnManualPower = adjPower;
	SetMotorDrivingSmoothFactor(myTube.tubeReturnMotor,DefaultMotorSmoothFactor);	// turn on smoothing while under manual control
	SetMotorDriveSpeed(myTube.tubeReturnMotor,TubeReturnMotorManualDriveSpeed);	// and drive at manual speed
	MOTORMove(myTube.tubeReturnMotor,adjPower);
}

void TUBETask(TUBE &myTube) {
	SERVOTask(myTube.tubeTipperServo);	// update servo

	MOTORTask(myTube.tubeReturnMotor);	// Do motor task first before checking if motor is running
	if (!IsTubeMoving(myTube))	// if tube isn't moving, it should be stopped
	{
		MOTORStop(myTube.tubeReturnMotor);
	}
	else
	{	// tube is moving, check current status
		if (IsMotorStopped(myTube.tubeReturnMotor)) {
			switch(myTube.targetTubeReturnState)
			{
			case TubeReturnStowed:
				myTube.curTubeReturnState = TubeReturnStowed;	// we made it
				break;
			case TubeReturnDeployed:
				myTube.curTubeReturnState = TubeReturnDeployed;
				break;
			case TubeReturnDeployedTipped:
				myTube.curTubeReturnState = TubeReturnDeployedTipped;
				//writeDebugStreamLine("TubeTask> tube return is deployed> Pos: %d",GetMotorEncoderPosition(myTube.tubeReturnMotor));
				break;
			case TubeReturnManualControl:
				//myTube.curTubeReturnState = TubeReturnManualControl;
				break;
			}
			writeDebugStreamLine("TubeReturnStopped> encoder value: %d",GetMotorEncoderPosition(myTube.tubeReturnMotor));
		}
		else if(TubeReturnStowed == myTube.targetTubeReturnState)	// motor is still moving and our target is stowed, see if the tube is stalled (stalling is okay since we are using an AndyMark motor which won't burn out)
		{
			long curTubeReturnMotorEncoderValue = GetMotorEncoderPosition(myTube.tubeReturnMotor);
			long diffVal = abs(curTubeReturnMotorEncoderValue - myTube.prevTubeReturnEncoderValue);
			writeDebugStreamLine("TUBETask> Stowing> difference in encoder values: %d",diffVal);
			if (diffVal > TubeReturnStowTolerance)
			{
				writeDebugStreamLine("Stowing> Tube Return encoder value: %d",GetMotorEncoderPosition(myTube.tubeReturnMotor));
				myTube.prevTubeReturnEncoderValue = curTubeReturnMotorEncoderValue;	// set this for next time
				myTube.TubeReturnStowStallCountdown = TubeReturnStowStallCountdownStart;	// tube return not stalled, reset countdown timer
				//TubeTipperTip(myTube);	// tip the tube to take up slack
			}
			else
			{
				myTube.TubeReturnStowStallCountdown--;	// countdown timer
				//writeDebugStreamLine("TUBETask> Stowing> counting down to stall: %d",myTube.TubeReturnStowStallCountdown);
				if (myTube.TubeReturnStowStallCountdown <= 0)
				{
					MOTORStop(myTube.tubeReturnMotor);	// we are stalled, stop motor
					//writeDebugStreamLine("TUBETask> Stowed ******************> Tube Return encoder value: %d",GetMotorEncoderPosition(myTube.tubeReturnMotor));
					ResetMotorEncoderPosition(myTube.tubeReturnMotor);	// reset motor encoder value to zero
					myTube.curTubeReturnState = TubeReturnStowed;	// we made it
					TubeTipperStow(myTube);	// now we can stow the tube tipper
				}
			}
		}
		else if(TubeReturnManualControl == myTube.targetTubeReturnState)
		{
			if (0 == myTube.tubeReturnManualPower)	// we are under manual power and the power is zero -> shut off motor
			{
				MOTORStop(myTube.tubeReturnMotor);	// stop motor
				myTube.curTubeReturnState = TubeReturnManualControl;
			}
		}
	}
}

void TUBEInit(TUBE &myTube, int tubeReturnMotorID, int tubeTipperServoID) {
	MOTORInit(myTube.tubeReturnMotor,tubeReturnMotorID,TubeReturnMotorSmoothFactor,TubeReturnMotorDriveSpeed);
	TubeTipperInitStowed(myTube);	// start with tipper stowed
	TubeTipperStow(myTube);		// this will set the state
	myTube.tubeReturnManualPower = 0;	// no manual power yet
	myTube.curTubeReturnState = TubeReturnStowed;	// tube is stowed at the start
	myTube.targetTubeReturnState = TubeReturnStowed;
	myTube.TubeReturnStowStallCountdown = 0;	// we aren't checking for stall yet
	myTube.prevTubeReturnEncoderValue = 0;
	TUBETask(myTube);
}
#endif
