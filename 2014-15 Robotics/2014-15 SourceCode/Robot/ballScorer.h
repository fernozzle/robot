#ifndef _BALL_SCORER_H
#define _BALL_SCORER_H
//
//	Includes
//
#include "BallGrabber.h"	// for BALL_GRABBER
#include "lift.h"	         // for LIFTSTATE
#include "tube.h"				// for TUBE

//
//	Constants
//
#define BallScorerBallToTubeCountdownStart 17  // take this many cycles to wait for the ball grabber to finish getting balls into tube
#define BALL_SCORING_TIME 500	// number of ms to allow balls to score from tube to goal - don't make this too low, better to be sure!

typedef enum
{
	BallScorerMasterNone,															// no master task
	BallScorerMasterDeployGrabberArmOpenJaws,								// deploy the ball grabber arm and then open the jaws
	BallScorerMasterBallToTubeAndBacktoReadyToScore,	//scores a ball to the tube and then gets ready to score again
	BallScorerMasterTubeToGoal												// deploy tube and tip into the goal
} BALLSCORERMASTERTASK;

typedef enum
{
	BallScorerAllStowed,
	BallScorerBallGrabberDeployed,		// we don't care about the tube state while moving, we'll pick either BallScorerArmDeployedTubeStowed or BallScorerArmDeployedTubeDeployed when we are done
	BallScorerArmDeployedTubeStowed,
	BallScorerArmDeployedTubeDeployed,
	BallScorerBallToTube,
	BallScorerTubeToGoal,
	BallScorerTubeManualControl,
	BallScorerMoving
} BALLSCORERSTATE;

typedef struct {
	BALL_GRABBER bsBallGrabber;
	TUBE bsTube;
	BALLSCORERSTATE curBallScorerState;
	BALLSCORERSTATE targetBallScorerState;
	BALLSCORERMASTERTASK masterTask;
	int ballScorerBallToTubeCountdown;	// this counts down from BallScorerBallToTubeCountdownStart before the balls are considered fully scored
} BALL_SCORER;

#define GetBallScorerState(myBallScorer) myBallScorer.curBallScorerState	// return ball scorer state
// initialize ball scorer before restoring in teleop - Stow tube tipper & close ball grabber jaws
#define BallScorerPreRestoreInit(myBallScorer) TubeTipperInitStowed(myBallScorer.bsTube);	BallGrabberInitClosed(myBallScorer.bsBallGrabber)

void BALLSCORERStop(BALL_SCORER &myBallScorer) {	// stop  the ball scorer -> this can be called in autonomous to shutdown when there are problems
	TUBEStop(myBallScorer.bsTube);	// stop the tube
	BALL_GRABBERStop(myBallScorer.bsBallGrabber);	// stop the ball grabber
}


TUBERETURNSTATE GetBallScorerTubeReturnState(BALL_SCORER &myBallScorer) {
	return(GetTubeReturnState(myBallScorer.bsTube));
}

void BallScorerToggleTubeTipper(BALL_SCORER &myBallScorer) {	// toggle the tube tipper
	ToggleTubeTipper(myBallScorer.bsTube);
}

void BallScorerMoveTubeTipper(BALL_SCORER &myBallScorer, int move) {	// moves the tube tipper within its limits
	MoveTubeTipper(myBallScorer.bsTube,move);
}

void BallScorerStowTube(BALL_SCORER &myBallScorer, LIFTSTATE curLiftState) {
	// Get current state of sub objects
	TUBERETURNSTATE curTubeReturnState;
	TUBETIPPERSTATE curTubeTipperState;
	GetTubeState(myBallScorer.bsTube,curTubeReturnState,curTubeTipperState);   // tube
	BALLGRABBERARMSTATE curBallGrabberArmState;
	BALLGRABBERJAWSSTATE curBallGrabberJawsState;
	GetBallGrabberState(myBallScorer.bsBallGrabber,curBallGrabberArmState,curBallGrabberJawsState); // ball grabber

	// What we do depends on the current state of our sub-objects
	if (TubeReturnStowed == curTubeReturnState)  // tube is already stowed
	{
		writeDebugStreamLine("BallScorerStowTube> Tube Return already stowed, stowing tipper");
		TubeTipperStow(myBallScorer.bsTube);   // stow the tube tipper (it will figure out if it is already stowed)
		// To set our state we need to know if the ball grabber is stowed or not
		if (BallGrabberArmStowed == curBallGrabberArmState)
		{
			myBallScorer.curBallScorerState = BallScorerAllStowed;   // ball grabber is stowed, so everything is stowed
		}
		else
		{
			myBallScorer.curBallScorerState = BallScorerArmDeployedTubeStowed;   // ball grabber is deployed
		}
	}
	else if(BallGrabberArmDeployed == curBallGrabberArmState)   // we can only stow the tube if the ball grabber is deployed
	{	// NOTE: Don't stow the tube tipper since it will help take up slack
		writeDebugStreamLine("TubeToStowStarting");
		myBallScorer.targetBallScorerState = BallScorerArmDeployedTubeStowed;	// this is our goal
		myBallScorer.curBallScorerState = BallScorerMoving;   // we are moving
		MoveTube(myBallScorer.bsTube,TubeReturnStowed,curLiftState);
		TubeTipperTip(myBallScorer.bsTube);	// tip the tube to take up slack
	}
	else
	{
		myBallScorer.curBallScorerState = BallScorerMoving;   // arm shouldn't be stowed since tube isn't stowed, so it must be moving therefore the ball scorer is moving
	}
}

void BallScorerStowBallGrabber(BALL_SCORER &myBallScorer) {
	// Get current state of sub objects
	BALLGRABBERARMSTATE curBallGrabberArmState;
	BALLGRABBERJAWSSTATE curBallGrabberJawsState;
	GetBallGrabberState(myBallScorer.bsBallGrabber,curBallGrabberArmState,curBallGrabberJawsState); // ball grabber
	TUBERETURNSTATE curTubeReturnState;
	TUBETIPPERSTATE curTubeTipperState;
	GetTubeState(myBallScorer.bsTube,curTubeReturnState,curTubeTipperState);   // tube

	// What we do depends on the current state of our sub-objects
	if (BallGrabberArmStowed == curBallGrabberArmState)   // ball grabber already stowed
	{
		myBallScorer.curBallScorerState = BallScorerAllStowed;
	}
	else if(BallGrabberArmDeployed == curBallGrabberArmState)   // ball grabber deployed, so it is okay to stow if tube is stowed, otherwise we have to wait
	{
		if (TubeReturnStowed == curTubeReturnState)  // tube is already stowed
		{
			myBallScorer.curBallScorerState = BallScorerMoving;   // we are moving
			CloseBallGrabberJaws(myBallScorer.bsBallGrabber);  // we need to close the jaws to stow the ball grabber arm (they may be closed already)
			StowBallGrabberArm(myBallScorer.bsBallGrabber);
		}
	}
	else
	{
		// We can't do anything until ball grabber arm is either stowed or deployed
	}
}

void BallScorerDeployBallGrabber(BALL_SCORER &myBallScorer, BALLGRABBERJAWSSTATE jawState) {
	DeployBallGrabberArm(myBallScorer.bsBallGrabber);	// deploy the arm first
	SetBallGrabberJaws(myBallScorer.bsBallGrabber,jawState);	// then the jaws
	myBallScorer.targetBallScorerState = BallScorerBallGrabberDeployed;
	myBallScorer.curBallScorerState = BallScorerMoving;
}

void BallScorerToggleBallGrabberJaws(BALL_SCORER &myBallScorer) {
	ToggleBallGrabberJaws(myBallScorer.bsBallGrabber);
}

void BallScorerAllStow(BALL_SCORER &myBallScorer, LIFTSTATE curLiftState) {	// stow everything
	myBallScorer.targetBallScorerState = BallScorerAllStowed;   // This is our goal
	BallScorerStowTube(myBallScorer,curLiftState);		// The tube has to be stowed first, so try and stow that
	if (BallScorerAllStowed == myBallScorer.curBallScorerState)
	{
		// all done
	}
	else if(BallScorerArmDeployedTubeStowed == myBallScorer.curBallScorerState)
	{
		BallScorerStowBallGrabber(myBallScorer);  // need to stow ball grabber now that tube is stowed -> it will close the jaws as necessary
	}
	else
	{
		// things must be moving, so we have to wait
	}
}

bool IsBallScorerBallGrabberStowed(BALL_SCORER &myBallScorer)	{	// is the ball grabber stowed?
	return IsBallGrabberStowed(myBallScorer.bsBallGrabber);	// the ball grabber knows!
}

void BallScorerSetTubeReturnStateToStow(BALL_SCORER &myBallScorer) {	// used to reset the tube state to stowed when under manual control
	SetTubeReturnStateToStow(myBallScorer.bsTube);
	if (IsBallScorerBallGrabberStowed(myBallScorer))
	{
		myBallScorer.curBallScorerState = BallScorerAllStowed;	// ball grabber ar stowed too, so we are all stowed
	}
	else
	{
		myBallScorer.curBallScorerState = BallScorerArmDeployedTubeStowed;	// ball grabber is deployed
	}
}

bool IsBallScorerReadyToScore(BALL_SCORER &myBallScorer,bool bJawStateImportant) {	// is ball scorer ready to score?
	TUBERETURNSTATE curTubeReturnState;
	TUBETIPPERSTATE curTubeTipperState;
	GetTubeState(myBallScorer.bsTube,curTubeReturnState,curTubeTipperState);   // tube
	BALLGRABBERARMSTATE curBallGrabberArmState;
	BALLGRABBERJAWSSTATE curBallGrabberJawsState;
	GetBallGrabberState(myBallScorer.bsBallGrabber,curBallGrabberArmState,curBallGrabberJawsState); // ball grabber
	if ((TubeReturnStowed == curTubeReturnState) && (TubeTipperStowed == curTubeTipperState) &&
		(BallGrabberArmDeployed == curBallGrabberArmState))
	{
		if (bJawStateImportant && (BallGrabberJawsOpen != curBallGrabberJawsState))	// only check this if jaws state is important
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		return false;
	}
}
/*
void GetBallScorerReadyToScore(BALL_SCORER &myBallScorer) {   // get ball scorer ready to score = tube stowed, tube tipper stowed, ball grabber arms deployed, ball grabber jaws open
if (!IsBallScorerReadyToScore(myBallScorer,true))	// only do this if we aren't already ready to score (we care about the jaws state)
{
myBallScorer.targetBallScorerState = BallScorerArmDeployedTubeStowed;	// this is our sub goal
DeployBallGrabberArm(myBallScorer.bsBallGrabber);  // deploy the ball Grabber arm to start and open the jaws, BALLSCORERTask will handle the rest
OpenBallGrabberJaws(myBallScorer.bsBallGrabber);
myBallScorer.curBallScorerState = BallScorerMoving;
}
}
*/
void GetBallScorerBallToTube(BALL_SCORER &myBallScorer) {   // score ball to tube
	if (IsBallScorerReadyToScore(myBallScorer,false))	// we can only score a ball to the tube if we are ready to score (we don't care about the jaws state)
	{
		myBallScorer.targetBallScorerState = BallScorerBallToTube;   // This is our goal
		CloseBallGrabberJaws(myBallScorer.bsBallGrabber); // close the ball grabber jaws to start (we always want to do this in case they got knocked out of closed) BALLSCORERTask will handle the rest
		myBallScorer.curBallScorerState = BallScorerMoving;
	}
}

void GetBallScorerTubeToGoal(BALL_SCORER &myBallScorer, LIFTSTATE curLiftState) {   // score tube to goal -> deploy tube and then tip it
	if (IsBallScorerReadyToScore(myBallScorer,false))	// we can only score tube to the goal if we are ready to score (we don't care about the jaws state)
	{
		myBallScorer.masterTask = BallScorerMasterTubeToGoal;	// this is our master task1
		writeDebugStreamLine("BallScorerMasterTubeToGoal Starting. LiftState: %d",curLiftState);
		myBallScorer.targetBallScorerState = BallScorerArmDeployedTubeDeployed;   // This is our goal
		TubeTipperStow(myBallScorer.bsTube); // Make sure tube tipper is stowed before we sent the tube to the goal
		MoveTube(myBallScorer.bsTube,TubeReturnDeployed,curLiftState);   // Move to goal to start, BALLSCORERTask will handle the rest
		myBallScorer.curBallScorerState = BallScorerMoving;
	}
}
/*
bool IsBallScorerMoving(BALL_SCORER &myBallScorer) {
return (BallScorerMoving == myBallScorer.curBallScorerState);
}
*/
void BallScorerMoveTubeManual(BALL_SCORER &myBallScorer, int power) {	// moves the tube under manual control
	if (BallGrabberArmDeployed == GetBallGrabberArmState(myBallScorer.bsBallGrabber))	// only move the tube if the ball grabber arm is deployed
	{
		writeDebugStreamLine("moving tube manually> power = %d",power);
		myBallScorer.targetBallScorerState = BallScorerTubeManualControl;
		myBallScorer.curBallScorerState = BallScorerMoving;
		myBallScorer.masterTask = BallScorerMasterNone;	// turn off any master tasks that are running
		MoveTubeManual(myBallScorer.bsTube,power);
	}
}

void BallScorerSetTubeReturnManualPower(BALL_SCORER &myBallScorer, int power) {	// moves the tube under manual control
	SetTubeReturnManualPower(myBallScorer.bsTube,power);
}

void BallScorerDeployGrabberArmOpenJaws(BALL_SCORER &myBallScorer) {	// deploy ball grabber arm and then open jaws
	if (BallGrabberArmStowed == GetBallGrabberArmState(myBallScorer.bsBallGrabber))	// can only do this if ball grabber arm is stowed
	{
		myBallScorer.masterTask = BallScorerMasterDeployGrabberArmOpenJaws;	// this is our master task1
		BallScorerDeployBallGrabber(myBallScorer,BallGrabberJawsClosed);	// Part 1 is deploying ball grabber with jaws closed to start with.
	}
}
void BallScorerBallToTubeAndBacktoReadyToScore(BALL_SCORER &myBallScorer) {// move the ball to tube and the deploy arm ready to score with jaws open
	if (IsBallScorerReadyToScore(myBallScorer,false))	// we can only score tube to the goal if we are ready to score (we don't care about the jaws state)
	{
		myBallScorer.masterTask = BallScorerMasterBallToTubeAndBacktoReadyToScore;	// this is our master task1
		myBallScorer.ballScorerBallToTubeCountdown = BallScorerBallToTubeCountdownStart;	// we can initialize the countdown timer now since it won't get decremented until we want to pause
		GetBallScorerBallToTube(myBallScorer);	// first task is to score ball to tube
	}
}

void BALLSCORERTask(BALL_SCORER &myBallScorer, LIFTSTATE curLiftState) {
	BALLGRABBERTask(myBallScorer.bsBallGrabber); // let ball grabber have a chance to figure out what its state is
	TUBETask(myBallScorer.bsTube); // let tube have a chance to figure out what its state is
	// Get current state of sub objects
	BALLGRABBERARMSTATE curBallGrabberArmState;
	BALLGRABBERJAWSSTATE curBallGrabberJawsState;
	GetBallGrabberState(myBallScorer.bsBallGrabber,curBallGrabberArmState,curBallGrabberJawsState); // ball grabber
	TUBERETURNSTATE curTubeReturnState;
	TUBETIPPERSTATE curTubeTipperState;
	GetTubeState(myBallScorer.bsTube,curTubeReturnState,curTubeTipperState);   // tube

	if (BallScorerMoving == myBallScorer.curBallScorerState) // if we are moving, see what our goal is and figure out what to do next
	{
		switch(myBallScorer.targetBallScorerState)
		{
		case BallScorerAllStowed:  // all stowed
			BallScorerAllStow(myBallScorer,curLiftState); // keep trying to do this until we succeed -> it will set our state appropiately
			break;
		case BallScorerBallGrabberDeployed:  // Deploying arm. we don't care about the tube state while moving, we'll pick either BallScorerArmDeployedTubeStowed or BallScorerArmDeployedTubeDeployed when we are done
			if (BallGrabberArmDeployed == curBallGrabberArmState)   // ball grabber deployed so we are done
			{
				if (BallScorerMasterDeployGrabberArmOpenJaws == myBallScorer.masterTask)	// part 2 of this master task
				{
					myBallScorer.masterTask = BallScorerMasterNone;	// task is done
					OpenBallGrabberJaws(myBallScorer.bsBallGrabber);	// okay to open the jaws now
				}
				if (TubeReturnStowed == GetTubeReturnState(myBallScorer.bsTube))
				{
					myBallScorer.curBallScorerState = BallScorerArmDeployedTubeStowed;		// tube is stowed, so we are ready to score again
				}
				else
				{
					myBallScorer.curBallScorerState = BallScorerArmDeployedTubeDeployed;		// tube isn't stowed, it must be moving up to or back from the goal. That's fine, it just means we'll have to wait for it stow to score again
				}
			}
			break;
		case BallScorerArmDeployedTubeStowed:  // See if ball grabber is deployed yet. If not, we can't store the tube yet
			if (BallGrabberArmDeployed == curBallGrabberArmState)   // ball grabber deployed, so it is okay to stow the tube
			{
				if (TubeReturnStowed == curTubeReturnState)	// tube return is stowed, we are done
				{
					myBallScorer.curBallScorerState = BallScorerArmDeployedTubeStowed;
					TubeTipperStow(myBallScorer.bsTube);	// now we can stow the tube tipper
					writeDebugStreamLine("TubeStowedArmDeployed> DONE");
				}
				else
				{
					if (TubeReturnMoving != curTubeReturnState)	// only do this if tube return isn't already moving
					{
						BallScorerStowTube(myBallScorer,curLiftState);  // stow the tube - this will set our state appropriately
					}
					else
					{
						TubeTipperTip(myBallScorer.bsTube);	// while stowing the tube, keep tipping the tipper to take up slack
						//writeDebugStreamLine("Tipping tipper to take up slack");
					}
				}
			}
			else
			{
				// We can only stow the tube when the ball grabber is deployed
			}
			break;
		case BallScorerBallToTube: // we want to score a ball to the tube
			if (BallGrabberJawsClosed == curBallGrabberJawsState) // jaws are done closing, see if arm is done too
			{
				if (BallGrabberArmStowed == curBallGrabberArmState)  // arm is done stowing
				{
					if (BallScorerMasterBallToTubeAndBacktoReadyToScore == myBallScorer.masterTask)	// this is part of a master task. Part 1 is now done, time to start part 2
					{
						myBallScorer.ballScorerBallToTubeCountdown--;	// countdown
						if (myBallScorer.ballScorerBallToTubeCountdown <= 0)	// balls should be safely in the goal by now
						{
							BallScorerDeployGrabberArmOpenJaws(myBallScorer);	// Part 2 is handled by this master task
						}
					}
					else
					{
						myBallScorer.curBallScorerState = BallScorerBallToTube;   // not a master task, ball should be in the goal

					}
				}
				else if(BallGrabberArmDeployed == curBallGrabberArmState)   // jaws are closed but arm hasn't moved yet.
				{
					StowBallGrabberArm(myBallScorer.bsBallGrabber); // so stow it
				}
			}
			else
			{
				// ball grabber jaws aren't closed yet, we need to wait
			}
			break;
		case BallScorerArmDeployedTubeDeployed:   // we want to deploy the tube
			if (TubeReturnDeployed == curTubeReturnState)   // tube is deployed -> the arm had better be deployed, otherwise we are in trouble
			{
				myBallScorer.curBallScorerState = BallScorerArmDeployedTubeDeployed; // we made it
				if (BallScorerMasterTubeToGoal == myBallScorer.masterTask)	// part 1 is done. Time to start part 2
				{
					writeDebugStreamLine("TubeToGoal> tube is deployed, time to tip");
					myBallScorer.targetBallScorerState = BallScorerTubeToGoal;	// part 2 is tipping it into the goal
					myBallScorer.curBallScorerState = BallScorerMoving;	// we are still moving
					TubeTipperTip(myBallScorer.bsTube); // Tip the tube to take up slack
					MoveTube(myBallScorer.bsTube,TubeReturnDeployedTipped,curLiftState); // get motor started
					TubeTipperTip(myBallScorer.bsTube); // Tip the tube to take up slack
				}
			}
			break;
		case BallScorerTubeToGoal: // We want to move the tube to the goal and score
			TubeTipperTip(myBallScorer.bsTube); // keep tipping this to take up slack
			if (TubeReturnDeployedTipped == curTubeReturnState)
			{
				myBallScorer.curBallScorerState = BallScorerTubeToGoal;	// we made it
				writeDebugStreamLine("BallScorerTubeToGoal> Balls in Goal!");
				if (BallScorerMasterTubeToGoal == myBallScorer.masterTask)	// part 1 is done. Time to start part 2
				{
					myBallScorer.masterTask = BallScorerMasterNone;	// part 2 is done so we are done
				}
			}
			break;
		case BallScorerTubeManualControl:  // tube is under manual control, see if it has stopped moving
			if (!IsTubeMoving(myBallScorer.bsTube))
			{
				myBallScorer.curBallScorerState = BallScorerTubeManualControl;	// stopped moving
				writeDebugStreamLine("*****************************");
				writeDebugStreamLine("BALL SCORER STOPPED MOVING!!");
				writeDebugStreamLine("*****************************");
				writeDebugStreamLine("TubeReturn encoder value: %d",GetMotorEncoderPosition(myBallScorer.bsTube.tubeReturnMotor));
			}
			break;
		}
		TUBETask(myBallScorer.bsTube);	// let tube task run again
	}
	else
	{
		// we aren't moving
	}
}

void BALLSCORERInit(BALL_SCORER &myBallScorer, int tubeReturnMotorID, int tubeTipperServoID, int ballGrabberArmMotorID,
int ballGrabberJawsServoID, BALLSCORERSTATE ballScorerState, LIFTSTATE curLiftState, BALLGRABBERARMSTATE ballGrabberArmState) {

	BALLGRABBERInit(myBallScorer.bsBallGrabber,ballGrabberArmMotorID,ballGrabberJawsServoID, ballGrabberArmState);   // init ball grabber
	TUBEInit(myBallScorer.bsTube,tubeReturnMotorID,tubeTipperServoID);   // init tube
	myBallScorer.curBallScorerState = ballScorerState;	// Ball Scorer is stowed at the start
	myBallScorer.targetBallScorerState = ballScorerState;
	myBallScorer.masterTask = BallScorerMasterNone;	// no master task yet
	myBallScorer.ballScorerBallToTubeCountdown = 0;	// not scoring balls yet
	BALLSCORERTask(myBallScorer,curLiftState);
}
#endif
