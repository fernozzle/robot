#ifndef _STANDARD_SERVO_H
#define _STANDARD_SERVO_H

//
// Type definitions.
//
typedef struct
{
	int	servoID;	// servo ID
	int position;	// servo position
} STANDARD_SERVO;


// sets position of servo (from 0 to 255)
void SetServoPosition(STANDARD_SERVO &myServo, int position){
	myServo.position = position;
	servo[myServo.servoID] = myServo.position;
}

//	2014Dec22> We ran out of subroutines with RobotC 4.27, so change these to a #define hopefully next year this won't be a limit

// returns position of servo (from 0 to 255)
#define GetServoPosition(myServo) myServo.position
/*
int GetServoPosition(STANDARD_SERVO &myServo){
	return myServo.position;
}
*/

#define SERVOTask(myServo) {}
/*
void
SERVOTask(STANDARD_SERVO &myServo) {
	// no reason to get position since controller just reports back what we sent, not actual psoition> myServo.position = servo[myServo.servoID];	// get position
	// Doing this every time just wastes communication bandwidth to the controllers.
	return;
}
*/

void
SERVOInit(
STANDARD_SERVO &myServo, int servoID, int position, bool bSetInitialState) {
	myServo.servoID = servoID;
	if (bSetInitialState)   // set the initial state -> When transitioning from autonomous to teleop, we don't want to set the state of some servos
	{
		SetServoPosition(myServo, position);
	}
	SERVOTask(myServo);

	return;
}

#endif
