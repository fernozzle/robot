#ifndef _ROBOT_H
#define _ROBOT_H

//
//	Includes
//
#include "JoystickDriver.c"	//Include file to "handle" the Bluetooth messages.
#include "drivetrain.h"			// for drive train
#include "ballScorer.h"			// for BALL_SCORER
#include "goalGrabber.h"		// for GOAL_GRABBER
#include "lift.h"						// for LIFT
#include "tube.h"						// for TUBE
#include "restore.h"				// for RESTORESTATE

//
//	Constants
//
#define KICKSTAND_ARM_STOWED 255
#define KICKSTAND_ARM_DEPLOYED 0

//
//	DriveTrain
//
const float DriveTrainSpeedGoalGrabbing = 0.75;	// drive full speed so we can get to the goal fast
const float DriveTrainSpeedBallGrabbing = 0.57;	// drive slow so we don't lose the goal
const	int SlowMovePower = 30;	// use thse values for moving and turning with the joy1 Hat
const	int SlowTurnPower = 20;
const float SlowTankDrivePowerFactor = 0.3;	// multiply joystick input by this when holding down both Btn5 & Btn7
//
//	Tube Return
//
const float TubeReturnManualSpeed = 0.3;	// don't go too fast under manual control
//
//	Tube Tipper
//
const int TubeTipperMoveIncrement = 1;	// when moving the tube tipper manually, move it this increment +/-
//
// Type definitions.
//
typedef struct
{
	DRIVETRAIN rDriveTrain;      // Robot in 2014-15 consists of a drivetrain
	LIFT rLift;										// Lift
	BALL_SCORER rBallScorer;				// Ball scorer
	GOAL_GRABBER rGoalGrabber;			// Goal grabber
	STANDARD_SERVO kickstandArmServo; // Kickstand arm

	bool bStateHasBeenRestored;								// true if the 2nd driver has indicated which state the robot is in at the beginning of tele-op and has lifted all buttons
	RESTORESTATE rChosenRestoreState;					// the restore state that was chosen

	bool bCanToggleDrivingMode;								// true if driving mode can be toggled. This allows the use of one button to toggle driving mode between foward (goal grabbing) and reverse (ball grabbing)
	bool bCanToggleJawsMode;									// true if the ball grabber jaws can be toggled. This allows the use of one buton to toggle between open and closed
	bool bCanToggleTippingMode;								// true if the tube tipper can be toggled. This allows the use of one buttong to toggle between tipped and stowed
	bool bCanScoreBallToTube;									// true if we can score a ball to the tube using the trigger - this prevents over cycling
	bool bCanScoreTubeToGoalBackToStowToggle;	// true if we can score the tube to the goal using the trigger - this prevents over cycling
	bool bCanMoveLift;												// true if we can move the lift using the number buttons - this prevents over cycling
	bool bCanMoveGoalGrabber;									// true if we can move the goal grabber using the number buttons - this prevents overcycling
} ROBOT;

void HandleController1(ROBOT &myRobot, SENSORS &mySensors) {
	//
	// Drivetrain
	//
	if (joy1Btn(Btn10)) {	// toggle driving mode
		if (myRobot.bCanToggleDrivingMode)
		{
			DriveTrainMode curDriveTrainMode = GetDriveTrainMode(myRobot.rDriveTrain);
			if (DriveTrainModeBallGrabbing == curDriveTrainMode)
			{
				curDriveTrainMode = DriveTrainModeGoalGrabbing;	// toggle
			}
			else
			{
				curDriveTrainMode = DriveTrainModeBallGrabbing;	// toggle
			}
			SetDriveTrainMode(myRobot.rDriveTrain,curDriveTrainMode);
			myRobot.bCanToggleDrivingMode = false;	// don't let driver change it again until button is released
		}
	}
	else
	{
		myRobot.bCanToggleDrivingMode = true;	// button isn't pressed so we can set this again
	}
	//
	// 		Slow Moving - drivetrain mode controls ahead/reverse
	//
	if (0 == joystick.joy1_TopHat)
	{
		DRIVETRAINMoveTank(myRobot.rDriveTrain,SlowMovePower,SlowMovePower);	// straight ahead
	}
	else if (2 == joystick.joy1_TopHat)
	{
		DRIVETRAINMoveTank(myRobot.rDriveTrain,SlowTurnPower,0); // track right
	}
	else if (4 == joystick.joy1_TopHat)
	{
		DRIVETRAINMoveTank(myRobot.rDriveTrain,-SlowMovePower,-SlowMovePower);	// reverse
	}
	else if (6 == joystick.joy1_TopHat)
	{
		DRIVETRAINMoveTank(myRobot.rDriveTrain,0,SlowTurnPower); // track left
	}
	else
	{
		int driveTrainLeftPower = JOYSTICK_POWER(joystick.joy1_y1);	// apply deadband and normalize joystick input
		if (joy1Btn(5) && joy1Btn(6)) {
			driveTrainLeftPower = ceil((float) driveTrainLeftPower * SlowTankDrivePowerFactor);	// slow-motion
		}
		int driveTrainRightPower = JOYSTICK_POWER(joystick.joy1_y2);
		if (joy1Btn(5) && joy1Btn(6)) {
			driveTrainRightPower  = ceil((float) driveTrainRightPower * SlowTankDrivePowerFactor);	// slow-motion
		}
		//writeDebugStreamLine("Joy1:y1 %d // Joy1:y2 %d",driveTrainLeftPower,driveTrainRightPower);
		DRIVETRAINMoveTank(myRobot.rDriveTrain,driveTrainLeftPower,driveTrainRightPower);
	}
	DRIVETRAINTask(myRobot.rDriveTrain,mySensors.rsGyro);	// drive train task needs the gyro if turning automatically
	//
	// Goal Grabber
	//
	if (myRobot.bCanMoveGoalGrabber && joy1Btn(Btn1))//  Open sidegrabbers and Topgrabbers  |  Right keypad (1)
	{
		myRobot.bCanMoveGoalGrabber = false;	// set this to prevent over cycling
		GOALGRABBERSetState(myRobot.rGoalGrabber,GoalGrabberOpen);
	}
	else if (myRobot.bCanMoveGoalGrabber && joy1Btn(Btn2)) //  Grab  |  Right keypad (2)
	{
		myRobot.bCanMoveGoalGrabber = false;	// set this to prevent over cycling
		GOALGRABBERSetState(myRobot.rGoalGrabber,GoalGrabberGrab);
	}
	else if (myRobot.bCanMoveGoalGrabber && joy1Btn(Btn3)) //  Plow  |  Right keypad (3)
	{
		myRobot.bCanMoveGoalGrabber = false;	// set this to prevent over cycling
		GOALGRABBERSetState(myRobot.rGoalGrabber,GoalGrabberPlow);
	}
	else if (myRobot.bCanMoveGoalGrabber && joy1Btn(Btn4))  // for driving up the ramp | Right keypad (4)
	{
		myRobot.bCanMoveGoalGrabber = false;	// set this to prevent over cycling
		SetGoalGrabberStabilizers(myRobot.rGoalGrabber,GoalGrabberGrab);	// set goalgrabber stabilizers to grabbing.
		SetGoalGrabberSideArms(myRobot.rGoalGrabber,GoalGrabberOpen);			// set goalgrabber side arms to open
	}
	else if (myRobot.bCanMoveGoalGrabber && joy1Btn(Btn7))
	{
		myRobot.bCanMoveGoalGrabber = false;	// set this to prevent over cycling
		SetLeftSideGrabberArms(myRobot.rGoalGrabber,GoalGrabberGrab);	// grab left side
	}
	else if (myRobot.bCanMoveGoalGrabber &&joy1Btn(Btn8))
	{
		myRobot.bCanMoveGoalGrabber = false;	// set this to prevent over cycling
		SetRightSideGrabberArms(myRobot.rGoalGrabber,GoalGrabberGrab);	// grab right side
	}
	else if (myRobot.bCanMoveGoalGrabber && joy1Btn(Btn9))  // Parking Brake | Button 9
	{
		myRobot.bCanMoveGoalGrabber = false;	// set this to prevent over cycling
		PARKINGBRAKESTATE curParkingBrakeState = GetParkingBrakeState(myRobot.rDriveTrain);
		if (ParkingBrakeOn == curParkingBrakeState)
		{
			SetParkingBrakeOff(myRobot.rDriveTrain);
		}
		else
		{
			SetParkingBrakeOn(myRobot.rDriveTrain);
		}
	}
	else
	{
		myRobot.bCanMoveGoalGrabber = true;	// drive released buttons, ok to control goal grabber
	}

	GOALGRABBERTask(myRobot.rGoalGrabber);
}

void HandleController2(ROBOT &myRobot, SENSORS &mySensors) {
	//
	//	Lift - We are doing one interlock on the lift here. You can only operate the lift if the tube is stowed
	//
	TUBERETURNSTATE curTubeReturnState = GetBallScorerTubeReturnState(myRobot.rBallScorer);

	if (TubeReturnStowed == curTubeReturnState)	// lift can only move is tube is stowed otherwise there could be problems.
	{
		int liftPower = JOYSTICK_POWER(joystick.joy2_y2);
		if (0 != liftPower) {
			writeDebugStreamLine("Moving Lift manually. Joystick power: %d",liftPower);
			MoveLiftManual(myRobot.rLift,joystick.joy2_y2);	// manual control
		}
		else
		{
			SetLiftManualPower(myRobot.rLift,0);	// not moving lift under manual power
		}
		if (myRobot.bCanMoveLift && joy2Btn(Btn1))	// stow
		{
			myRobot.bCanMoveLift = false;		// set this to prevent over cycling
			if (joy2Btn(Btn10))
			{
				SetLiftState(myRobot.rLift,LiftStowed);	// reset lift state to stowed
			}
			else
			{
				MoveLift(myRobot.rLift,LiftStowed);	// move to Lift stowed
			}
		}
		else if (myRobot.bCanMoveLift && joy2Btn(Btn2))	// 60 cm goal
		{
			myRobot.bCanMoveLift = false;		// set this to prevent over cycling
			if (joy2Btn(Btn10))
			{
				SetLiftState(myRobot.rLift,LiftPos60cm);	// reset lift state to LiftPos60cm
			}
			else
			{
				MoveLift(myRobot.rLift,LiftPos60cm);	// move to 60cm position
			}
		}
		else if (myRobot.bCanMoveLift && joy2Btn(Btn3))	// 90 cm goal
		{
			myRobot.bCanMoveLift = false;		// set this to prevent over cycling
			if (joy2Btn(Btn10))
			{
				SetLiftState(myRobot.rLift,LiftPos90cm);	// reset lift state to LiftPos90cm
			}
			else
			{
				MoveLift(myRobot.rLift,LiftPos90cm);	// move to 90 cm position
			}
		}
		else if (myRobot.bCanMoveLift && joy2Btn(Btn4))	// 120 cm goal
		{
			myRobot.bCanMoveLift = false;		// set this to prevent over cycling
			if (joy2Btn(Btn10))
			{
				SetLiftState(myRobot.rLift,LiftPos120cm);	// reset lift state to LiftPos120cm
			}
			else
			{
				MoveLift(myRobot.rLift,LiftPos120cm);	// move to 120 cm
			}
		}
		else
		{
			myRobot.bCanMoveLift = true;	// none of the lift buttons are pressed, it is okay to move the lift automatically now - this prevents over cycling
		}
	}
	LIFTTask(myRobot.rLift);
	LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);	// need this for ball scorer
	//
	//	Ball Scorer
	//
	//			Ball Grabber -> operated through the myRobot.rBallScorer object which handles all interlocks
	//
	if(joy2Btn(Btn5)) {	// toggle jaws
		if (myRobot.bCanToggleJawsMode)
		{
			BallScorerToggleBallGrabberJaws(myRobot.rBallScorer);
			myRobot.bCanToggleJawsMode = false;	// don't let driver change it again until button is released
		}
	}
	else
	{
		myRobot.bCanToggleJawsMode = true;	// button isn't pressed so we can set this again
	}

	if (0 == joystick.joy2_TopHat)	// stowing ball grabber
	{
		BallScorerStowBallGrabber(myRobot.rBallScorer);
	}else if (4 == joystick.joy2_TopHat)	// deploying ball grabber - leave jaws closed -> driver 2 can use the left trigger (btn7) to deploy with jaws open
	{
		BallScorerDeployBallGrabber(myRobot.rBallScorer,BallGrabberJawsClosed);
	}
	//
	//			BallToTubeAndBacktoReadyToScore - score ball to tube and them deploy ball graber so it is ready to score again
	//
	if (joy2Btn(Btn7))
	{
		if (myRobot.bCanScoreBallToTube)
		{
			myRobot.bCanScoreBallToTube = false;	// force driver to release trigger to do this again
			if (IsBallScorerBallGrabberStowed(myRobot.rBallScorer))	// if the ball grabber is stowed, use this to deploy it ready to score (jaws open)
			{
				BallScorerDeployGrabberArmOpenJaws(myRobot.rBallScorer);
			}
			else
			{
				BallScorerBallToTubeAndBacktoReadyToScore(myRobot.rBallScorer);	// score the ball
			}
		}
	}
	else
	{
		myRobot.bCanScoreBallToTube = true;	// trigger is released, so it is okay to do this
	}
	//
	//	Tube Tipper
	//
	if(joy2Btn(Btn6)) {	// toggle tipper
		if (myRobot.bCanToggleTippingMode)
		{
			BallScorerToggleTubeTipper(myRobot.rBallScorer);
			myRobot.bCanToggleTippingMode = false;	// don't let driver change it again until button is released
		}
	}
	else
	{
		myRobot.bCanToggleTippingMode = true;	// button isn't pressed so we can set this again
	}
	// manual tipper movement
	if (6 == joystick.joy2_TopHat)
	{
		BallScorerMoveTubeTipper(myRobot.rBallScorer,-TubeTipperMoveIncrement);	// stow
	}else if (2 == joystick.joy2_TopHat)
	{
		BallScorerMoveTubeTipper(myRobot.rBallScorer,TubeTipperMoveIncrement);	// stow
	}
	//
	//		Tube Return
	//
	int tubeReturnPower = JOYSTICK_POWER(joystick.joy2_y1);
	if (0 != tubeReturnPower) {
		BallScorerMoveTubeManual(myRobot.rBallScorer,tubeReturnPower);	// manual control
	}
	else
	{
		BallScorerSetTubeReturnManualPower(myRobot.rBallScorer,0);	// not moving lift under manual power
	}
	if (joy2Btn(Btn9))	// Reset tube return position to stow when under manual control
	{
		BallScorerSetTubeReturnStateToStow(myRobot.rBallScorer);
	}
	BALLSCORERTask(myRobot.rBallScorer,curLiftState);
	//
	//			TubeToGoal BackToStow Toggle - move the tube to the goal, tip it and then reset it to stowed
	//	NOTE: Do this after other ball scorer tasks have operated and BALLSCORERTask has run. That way we won't do this if it is moving
	//
	if (joy2Btn(Btn8))
	{
		//writeDebugStreamLine("Robot> Joy2-Btn8 pressed");
		if (myRobot.bCanScoreTubeToGoalBackToStowToggle)
		{
			myRobot.bCanScoreTubeToGoalBackToStowToggle = false;	// force driver to release the trigger to do this again
			BALLSCORERSTATE curBallScorerState = GetBallScorerState(myRobot.rBallScorer);	// get this once
			if (BallScorerTubeToGoal == curBallScorerState)	// if the tube is at the goal, bring it back to stow
			{
				writeDebugStreamLine("Robot> stowing Tube");
				BallScorerStowTube(myRobot.rBallScorer,curLiftState);	// tube is at the goal, okay to stow
			}
			else if(BallScorerArmDeployedTubeStowed == curBallScorerState)
			{
				writeDebugStreamLine("Robot> TubeToGoal");
				GetBallScorerTubeToGoal(myRobot.rBallScorer,curLiftState);	// tube is stowed and arm is deployed, so it is okay to score tube to goal
			}
		}
	}
	else
	{
		myRobot.bCanScoreTubeToGoalBackToStowToggle = true;	// trigger is released, okay to do this
	}
	BALLSCORERTask(myRobot.rBallScorer,curLiftState);		// run this again after handling TubeToGoal BackToStow Toggle

}

void ROBOTRestore(ROBOT &myRobot, RESTORESTATE restoreState) {

	LIFTSTATE curLiftState;
	BALLSCORERSTATE ballScorerState;
	BALLGRABBERARMSTATE ballGrabberArmState;
	bool bOpenJaws = false;	// true if should open ball grabber jaws to let driver 2 know that robot is restored
	switch(restoreState)
	{
	case AllStowed:
		SetLiftState(myRobot.rLift,LiftStowed);	// reset lift state to stowed
		curLiftState				= LiftStowed;
		ballScorerState			= BallScorerAllStowed;
		ballGrabberArmState = BallGrabberArmStowed;
		break;
	case Released60cm:
		curLiftState				= LiftPos60cm;
		ballScorerState			= BallScorerArmDeployedTubeStowed;
		ballGrabberArmState = BallGrabberArmDeployed;
		bOpenJaws = true;	// arm is deployed, so open jaws
		break;
	case Released90cm:
		curLiftState				= LiftPos90cm;
		ballScorerState			= BallScorerArmDeployedTubeStowed;
		ballGrabberArmState = BallGrabberArmDeployed;
		bOpenJaws = true;	// arm is deployed, so open jaws
		break;
	case Released120cm:
		curLiftState				= LiftPos120cm;
		ballScorerState			= BallScorerArmDeployedTubeStowed;
		ballGrabberArmState = BallGrabberArmDeployed;
		bOpenJaws = true;	// arm is deployed, so open jaws
		break;
	}

	LIFTInit(myRobot.rLift, liftMotorID, liftBrakeServoID, true, curLiftState);
	BALLSCORERInit(myRobot.rBallScorer, tubeReturnMotorID, tubeTipperServoID, ballGrabberArmMotorID, ballGrabberJawsServoID, ballScorerState,
	curLiftState, ballGrabberArmState);
	if (bOpenJaws)
	{
		OpenBallGrabberJaws(myRobot.rBallScorer.bsBallGrabber);	// open the jaws to let driver 2 know that robot is restored
	}
	myRobot.bStateHasBeenRestored = true;	// robot is restored
}

void HandleRestore(ROBOT &myRobot) {
	if (Unchosen == myRobot.rChosenRestoreState)
	{ // Restore state has not been chosen
		writeDebugStreamLine("Wait drvr2... lm: %d",nMotorEncoder[myRobot.rLift.liftMotor.MotorID]);
		if (joy2Btn(Btn10))
		{
			if (joy2Btn(Btn1)) myRobot.rChosenRestoreState = AllStowed;
			if (joy2Btn(Btn2)) myRobot.rChosenRestoreState = Released60cm;
			if (joy2Btn(Btn3)) myRobot.rChosenRestoreState = Released90cm;
			if (joy2Btn(Btn4)) myRobot.rChosenRestoreState = Released120cm;
		}
	}
	else // Restore state has been chosen...
	{
		// ...but don't continue until all relevant buttons have been lifted
		if (!joy2Btn(Btn10) && !joy2Btn(Btn1) && !joy2Btn(Btn2) && !joy2Btn(Btn3) && !joy2Btn(Btn4)) {
			ROBOTRestore(myRobot, myRobot.rChosenRestoreState);
			myRobot.rChosenRestoreState = true;
			SetServoPosition(myRobot.kickstandArmServo, KICKSTAND_ARM_DEPLOYED);
			writeDebugStreamLine("Robot Restored");
		}
	}
}

void ROBOTTask(ROBOT &myRobot, SENSORS &mySensors) {   // this function is called periodically to execute robot operations by reading the joysticks and sending the appropriate commands to the different parts of the robot

	getJoystickSettings(joystick);

	HandleController1(myRobot, mySensors);

	if (myRobot.bStateHasBeenRestored) // Only accept controller 2 input once the robot's state has been restored
	{
		HandleController2(myRobot, mySensors);
	}
	else
	{
		HandleRestore(myRobot);
	}
}

/*
*  This function initializes the ROBOT from the pragma definitions in the main program
*/
void
ROBOTInit(ROBOT &myRobot, SENSORS &mySensors, bool bTeleop){
	//
	// Initialize the drive train:
	//    start out in goal grabbing mode (DriveTrainModeGoalGrabbing) That is what we most likely have to do at the start of teleop
	//    So we want smoothing on and speed = DriveTrainSpeedGoalGrabbing
	//
	DRIVETRAINInit(myRobot.rDriveTrain,mySensors.rsGyro,leftFrontMotorID,leftRearMotorID,rightFrontMotorID,rightRearMotorID,
	DefaultMotorSmoothFactor,DriveTrainSpeedGoalGrabbing,DriveTrainModeGoalGrabbing,parkingBrakeServoID);

	myRobot.bStateHasBeenRestored								= false; // state has not been restored at the beginning
	myRobot.rChosenRestoreState									= Unchosen;

	myRobot.bCanToggleDrivingMode								= false; // start out with toggling disabled. Wait for button to be not pressed (in ROBOTTask) before enabling it.

	myRobot.bCanToggleJawsMode									= false; // start out with toggling disabled
	myRobot.bCanToggleTippingMode								= false; // start out with toggling disabled
	myRobot.bCanScoreBallToTube									= false; // start out this disabled
	myRobot.bCanScoreTubeToGoalBackToStowToggle = false; // start out with this disabled
	myRobot.bCanMoveLift												= false; // start out with this disabled
	myRobot.bCanMoveGoalGrabber									= false; // start out with this disabled

	SERVOInit(myRobot.kickstandArmServo, kickstandArmServoID, KICKSTAND_ARM_STOWED, true);
	//
	// Mode specific initialization
	//
	if (bTeleop)
	{
		//
		// Initialize for Teleop
		// we are going to save initializing the lift and ball scorer until after robot restore, but
		//	we need to initialize some parts of the ball scorer
		BallScorerPreRestoreInit(myRobot.rBallScorer);
		//
		// Initialize the Goal Grabber.
		//	In teleop, we don't want to change the state of the goal grabber because it might already be grabbing a goal
		//
		GOALGRABBERInit(myRobot.rGoalGrabber,leftGoalGrabberArmServoID,rightGoalGrabberArmServoID,leftGoalStabilizerServoID,rightGoalStabilizerServoID,false);
	}
	else
	{
		// Initialize for Autonomous
		LIFTInit(myRobot.rLift,liftMotorID,liftBrakeServoID,true,LiftStowed);	// NOTE: For now set it
		LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);
		//
		// Initialize the Ball Scorer
		//
		BALLSCORERInit(myRobot.rBallScorer,tubeReturnMotorID,tubeTipperServoID,ballGrabberArmMotorID,ballGrabberJawsServoID, BallScorerAllStowed,
		curLiftState,BallGrabberArmStowed);
		//
		// Initialize the Goal Grabber
		//
		GOALGRABBERInit(myRobot.rGoalGrabber,leftGoalGrabberArmServoID,rightGoalGrabberArmServoID,leftGoalStabilizerServoID,rightGoalStabilizerServoID,true);
	}
	return;
}

#endif
