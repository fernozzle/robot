#ifndef _LIFT_H
#define _LIFT_H
//
//	Includes
//
#include "standardMotor.h"	// for STANDARD_MOTOR
#include "standardServo.h"	// for STANDARD_SERVO
//
// Constants
//
// Brake
//
#define LiftBrakeOnPos 219
#define LiftBrakeOffPos 255

typedef enum
{
	LiftBrakeOn,
	LiftBrakeOff
} LIFTBRAKESTATE;

//
// Lift
//
#define LiftMotorSmoothFactor NoMotorSmoothing   // we don't want any motor smoothing
#define LiftMotorDriveSpeed FullDriveSpeed    // we are going to vary the power

#define LiftStowedHeight 0
#define GoalHeight60cm 1200
#define GoalHeight90cm 3375
#define GoalHeight120cm 5666
#define LiftMaxHeight 6000
#define LiftMotorDriveMaxPower MaximumMotorPowerAndyMark		// we are using an AndyMark motor for the lift
#define LiftMotorPowerFactor 0.70	// go slower than the maximum for more repeatable lifts
#define LiftMotorMovePower (int) ((float) LiftMotorDriveMaxPower * LiftMotorPowerFactor)

typedef enum
{
	LiftStowed = 0,
	LiftMoving,
	LiftPos60cm,
	LiftPos90cm,
	LiftPos120cm,
	LiftManualControl	// lift is under manual control
} LIFTSTATE;

typedef struct {
	STANDARD_SERVO liftBrakeServo;
	LIFTBRAKESTATE liftBrakeState;
	STANDARD_MOTOR liftMotor;
	int liftManualPower;			// how much manual power is being sent to the lift
	LIFTSTATE curLiftState;
	LIFTSTATE targetLiftState;
	long prevLiftMotorEncoderValue;	// previous lift motor encoder position
	int liftMotorValueCounter;		// counter to detect that lift motor isn't moving
} LIFT;

void SetLiftBrakeOn(LIFT &myLift, bool bForceBrake = false) {	// put lift brake on unless it is already on
	//writeDebugStreamLine("Setting Lift brake on...");
	if ((LiftBrakeOn != myLift.liftBrakeState) || bForceBrake)
	{
		//writeDebugStreamLine("Lift brake off, so setting it on");
		SetServoPosition(myLift.liftBrakeServo,LiftBrakeOnPos);
		myLift.liftBrakeState = LiftBrakeOn;
	}
}

void SetLiftBrakeOff(LIFT &myLift) {	// take lift brake off unless it is already off
	//writeDebugStreamLine("Setting Lift brake off...");
	if (LiftBrakeOff != myLift.liftBrakeState)
	{
		//writeDebugStreamLine("Lift brake on, so setting it off");
		SetServoPosition(myLift.liftBrakeServo,LiftBrakeOffPos);
		myLift.liftBrakeState = LiftBrakeOff;
	}
}

void LIFTStop(LIFT &myLift) {	// stop the lift
	MOTORStop(myLift.liftMotor);	// turn off motor
	SetLiftBrakeOn(myLift,true);	// force the brake to be ON
	//writeDebugStreamLine("LiftStopped");
}

int GetLiftManualPower(LIFT &myLift) {	// get the manual power level
	return(myLift.liftManualPower);
}

void SetLiftManualPower(LIFT &myLift, int newPower) {	// set the manual power level
	myLift.liftManualPower = newPower;
}

LIFTSTATE GetCurLiftState(LIFT &myLift) {
	return(myLift.curLiftState);
}

bool IsLiftUnderManualControl(LIFT &myLift) {	// returns true if lift is under manual control
	return(LiftManualControl == GetCurLiftState(myLift));
}

bool IsLiftMoving(LIFT &myLift) {
	LIFTSTATE curLiftState = GetCurLiftState(myLift);
	return (LiftMoving == curLiftState);
}

long GetLiftMotorTargetVal(LIFTSTATE liftState) {
	switch(liftState)
	{
	case LiftStowed:
		return LiftStowedHeight;
		break;
	case LiftPos60cm:
		return GoalHeight60cm;
		break;
	case LiftPos90cm:
		return GoalHeight90cm;
		break;
	case LiftPos120cm:
		return GoalHeight120cm;
		break;
	default:
		writeDebugStreamLine("GetLiftMotorTargetValt> illegal state");
		return LiftStowedHeight;	// have to return something
		break;
	}
}

void MoveLiftInternal(LIFT &myLift, LIFTSTATE newLiftState) {
	// During teleop, we have to move the lift realitve to its state since the motor encoder can't be set to a specific value
	// and at the beginning of teleop it is always set to 0
	long liftMotorCurVal = GetLiftMotorTargetVal(myLift.curLiftState);
	long liftMotorTargetVal = GetLiftMotorTargetVal(newLiftState);
	long diffLiftMotorVal = liftMotorTargetVal - liftMotorCurVal;
	ResetMotorEncoderPosition(myLift.liftMotor);	// reset to zero
	writeDebugStreamLine("MLInt>diff %d",diffLiftMotorVal);
	SetLiftBrakeOff(myLift);	// release brake
	MOTORMoveToTarget(myLift.liftMotor,LiftMotorMovePower,diffLiftMotorVal);	// this will handle the direction for us
	myLift.targetLiftState = newLiftState;
	myLift.curLiftState = LiftMoving;
	myLift.prevLiftMotorEncoderValue = 0;
	myLift.liftMotorValueCounter = 0;
}

void MoveLift(LIFT &myLift, LIFTSTATE newLiftState) {
	//writeDebugStreamLine("Moving Lift...");

	if(IsLiftMoving(myLift)){
		//writeDebugStreamLine("Lift is already moving: target: %d command: %d",myLift.targetLiftState,newLiftState);
		return;
	}
	SetMotorDrivingSmoothFactor(myLift.liftMotor,NoMotorSmoothing);	// turn off smoothing while under automatic control for faster moves
	switch(newLiftState)
	{
	case LiftStowed:
		if (LiftStowed != myLift.curLiftState)	// don't do anything if we are already here
		{
			MoveLiftInternal(myLift,newLiftState);
		}
		break;
	case LiftPos60cm:
		if (LiftPos60cm != myLift.curLiftState)	// don't do anything if we are already here
		{
			MoveLiftInternal(myLift,newLiftState);
			writeDebugStreamLine("Moving lift to 60cm");
		}
		break;
	case LiftPos90cm:
		if (LiftPos90cm != myLift.curLiftState)	// don't do anything if we are already here
		{
			MoveLiftInternal(myLift,newLiftState);
			writeDebugStreamLine(">Moving lift to 90cm");
		}
		break;
	case LiftPos120cm:
		if (LiftPos120cm != myLift.curLiftState)	// don't do anything if we are already here
		{
			MoveLiftInternal(myLift,newLiftState);
			//writeDebugStreamLine("Moving lift to 120cm");
		}
		break;
	default:
		//writeDebugStreamLine("Lift> illegal state");
		break;
	}
}

void SetLiftState(LIFT &myLift, LIFTSTATE curLiftState) {	// used to reset the lift state to a paritcular state when under manual control
	writeDebugStreamLine("SettingLiftState: %d",curLiftState);
	MOTORStop(myLift.liftMotor);	// in any case, stop motor and set lift brake
	SetLiftBrakeOn(myLift);
	if (LiftStowed == curLiftState)
	{
		ResetMotorEncoderPosition(myLift.liftMotor);	// we want to reset this if lift is stowed, otherwise, leave it alone
	}
	myLift.curLiftState = curLiftState;
	myLift.targetLiftState = curLiftState;
}

void MoveLiftManual(LIFT &myLift, int power) {	// moves the lift under manual control
	//writeDebugStreamLine("Moving Lift Manually, power: %d",power);
	if (0 == power)
	{
		LIFTStop(myLift);	// stop the lift
	}
	else
	{
		SetMotorDrivingSmoothFactor(myLift.liftMotor,DefaultMotorSmoothFactor);	// turn on smoothing while under manual control
		myLift.targetLiftState = LiftManualControl;
		myLift.curLiftState = LiftMoving;
		myLift.liftManualPower = power;
		SetLiftBrakeOff(myLift);	// unlock brake when not moving
		MOTORMove(myLift.liftMotor,myLift.liftManualPower);
	}
}

void LIFTTask(LIFT &myLift) {

	if (!IsLiftMoving(myLift))	// if lift isn't moving, it should be stopped
	{
		LIFTStop(myLift);	// stop the lift
	}
	MOTORTask(myLift.liftMotor);	// Do motor task first before checking if motor is running
	if (IsMotorStopped(myLift.liftMotor)) {
		//writeDebugStreamLine("LiftTask> motor stopped moving");
		LIFTStop(myLift);	// stop the lift
		switch(myLift.targetLiftState)
		{
		case LiftStowed:
			myLift.curLiftState = LiftStowed;	// we made it
			break;
		case LiftPos60cm:
			myLift.curLiftState = LiftPos60cm;
			break;
		case LiftPos90cm:
			myLift.curLiftState = LiftPos90cm;
			break;
		case LiftPos120cm:
			myLift.curLiftState = LiftPos120cm;
			break;
		case LiftManualControl:
			myLift.curLiftState = LiftManualControl;
			break;
		}
	}
	else if(LiftManualControl == myLift.targetLiftState)
	{
		//writeDebugStreamLine("lift under manual control");
		if (0 == myLift.liftManualPower)	// we are under manual power and the power is zero -> shut off motor
		{
			LIFTStop(myLift);	// stop lift
			myLift.curLiftState = LiftManualControl;
			//writeDebugStreamLine("Lift Stopped> encoder value: %d",GetMotorEncoderPosition(myLift.liftMotor));
		}
	}
	else
	{
		long curLiftMotorPos = GetMotorEncoderPosition(myLift.liftMotor);
		if (curLiftMotorPos == myLift.prevLiftMotorEncoderValue)
		{
			myLift.liftMotorValueCounter++;
#define LiftMotorNotMovingCount 5
			if (myLift.liftMotorValueCounter >= LiftMotorNotMovingCount)
			{
				writeDebugStreamLine("Lift not moving. Force");
				//MoveLiftInternal(myLift,myLift.targetLiftState);
				MOTORSetPower(myLift.liftMotor,myLift.liftMotor.targetPower);
			}
		}
		else
		{
			myLift.prevLiftMotorEncoderValue = curLiftMotorPos;
			mylift.liftMotorValueCounter = 0;	// reset this
		}
		writeDebugStreamLine("Lift Moving: target: %d cur: %d",myLift.liftMotor.targetEncoderValue,curLiftMotorPos);
	}
	SERVOTask(myLift.liftBrakeServo);
}

void LIFTInit(LIFT &myLift, int liftMotorID, int brakeServoID, bool bSetInitialState, LIFTSTATE liftState) {
	bool bResetLiftMotorEncoder = true;	// default to resetting the lift motor encoder
	SERVOInit(myLift.liftBrakeServo,brakeServoID,LiftBrakeOn,true);  // start with brake on regardless of bSetInitialState
	myLift.liftBrakeState = LiftBrakeOff;	// this will force SetLiftBrakeOn to tell the servo to set its position
	SetLiftBrakeOn(myLift);	// this will set the state
	myLift.liftManualPower = 0;	// no manual power yet
	myLift.prevLiftMotorEncoderValue = 0;
	if (bSetInitialState)	// only set initial state if asked
	{
		myLift.curLiftState = liftState;
		long curLiftMotorPos = 0;	// default to stowed
		switch(myLift.curLiftState)	// we need to set the lift motor encoder depending on the state
		{
		case LiftStowed:
			curLiftMotorPos = LiftStowedHeight;	// stowed
			bResetLiftMotorEncoder = true;	// we are stowed, so we want the lift motor encoder reset
			break;
		case LiftPos60cm:
			curLiftMotorPos = GoalHeight60cm;	// 60cm
			bResetLiftMotorEncoder = false;	// we don't want to reset the lift motor encoder
			break;
		case LiftPos90cm:
			curLiftMotorPos = GoalHeight90cm;	// 90cm
			bResetLiftMotorEncoder = false;	// we don't want to reset the lift motor encoder
			break;
		case LiftPos120cm:
			curLiftMotorPos = GoalHeight120cm;	// 120cm
			bResetLiftMotorEncoder = false;	// we don't want to reset the lift motor encoder
			break;
		case LiftManualControl:
			curLiftMotorPos = GoalHeight120cm;	// manual control, so start at maximum height
			myLift.curLiftState = LiftManualControl;
			bResetLiftMotorEncoder = false;	// we don't want to reset the lift motor encoder
			break;
		}
		myLift.targetLiftState = liftState;	// no target yet
		myLift.prevLiftMotorEncoderValue = curLiftMotorPos;
		myLift.liftMotorValueCounter = 0;
	}
	MOTORInit(myLift.liftMotor,liftMotorID,LiftMotorSmoothFactor,LiftMotorDriveSpeed,bResetLiftMotorEncoder);  // okay to set a motor regardless of bSetInitialState
	writeDebugStreamLine("LiftInit>curPos %d m: %d",myLift.liftMotor.actualEncoderValue,nMotorEncoder[myLift.liftMotor.motorID]);
	LIFTTask(myLift);
}
#endif
