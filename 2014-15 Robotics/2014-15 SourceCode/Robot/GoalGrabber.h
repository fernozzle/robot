#ifndef _GOAL_GRABBER_H
#define _GOAL_GRABBER_H

//
//	Includes
//
#include "standardServo.h"	// for STANDARD_SERVO

const int LeftGoalGrabberArmOpen = 132;
const int LeftGoalGrabberArmClosed = 0;
const int LeftGoalGrabberArmGrab = 98;
const int LeftGoalGrabberArmGrabFirm = 92;
const int LeftGoalGrabberArmPlow = 30;
const int RightGoalGrabberArmOpen = 49;
const int RightGoalGrabberArmClosed = 206;
const int RightGoalGrabberArmGrab = 93;
const int RightGoalGrabberArmGrabFirm = 99;
const int RightGoalGrabberArmPlow = 141;

const int LeftGoalStablizerStowed = 0;
const int LeftGoalStablizerDeployed = 218;
const int RightGoalStablizerStowed = 0;
const int RightGoalStablizerDeployed = 214;

const int PlowingCountdownStart = 5;	// wait this many cycles before moving opposite side arm when plowing

typedef enum {
	GoalGrabberClosed,	// Starting position -> everything stowed. This is what we need for the start of autonomous mode
	GoalGrabberOpen,		// Open, ready to grab goal
	GoalGrabberGrab,		// Grab goal
	GoalGrabberPlow,		// Plow position so we won't capture balls
	GoalGrabberPlowing	// moving into plow position, so left arm can go first
} GOALGRABBERSTATE;

typedef struct {
	STANDARD_SERVO leftGoalGrabberArmServo;
	STANDARD_SERVO rightGoalGrabberArmServo;
	STANDARD_SERVO leftGoalStabilizerServo;
	STANDARD_SERVO rightGoalStabilizerServo;
	GOALGRABBERSTATE curState;		// current state
	int plowingCountdown;					// used to delay the movement of one side when going to plow position
} GOAL_GRABBER;

void SetGoalGrabberLeftSideArm(GOAL_GRABBER &myGoalGrabber,GOALGRABBERSTATE inputState){
	switch(inputState) {
	case GoalGrabberClosed:
		SetServoPosition(myGoalGrabber.leftGoalGrabberArmServo,LeftGoalGrabberArmClosed);
		break;
	case GoalGrabberOpen:
		SetServoPosition(myGoalGrabber.leftGoalGrabberArmServo,LeftGoalGrabberArmOpen);
		break;
	case GoalGrabberGrab:
		SetServoPosition(myGoalGrabber.leftGoalGrabberArmServo,LeftGoalGrabberArmGrab);
		break;
	case GoalGrabberPlow:
		SetServoPosition(myGoalGrabber.leftGoalGrabberArmServo,LeftGoalGrabberArmPlow);
		break;
	}
}

void SetGoalGrabberLeftStablizer(GOAL_GRABBER &myGoalGrabber,GOALGRABBERSTATE inputState){
	switch(inputState) {
	case GoalGrabberClosed:
		SetServoPosition(myGoalGrabber.leftGoalStabilizerServo,LeftGoalStablizerStowed);
		break;
	case GoalGrabberOpen:
		SetServoPosition(myGoalGrabber.leftGoalStabilizerServo,LeftGoalStablizerStowed);
		break;
	case GoalGrabberGrab:
		SetServoPosition(myGoalGrabber.leftGoalStabilizerServo,LeftGoalStablizerDeployed);
		break;
	case GoalGrabberPlow:
		SetServoPosition(myGoalGrabber.leftGoalStabilizerServo,LeftGoalStablizerDeployed);	// deploy stabilizer in plow position to prevent balls from getting in there
		break;
	}
}

void SetGoalGrabberRightSideArm(GOAL_GRABBER &myGoalGrabber,GOALGRABBERSTATE inputState){
	switch(inputState) {
	case GoalGrabberClosed:
		SetServoPosition(myGoalGrabber.rightGoalGrabberArmServo,RightGoalGrabberArmClosed);
		break;
	case GoalGrabberOpen:
		SetServoPosition(myGoalGrabber.rightGoalGrabberArmServo,RightGoalGrabberArmOpen);
		break;
	case GoalGrabberGrab:
		SetServoPosition(myGoalGrabber.rightGoalGrabberArmServo,RightGoalGrabberArmGrab);
		break;
	case GoalGrabberPlow:
		SetServoPosition(myGoalGrabber.rightGoalGrabberArmServo,RightGoalGrabberArmPlow);
		break;
	}
}

void SetGoalGrabberRightStablizer(GOAL_GRABBER &myGoalGrabber,GOALGRABBERSTATE inputState){
	switch(inputState) {
	case GoalGrabberClosed:
		SetServoPosition(myGoalGrabber.rightGoalStabilizerServo,RightGoalStablizerStowed);
		break;
	case GoalGrabberOpen:
		SetServoPosition(myGoalGrabber.rightGoalStabilizerServo,RightGoalStablizerStowed);
		break;
	case GoalGrabberGrab:
		SetServoPosition(myGoalGrabber.rightGoalStabilizerServo,RightGoalStablizerDeployed);
		break;
	case GoalGrabberPlow:
		SetServoPosition(myGoalGrabber.rightGoalStabilizerServo,RightGoalStablizerDeployed);	// deploy stabilizer in plow position to prevent balls from getting in there
		break;
	}
}

void SetGoalGrabberSideArms(GOAL_GRABBER &myGoalGrabber, GOALGRABBERSTATE inputState){
	SetGoalGrabberLeftSideArm(myGoalGrabber,inputState);
	SetGoalGrabberRightSideArm(myGoalGrabber,inputState);
}

void SetGoalGrabberStabilizers(GOAL_GRABBER &myGoalGrabber, GOALGRABBERSTATE inputState){
	SetGoalGrabberLeftStablizer(myGoalGrabber,inputState);
	SetGoalGrabberRightStablizer(myGoalGrabber,inputState);
}

void SetLeftSideGrabberArms(GOAL_GRABBER &myGoalGrabber, GOALGRABBERSTATE inputState){
	SetGoalGrabberLeftSideArm(myGoalGrabber,inputState);
	SetGoalGrabberLeftStablizer(myGoalGrabber,inputState);
}

void SetRightSideGrabberArms(GOAL_GRABBER &myGoalGrabber, GOALGRABBERSTATE inputState){
	SetGoalGrabberRightSideArm(myGoalGrabber,inputState);
	SetGoalGrabberRightStablizer(myGoalGrabber,inputState);
}

void GOALGRABBERSetState(GOAL_GRABBER &myGoalGrabber, GOALGRABBERSTATE inputState){
	myGoalGrabber.curState = inputState;
	if (GoalGrabberPlow == myGoalGrabber.curState)	// when plowing, we need to move the left arms first
	{
		writeDebugStreamLine("Plowing");
		myGoalGrabber.curState = GoalGrabberPlowing;	// plowing - servo task will handle moving the right arms
		myGoalGrabber.plowingCountdown = PlowingCountdownStart;	// initialize countdown
		SetGoalGrabberLeftSideArm(myGoalGrabber,inputState);
		SetGoalGrabberLeftStablizer(myGoalGrabber,inputState);
	}
	else
	{	// not plowing, go ahead and move now
		SetGoalGrabberLeftSideArm(myGoalGrabber,inputState);
		SetGoalGrabberLeftStablizer(myGoalGrabber,inputState);
		SetGoalGrabberRightSideArm(myGoalGrabber,inputState);
		SetGoalGrabberRightStablizer(myGoalGrabber,inputState);
	}
}

void GOALGRABBERTask(GOAL_GRABBER &myGoalGrabber) {
	if (GoalGrabberPlowing == myGoalGrabber.curState)		// we are moving into the plowing position. See if it is time to move the opposite side arms
	{
		myGoalGrabber.plowingCountdown--;	// countdown timer
		if (myGoalGrabber.plowingCountdown <= 0)
		{
		writeDebugStreamLine("Plowed");
			SetGoalGrabberRightSideArm(myGoalGrabber,GoalGrabberPlow);	// time to move the opposite side
			SetGoalGrabberRightStablizer(myGoalGrabber,GoalGrabberPlow);
			myGoalGrabber.curState = GoalGrabberPlow;			// we made it
		}
	}
	SERVOTask(myGoalGrabber.leftGoalGrabberArmServo);
	SERVOTask(myGoalGrabber.rightGoalGrabberArmServo);
	SERVOTask(myGoalGrabber.leftGoalStabilizerServo);
	SERVOTask(myGoalGrabber.rightGoalStabilizerServo);
}

void GOALGRABBERInit(GOAL_GRABBER &myGoalGrabber, int leftGoalGrabberArmServoID,
int rightGoalGrabberArmServoID, int leftGoalStabilizerServoID, int rightGoalStabilizerServoID,bool bSetInitialState) {
	SERVOInit(myGoalGrabber.leftGoalGrabberArmServo,leftGoalGrabberArmServoID,LeftGoalGrabberArmClosed,bSetInitialState);
	SERVOInit(myGoalGrabber.rightGoalGrabberArmServo,rightGoalGrabberArmServoID,RightGoalGrabberArmClosed,bSetInitialState);
	SERVOInit(myGoalGrabber.leftGoalStabilizerServo,leftGoalStabilizerServoID,LeftGoalStablizerStowed,bSetInitialState);
	SERVOInit(myGoalGrabber.rightGoalStabilizerServo,rightGoalStabilizerServoID,RightGoalStablizerStowed,bSetInitialState);
	if (bSetInitialState)
	{
		GOALGRABBERSetState(myGoalGrabber, GoalGrabberClosed);  // when autonomous starts we want to set the initial state
	}
	myGoalGrabber.plowingCountdown = 0;	// not plowing yet
}
#endif
