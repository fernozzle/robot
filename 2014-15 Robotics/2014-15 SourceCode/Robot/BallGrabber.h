#ifndef _BALL_GRABBER_H
#define _BALL_GRABBER_H
//
//	Includes
//
#include "standardMotor.h"	// for STANDARD_MOTOR
#include "standardServo.h"	// for STANDARD_SERVO
//
// Constants
//
// Jaws
//
#define BalllGrabberJawsOpenPos 111
#define BalllGrabberJawsClosedPos 146
#define BallGrabberJawsClosingCountdownStart 6	// take this many cycles to say the jaws are closed

//
// Arm - now using AndyMark motor
//
#define BallGrabberArmSmoothFactor NoMotorSmoothing  // we don't want any motor smoothing
#define BallGrabberArmDriveSpeed FullDriveSpeed    // we are going to vary the power
#define BallGrabberArmStowPower 47	// tried lower and small balls don't make it. -> tetrix was 60
#define BallGrabberArmDeployPower  -20		// tetrix was -20
#define BallGrabberMotorStowTarget 390		// turn off motor when these are reached -> tetrix was 475
#define BallGrabberMotorDeployTarget -470	// motor will act as a brake -> tetrix was -470

//
// enums
//
typedef enum
{
	BallGrabberArmStowed,
	BallGrabberArmStowing,
	BallGrabberArmDeployed,
	BallGrabberArmDeploying
} BALLGRABBERARMSTATE;

typedef enum
{
	BallGrabberJawsClosed,
	BallGrabberJawsClosing, // We will count down BallGrabberJawsClosingCountdownStart cycles before saying the jaws are closed
	BallGrabberJawsOpen
} BALLGRABBERJAWSSTATE;

typedef struct {
	STANDARD_SERVO jawsServo;
	STANDARD_MOTOR armMotor;
	BALLGRABBERARMSTATE armState;
	BALLGRABBERJAWSSTATE jawsState;
	long prevArmEncoderValue;		// previous arm motor encoder position
	int armMotorValueCounter;		// counter to detect that arm motor isn't moving
	int jawsClosingCountdown;		// this counts down from BallGrabberJawsClosingCountdownStart before the jaws are determined to be closed
} BALL_GRABBER;

#define BallGrabberInitClosed(myBallGrabber) SERVOInit(myBallGrabber.jawsServo,ballGrabberJawsServoID,BalllGrabberJawsClosedPos,true);  // jaws closed

void BALL_GRABBERStop(BALL_GRABBER &myBallGrabber) {	// stop the ball grabber - this can happen during autonomous when there are problems
	MOTORStop(myBallGrabber.armMotor);	// turn of ball grabber arm
	SetServoPosition(myBallGrabber.jawsServo,BalllGrabberJawsClosedPos); // close jaws. That will be safer
}

void SetBallGrabberState(BALL_GRABBER &myBallGrabber, BALLGRABBERARMSTATE armState, BALLGRABBERJAWSSTATE jawsState){
	myBallGrabber.armState = armState;
	myBallGrabber.jawsState = jawsState;
}

#define GetBallGrabberArmState(myBallGrabber) myBallGrabber.armState	// return arm state
#define GetBallGrabberJawsState(myBallGrabber) myBallGrabber.jawsState	// return jaws state
/*
bool AreBallGrabberJawsClosed(BALL_GRABBER &myBallGrabber) {
	return(BallGrabberJawsClosed == GetBallGrabberJawsState(myBallGrabber));
}

bool AreBallGrabberJawsOpen(BALL_GRABBER &myBallGrabber) {
	return(BallGrabberJawsOpen == GetBallGrabberJawsState(myBallGrabber));
}
*/
bool IsBallGrabberStowed(BALL_GRABBER &myBallGrabber) {	// is the ball grabber stowed?
	return (BallGrabberArmStowed == GetBallGrabberArmState(myBallGrabber));
}

void GetBallGrabberState(BALL_GRABBER &myBallGrabber, BALLGRABBERARMSTATE &curArmState, BALLGRABBERJAWSSTATE &curJawsState){
	curArmState = GetBallGrabberArmState(myBallGrabber);
	curJawsState = GetBallGrabberJawsState(myBallGrabber);
}
/*
bool IsBallGrabberReadyToScore(BALL_GRABBER &myBallGrabber) {
	BALLGRABBERARMSTATE curArmState;
	BALLGRABBERJAWSSTATE curJawsState;
	GetBallGrabberState(myBallGrabber,curArmState,curJawsState);
	if ((BallGrabberArmDeployed == curArmState) && (BallGrabberJawsOpen == curJawsState))
	{
		return true;
	}
	else
	{
		return false;
	}
}
*/

void CloseBallGrabberJaws(BALL_GRABBER &myBallGrabber) {  // always okay to close jaws
	 SetServoPosition(myBallGrabber.jawsServo,BalllGrabberJawsClosedPos); // set servo position no matter what in case it got knocked out of alignment
	if (BallGrabberJawsOpen == myBallGrabber.jawsState)
	{
		myBallGrabber.jawsState = BallGrabberJawsClosing;  // jaws are closing
		myBallGrabber.jawsClosingCountdown = BallGrabberJawsClosingCountdownStart;	// start the countdown
	}
}

void OpenBallGrabberJaws(BALL_GRABBER &myBallGrabber) {  // Only open jaws if arm is deployed or deploying (if we allow it to open while deploying it will be a little faster to get the next ball)
	if ((BallGrabberArmDeployed == myBallGrabber.armState) || (BallGrabberArmDeploying == myBallGrabber.armState))
	{
		SetServoPosition(myBallGrabber.jawsServo,BalllGrabberJawsOpenPos);
		myBallGrabber.jawsState = BallGrabberJawsOpen;  // jaws are open
	}
}

void SetBallGrabberJaws(BALL_GRABBER &myBallGrabber, BALLGRABBERJAWSSTATE jawState) {
	if (BallGrabberJawsOpen == jawState)
	{
		OpenBallGrabberJaws(myBallGrabber);
	}
	else if(BallGrabberJawsClosed == jawState)
	{
		CloseBallGrabberJaws(myBallGrabber);
	}
}

void ToggleBallGrabberJaws(BALL_GRABBER &myBallGrabber) {
	if (BallGrabberJawsOpen == myBallGrabber.jawsState)
	{
		CloseBallGrabberJaws(myBallGrabber);
	}
	else if(BallGrabberJawsClosed == myBallGrabber.jawsState)
	{
		OpenBallGrabberJaws(myBallGrabber);
	}
}

void StowBallGrabberArm(BALL_GRABBER &myBallGrabber) {
	if (BallGrabberArmDeployed == myBallGrabber.armState)	// it only makes sense to stow if we are deployed
	{
		if (BallGrabberJawsClosed == myBallGrabber.jawsState) // only stow if jaws are closed
		{
			ResetMotorEncoderPosition(myBallGrabber.armMotor);	// reset this before we move
			myBallGrabber.prevArmEncoderValue = 0;
			myBallGrabber.armMotorValueCounter = 0;
			MOTORMoveToTarget(myBallGrabber.armMotor,BallGrabberArmStowPower,BallGrabberMotorStowTarget);
			myBallGrabber.armState = BallGrabberArmStowing;  // arm is stowing are open
		}
	}
}

void DeployBallGrabberArm(BALL_GRABBER &myBallGrabber) {
	if (BallGrabberArmStowed == myBallGrabber.armState)	// it only makes sense to deploy if we are stowed
	{
		//writeDebugStreamLine("deploy ball grabber: stowed, check");
		if (BallGrabberJawsClosed == myBallGrabber.jawsState) // only deploy if jaws are closed
		{
			//writeDebugStreamLine("deploy ball grabber: jaws closed, check");
			ResetMotorEncoderPosition(myBallGrabber.armMotor);	// reset this before we move
			myBallGrabber.prevArmEncoderValue = 0;
			myBallGrabber.armMotorValueCounter = 0;
			MOTORMoveToTarget(myBallGrabber.armMotor,BallGrabberArmDeployPower,BallGrabberMotorDeployTarget);
			myBallGrabber.armState = BallGrabberArmDeploying;  // arm is stowing
		}
	}
}

void BALLGRABBERTask(BALL_GRABBER &myBallGrabber) {
	//
	// Jaws
	// Change state from closing to closed. That will allow us to allow time for the jaws to close before trying to score the ball(s) into the tube.
	//
	SERVOTask(myBallGrabber.jawsServo);
	if (BallGrabberJawsClosing == myBallGrabber.jawsState)
	{
		myBallGrabber.jawsClosingCountdown--;	// decrease the countdown
		if (myBallGrabber.jawsClosingCountdown <= 0)
		{
			myBallGrabber.jawsState = BallGrabberJawsClosed;  // they were closing, they are now closed.
		}
	}
	//
	// Arm Motor
	//
	MOTORTask(myBallGrabber.armMotor);  // execute motor task before checking status
	if (IsMotorStopped(myBallGrabber.armMotor))
	{	// Motor is stopped, the tube ball grabber arm motor has reached its target if it was deploying or stowing
STOPARMMOTOR:
	  if (BallGrabberArmDeploying == myBallGrabber.armState)
		{
			myBallGrabber.armState = BallGrabberArmDeployed;	// we are now deployed. Okay to open jaws
			ResetMotorEncoderPosition(myBallGrabber.armMotor);	// reset this after we move
			//writeDebugStreamLine("BG deployed");
		}
		else if(BallGrabberArmStowing == myBallGrabber.armState)
		{
			myBallGrabber.armState = BallGrabberArmStowed;	// we are now stowed.
			ResetMotorEncoderPosition(myBallGrabber.armMotor);	// reset this after we move
			//writeDebugStreamLine("BG stowed");
		}
	}
	else
	{
		long curArmMotorPos = GetMotorEncoderPosition(myBallGrabber.armMotor);
		if (curArmMotorPos == myBallGrabber.prevArmEncoderValue)
		{
			myBallGrabber.armMotorValueCounter++;
#define ArmMotorNotMovingCount 10
			if (myBallGrabber.armMotorValueCounter >= ArmMotorNotMovingCount)
			{
				writeDebugStreamLine("Arm not moving. Stop");
				MOTORStop(myBallGrabber.armMotor);
				goto STOPARMMOTOR;
			}
		}
		else
		{
			myBallGrabber.prevArmEncoderValue = curArmMotorPos;
			myBallGrabber.armMotorValueCounter = 0;	// reset this
		}
		writeDebugStreamLine("BG arm pos: %d",curArmMotorPos);	// ball grabber arm is moving
	}
}

void BALLGRABBERInit(BALL_GRABBER &myBallGrabber, int armMotorID, int jawsServoID, BALLGRABBERARMSTATE ballGrabberArmState) {
	MOTORInit(myBallGrabber.armMotor,armMotorID,BallGrabberArmSmoothFactor,BallGrabberArmDriveSpeed);
	SERVOInit(myBallGrabber.jawsServo,jawsServoID,BalllGrabberJawsClosedPos,true);  // start with jaws closed
	SetBallGrabberState(myBallGrabber,ballGrabberArmState,BallGrabberJawsClosed);   // arm stowed, jaws closed
	myBallGrabber.prevArmEncoderValue = 0;
	myBallGrabber.armMotorValueCounter = 0;
	myBallGrabber.jawsClosingCountdown = 0;	// not closing the jaws yet
	BALLGRABBERTask(myBallGrabber);
}
#endif
