// encapsulates properties/functions of the drive train
// TODO: drivetrain stalled

#ifndef _DRIVE_TRAIN_H
#define _DRIVE_TRAIN_H

//
//	Includes
//
#include "standardMotor.h"	// for STANDARD_MOTOR
#include "standardServo.h"	// for STANDARD_SERVO
//
// Constants
//
#define NumberOfDriveTrainMotors  4	// 4 drive train motors - 2 for each side
#define TurnToTargetSlowDownRange 25.0		// when we get this many degrees from the target, slow down - tried lower and it didn't work
#define DTMoveToTargetSlowDownRange 300		// for drive train, when we get this close to the target, slow down
#define DTMoveToTargetSlowPower 25	// slow power for drive train
//
// Parking Brake
//
const int ParkingBrakeOnPos = 216;
const int ParkingBrakeOffPos = 255;

typedef enum
{
	ParkingBrakeOn,
	ParkingBrakeOff
} PARKINGBRAKESTATE;

typedef enum {
	DriveTrainModeGoalGrabbing,	// forward is with goal grabber
	DriveTrainModeBallGrabbing	// forward is with ball grabber
} DriveTrainMode;

typedef enum {
	DriveTrainStop,
	DriveTrainTank,
	DriveTrainForward,
	DriveTrainReverse,
	DriveTrainForwardToTarget,
	DriveTrainReverseToTarget,
	DriveTrainTurnLeftToTarget,
	DriveTrainTurnRightToTarget,
	DriveTrainStalled
} DriveTrainState;

typedef struct {
	STANDARD_MOTOR leftFrontMotor;
	STANDARD_MOTOR leftRearMotor;
	STANDARD_MOTOR rightFrontMotor;
	STANDARD_MOTOR rightRearMotor;
	STANDARD_SERVO parkingBrakeServo;
	PARKINGBRAKESTATE parkingBrakeState;
	long targetEncoderValue;
	long actualEncoderValue;
	long prevActualEncoderValue;
	int leftTargetPower;	// left side target power
	int leftActualPower;	// smoothed
	int rightTargetPower;	// right side target power
	int rightActualPower;	// smoothed
	int actualSpeed;	// revs/sec
	int prevTime;
	int drivingSmoothFactor;	// Range: 0-100 0 = no smoothing, 100 = no movement
	float driveSpeed;					// driving speed used for slower, more precise driving 0.0 = no movement, 100 = full speed. A good value is DefaultSlowDriveSpeed for slow driving
	int lastDistCheckTime;		// the last time we checked for the distance travelled
	int moveToStallTimeLimit;	// the maximum number of ms to run the motor until stall (0 = don't check this)
	int moveToStallStartTime;	// The start time of trying to run the motor until stalling
	int lastStallCheckTime;		// The last time we checked the stall time
	long prevStallCheckEncoderValue;	// this is the motor encoder value the last time we checked for stalling
	float targetDegrees;	// target for gyro turning
	float curDegrees;			// current degrees
	DriveTrainState state;
	DriveTrainMode drivingMode;	// goal grabbing or ball grabbing
} DRIVETRAIN;

//
//	Parking Brake
//
#define GetParkingBrakeState(d) d.parkingBrakeState	// return parking brake state
#define SetParkingBrakeOn(d) if (ParkingBrakeOn != d.parkingBrakeState) {SetServoPosition(d.parkingBrakeServo,ParkingBrakeOnPos); \
	d.parkingBrakeState = ParkingBrakeOn;}	// set parking brake on
#define SetParkingBrakeOff(d) if (ParkingBrakeOff != d.parkingBrakeState) {SetServoPosition(d.parkingBrakeServo,ParkingBrakeOffPos);	\
	d.parkingBrakeState = ParkingBrakeOff};	// set parking brake off
//
//	DriveTrain
//
float GetDriveTrainGyroPosition(DRIVETRAIN &myDriveTrain) {	// returns the current gyro position
	return myDriveTrain.curDegrees;
}

float UdpateDriveTrainGyroPosition(DRIVETRAIN &myDriveTrain, GYRO &myGyro) {	// updates and returns the current gyro position
	myDriveTrain.curDegrees = GyroGetHeading(myGyro);
	return GetDriveTrainGyroPosition(myDriveTrain);
}

void SetDriveTrainEncoderPosition(DRIVETRAIN &myDriveTrain, long newPos) {
	SetMotorEncoderPosition(myDriveTrain.leftFrontMotor, newPos);	// set all motor encoder positions
	SetMotorEncoderPosition(myDriveTrain.leftRearMotor, newPos);	// reset all motor encoder positions
	SetMotorEncoderPosition(myDriveTrain.rightFrontMotor, newPos);	// reset all motor encoder positions
	SetMotorEncoderPosition(myDriveTrain.rightRearMotor, newPos);	// reset all motor encoder positions
	myDriveTrain.actualEncoderValue = newPos;
}

// reset the encoder value
void ResetDriveTrainEncoderPosition(DRIVETRAIN &myDriveTrain) {
	SetDriveTrainEncoderPosition(myDriveTrain,0);	// reset all motor encoder positions
}

// Returns the Drive Train encoder value
//	Takes the average of all the motor encoders
long GetDriveTrainEncoderValue(DRIVETRAIN &myDriveTrain) {
	long sumEncoderVal = GetMotorEncoderPosition(myDriveTrain.leftFrontMotor);	// start sum with this one
	sumEncoderVal += GetMotorEncoderPosition(myDriveTrain.leftRearMotor);
	sumEncoderVal += GetMotorEncoderPosition(myDriveTrain.rightFrontMotor);
	sumEncoderVal += GetMotorEncoderPosition(myDriveTrain.rightRearMotor);
	return sumEncoderVal / NumberOfDriveTrainMotors;	// calculate average
}

// Update the Drive Train encoder value
//	updates  the encoder values for all motors
void UpdateDriveTrainEncoderPosition(DRIVETRAIN &myDriveTrain) {
	UpdateMotorEncoderPosition(myDriveTrain.leftFrontMotor);
	UpdateMotorEncoderPosition(myDriveTrain.leftRearMotor);
	UpdateMotorEncoderPosition(myDriveTrain.rightFrontMotor);
	UpdateMotorEncoderPosition(myDriveTrain.rightRearMotor);
	myDriveTrain.actualEncoderValue = GetDriveTrainEncoderValue(myDriveTrain);
}

void SetDriveTrainMode(DRIVETRAIN &myDriveTrain, DriveTrainMode newMode) {
	myDriveTrain.drivingMode = newMode;
}

DriveTrainMode GetDriveTrainMode(DRIVETRAIN &myDriveTrain) {
	return myDriveTrain.drivingMode;
}
DriveTrainState GetDriveTrainState(DRIVETRAIN &myDriveTrain) {
	return myDriveTrain.state;
}

bool IsDriveTrainMoving(DRIVETRAIN &myDriveTrain) {
	DriveTrainState state = GetDriveTrainState(myDriveTrain);
	if ((DriveTrainStop == state) || (DriveTrainStalled == state))
	{
		return false;
	}
	else
	{
		return true;
	}
}
bool IsDriveTraiStopped(DRIVETRAIN &myDriveTrain) {
	return !IsDriveTrainMoving(myDriveTrain);
}

void UpdateDriveTrainState(DRIVETRAIN &myDriveTrain, int power, bool bMovingToTarget)
{
	if (power > 0)
	{
		if (bMovingToTarget)
		{
			myDriveTrain.state = DriveTrainForwardToTarget;
		}
		else
		{
			myDriveTrain.state = DriveTrainForward;
		}
	}
	else if (power < 0)
	{
		if (bMovingToTarget)
		{
			myDriveTrain.state = DriveTrainReverseToTarget;
		}
		else
		{
			myDriveTrain.state = DriveTrainReverse;
		}
	}
	else
	{
		myDriveTrain.state = DriveTrainStop;
	}
}

// stops the drivetrain
void DRIVETRAINStopLeftMotors(DRIVETRAIN &myDriveTrain) {
	MOTORStop(myDriveTrain.leftFrontMotor);
	MOTORStop(myDriveTrain.leftRearMotor);
	myDriveTrain.leftTargetPower = 0;
	myDriveTrain.leftActualPower = 0;
}
void DRIVETRAINStopRightMotors(DRIVETRAIN &myDriveTrain) {
	MOTORStop(myDriveTrain.rightFrontMotor);
	MOTORStop(myDriveTrain.rightRearMotor);
	myDriveTrain.rightTargetPower = 0;
	myDriveTrain.rightActualPower = 0;
}
void DRIVETRAINStop(DRIVETRAIN &myDriveTrain) {
	writeDebugStreamLine("Stopping DriveTrain");
	DRIVETRAINStopLeftMotors(myDriveTrain);
	DRIVETRAINStopRightMotors(myDriveTrain);
	myDriveTrain.actualSpeed = 0;
	myDriveTrain.moveToStallTimeLimit = 0;	// no longer checking for stalling
	myDriveTrain.state = DriveTrainStop;
}

//	Sets the left motors power
void DRIVETRAINSetLeftMotorsPower(DRIVETRAIN &myDriveTrain, int power) {
	myDriveTrain.leftActualPower = power;
	MOTORMove(myDriveTrain.leftFrontMotor,myDriveTrain.leftActualPower);
	MOTORMove(myDriveTrain.leftRearMotor, myDriveTrain.leftActualPower);
}
//	Sets the right motors power
void DRIVETRAINSetRightMotorsPower(DRIVETRAIN &myDriveTrain, int power) {
	myDriveTrain.rightActualPower = power;
	MOTORMove(myDriveTrain.rightFrontMotor,myDriveTrain.rightActualPower);
	MOTORMove(myDriveTrain.rightRearMotor, myDriveTrain.rightActualPower);
}

// Moves the left side of the drive train according to the target power
//	If drivingSmoothFactor !=  NoMotorSmoothing, the actual power is calculated and sent to the motors in the DRIVETRAINTask
void DRIVETRAINMoveLeftMotors(DRIVETRAIN &myDriveTrain, int targetPower) {
	if (DriveTrainModeGoalGrabbing == myDriveTrain.drivingMode)
	{
		myDriveTrain.leftTargetPower = targetPower;
	}
	else
	{
		myDriveTrain.leftTargetPower = -targetPower;	// driving in reverse for ball grabbing
	}
	if (NoMotorSmoothing == myDriveTrain.drivingSmoothFactor) {
		int power = myDriveTrain.leftTargetPower;		// no smoothing
		power *= myDriveTrain.driveSpeed;		// modify by the driving speed
		DRIVETRAINSetLeftMotorsPower(myDriveTrain,power);
	}
}

// Moves the right side of the drive train according to the target power
//	If drivingSmoothFactor !=  NoMotorSmoothing, the actual power is calculated and sent to the motors in the DRIVETRAINTask
void DRIVETRAINMoveRightMotors(DRIVETRAIN &myDriveTrain, int targetPower) {
	if (DriveTrainModeGoalGrabbing == myDriveTrain.drivingMode)
	{
		myDriveTrain.rightTargetPower = targetPower;
	}
	else
	{
		myDriveTrain.rightTargetPower = -targetPower;	// driving in reverse for ball grabbing
	}
	if (NoMotorSmoothing == myDriveTrain.drivingSmoothFactor) {
		int power = myDriveTrain.rightTargetPower;		// no smoothing
		power *= myDriveTrain.driveSpeed;		// modify by the driving speed
		DRIVETRAINSetRightMotorsPower(myDriveTrain,power);
	}
}

void DRIVETRAINSlow(DRIVETRAIN &myDriveTrain, int slowPower) {	// slows down the drive train
	int slowLeftPower = slowPower * sgn(myDriveTrain.leftActualPower);	// slow down while maintaining direction
	int slowRightPower = slowPower * sgn(myDriveTrain.rightActualPower);
	DRIVETRAINMoveLeftMotors(myDriveTrain,slowLeftPower);	// slow down when we get close if there is no smoothing
	DRIVETRAINMoveRightMotors(myDriveTrain,slowRightPower);
}

// sets the motor to target power basd on drive train mode
//	If drivingSmoothFactor !=  NoMotorSmoothing, the actual power is calculated and sent to the motor in the MOTORTask
void DRIVETRAINMove(DRIVETRAIN &myDriveTrain, int targetPower) {
	DRIVETRAINMoveLeftMotors(myDriveTrain, targetPower);
	DRIVETRAINMoveRightMotors(myDriveTrain, targetPower);
	UpdateDriveTrainState(myDriveTrain,targetPower, false);	// update state - not moving towards target
}
// Driving in tank mode with different power for each side.
//	Sets the motor to target power basd on drive train mode
//	If drivingSmoothFactor !=  NoMotorSmoothing, the actual power is calculated and sent to the motor in the MOTORTask
void DRIVETRAINMoveTank(DRIVETRAIN &myDriveTrain, int targetPowerLeft, int targetPowerRight) {
	int powerLeft = targetPowerLeft;
	int powerRight = targetPowerRight;
	if (DriveTrainModeBallGrabbing == myDriveTrain.drivingMode)
	{
		powerLeft = targetPowerRight;	// swap if we are in ball grabbing mode
		powerRight = targetPowerLeft;
	}
	DRIVETRAINMoveLeftMotors(myDriveTrain, powerLeft);	// send each side its power
	DRIVETRAINMoveRightMotors(myDriveTrain, powerRight);
	myDriveTrain.state = DriveTrainTank;	// driving in tank mode
}

//
//	Move DriveTrain to Target
//
// 	Sets the DriveTrain target encoder value
// 	Once it reaches that target it will shut off
// 	Sets the DriveTrain to the target power (-100 to 100)
//	If target is less than current encoder value, the motor will automatically move in reverse
//

void DRIVETRAINMoveToTarget(DRIVETRAIN &myDriveTrain, int targetPower, long targetPos) {
	myDriveTrain.targetEncoderValue = targetPos;
	UpdateDriveTrainEncoderPosition(myDriveTrain);	// make sure this is up to date
	myDriveTrain.actualEncoderValue = GetDriveTrainEncoderValue(myDriveTrain);
	if (myDriveTrain.targetEncoderValue < myDriveTrain.actualEncoderValue) {
		targetPower = -abs(targetPower);	// this is okay since we got a copy of it; make sure it is negative
	}
	else
	{
		targetPower = abs(targetPower);	// this is okay since we got a copy of it; make sure it is positive
	}
	DRIVETRAINMove(myDriveTrain,targetPower);
	UpdateDriveTrainState(myDriveTrain,targetPower,true);	// We need to override the state - we are moving towards target
}

//
//	Turn Drivetrain using Gyro
//

void DRIVETRAINTurnToTarget(DRIVETRAIN &myDriveTrain, GYRO &myGyro, int targetPower, float targetDeg) {
	myDriveTrain.targetDegrees = targetDeg;	// our target
	myDriveTrain.curDegrees = 0;	// we haven't turned yet
	GYROReset(myGyro);	// reset gyro to 0 degrees
	int leftDriveTrainPower = abs(targetPower);	// default to turning right - make sure positive power
	int rightDriveTrainPower = -abs(targetPower);
	if (0 != targetDeg) {	// don't do anything unless targetDeg != 0
		if (targetDeg > 0)
		{
			myDriveTrain.state = DriveTrainTurnRightToTarget;	// turning right, power is already set up for turning right
			writeDebugStreamLine("Turning Right: %.1f degrees  leftPower: %d  rightPower: %d",targetDeg,leftDriveTrainPower,rightDriveTrainPower);
			wait1Msec(5);	// make sure this gets printed out

		}
		else
		{
			myDriveTrain.state = DriveTrainTurnLeftToTarget;	// turning left
			leftDriveTrainPower = -leftDriveTrainPower;	// reverse power to turn left
			rightDriveTrainPower = -rightDriveTrainPower;
			writeDebugStreamLine("Turning Left: %.1f degrees  leftPower: %d  rightPower: %d",targetDeg,leftDriveTrainPower,rightDriveTrainPower);
			wait1Msec(5);	// make sure this gets printed out
		}
		DRIVETRAINMoveLeftMotors(myDriveTrain,leftDriveTrainPower);	// get motors moving
		DRIVETRAINMoveRightMotors(myDriveTrain,rightDriveTrainPower);
	}
}

// executes all the motor tasks
void DRIVETRAINMotorTask(DRIVETRAIN &myDriveTrain) {
	MOTORTask(myDriveTrain.leftFrontMotor);
	MOTORTask(myDriveTrain.leftRearMotor);
	MOTORTask(myDriveTrain.rightFrontMotor);
	MOTORTask(myDriveTrain.rightRearMotor);
	UpdateDriveTrainEncoderPosition(myDriveTrain);
}

// updates drive train values
void DRIVETRAINTask(DRIVETRAIN &myDriveTrain, GYRO &myGyro) {
	DRIVETRAINMotorTask(myDriveTrain);	// update motors
	GYROTask(myGyro);	// update gyro for turning
	// update encoder status
	long distTraveled = myDriveTrain.actualEncoderValue - myDriveTrain.prevActualEncoderValue;	// we shouldn't be calculating this too often
	myDriveTrain.prevActualEncoderValue = myDriveTrain.actualEncoderValue;	// save previous value
	float gyroPos = UdpateDriveTrainGyroPosition(myDriveTrain,myGyro);
	// update move to target status
	if (DriveTrainForwardToTarget == myDriveTrain.state)
	{
		long diff =  myDriveTrain.targetEncoderValue - myDriveTrain.actualEncoderValue;
		if (diff <= 0)
		{
			DRIVETRAINStop(myDriveTrain);	// we reached the target
		}
		else if(abs(diff) < DTMoveToTargetSlowDownRange)
		{
			if (NoMotorSmoothing == myDriveTrain.drivingSmoothFactor)
			{
				DRIVETRAINSlow(myDriveTrain,DTMoveToTargetSlowPower);	// slow down when we get close if there is no smoothing (this preserves the direction
				//writeDebugStreamLine("DriveForward: we are getting close, slow down");
			}
		}
	}
	else if (DriveTrainReverseToTarget == myDriveTrain.state)
	{
		long diff =  myDriveTrain.actualEncoderValue - myDriveTrain.targetEncoderValue;
		if (diff <= 0)
		{
			DRIVETRAINStop(myDriveTrain);	// we reached the target
			//writeDebugStreamLine("DriveReverse:STOP diff = %d",diff);
		}
		else if(abs(diff) < DTMoveToTargetSlowDownRange)
		{
			if (NoMotorSmoothing == myDriveTrain.drivingSmoothFactor)
			{
				DRIVETRAINSlow(myDriveTrain,DTMoveToTargetSlowPower);	// slow down when we get close if there is no smoothing (this preserves the direction
				//writeDebugStreamLine("DriveReverse:SLOW target: %d actual: %d diff = %d",myDriveTrain.targetEncoderValue,myDriveTrain.actualEncoderValue,diff);
			}
		}
		else
		{
			//writeDebugStreamLine("DriveReverse: target: %d actual: %d diff = %d",myDriveTrain.targetEncoderValue,myDriveTrain.actualEncoderValue,diff);
		}
	}
	else if(DriveTrainTurnLeftToTarget == myDriveTrain.state)	// degrees are negative
	{
		float diff = myDriveTrain.targetDegrees - myDriveTrain.curDegrees;	// this will be positive if we go past
		writeDebugStreamLine("DriveTask> turning Left: target Deg: %.1f curDeg: %.1f  diff: %.1f",myDriveTrain.targetDegrees,myDriveTrain.curDegrees,diff);
		if (diff >= 0)
		{
			DRIVETRAINStop(myDriveTrain);	// we reached target
		}
		else if(abs(diff) < TurnToTargetSlowDownRange)
		{
			if (NoMotorSmoothing == myDriveTrain.drivingSmoothFactor)
			{
				DRIVETRAINSlow(myDriveTrain,MoveToTargetSlowPower);	// slow down when we get close if there is no smoothing
				writeDebugStreamLine("TurnLeft: we are getting close, slow down");
			}
		}
	}
	else if(DriveTrainTurnRightToTarget == myDriveTrain.state)	// degrees are postive
	{
		float diff = myDriveTrain.targetDegrees - myDriveTrain.curDegrees;	// this will be positive if we go past
		writeDebugStreamLine("DriveTask> turning right: target Deg: %.1f curDeg: %.1f  diff: %.1f",myDriveTrain.targetDegrees,myDriveTrain.curDegrees,diff);
		if (diff <= 0)
		{
			DRIVETRAINStop(myDriveTrain);	// we reached target
		}
		else if(abs(diff) < TurnToTargetSlowDownRange)
		{
			if (NoMotorSmoothing == myDriveTrain.drivingSmoothFactor)
			{
				DRIVETRAINSlow(myDriveTrain,MoveToTargetSlowPower);	// slow down when we get close if there is no smoothing
				//writeDebugStreamLine("TurnRight: we are getting close, slow down");
			}
		}
	}

	// calculate the actual power based on smoothing & drive speed and set the drive train power if necessary
	if (myDriveTrain.leftActualPower != myDriveTrain.leftTargetPower) {
		myDriveTrain.leftActualPower = ((myDriveTrain.leftActualPower * myDriveTrain.drivingSmoothFactor) + ((myDriveTrain.leftTargetPower)*myDriveTrain.driveSpeed*(100-myDriveTrain.drivingSmoothFactor)))/100;
		DRIVETRAINSetLeftMotorsPower(myDriveTrain,myDriveTrain.leftActualPower); // only set this here, don't do it every time MOTORTask is called, it is a waste of resources
	}
	if (myDriveTrain.rightActualPower != myDriveTrain.rightTargetPower) {
		myDriveTrain.rightActualPower = ((myDriveTrain.rightActualPower * myDriveTrain.drivingSmoothFactor) + ((myDriveTrain.rightTargetPower)*myDriveTrain.driveSpeed*(100-myDriveTrain.drivingSmoothFactor)))/100;
		DRIVETRAINSetRightMotorsPower(myDriveTrain,myDriveTrain.rightActualPower); // only set this here, don't do it every time MOTORTask is called, it is a waste of resources
	}

	// set the each side of the drivetrain to stopped if actualPower = 0
	if (0 == myDriveTrain.leftActualPower) {
		DRIVETRAINStopLeftMotors(myDriveTrain);	// this will set all our state information
	}
	if (0 == myDriveTrain.rightActualPower) {
		DRIVETRAINStopRightMotors(myDriveTrain);	// this will set all our state information
	}

	// see if the drivetrain is stalled
	//	//	DON'T TEST THIS UNTIL WE GET ANDY MARK MOTOR WHICH WON'T BURN OUT
	//if (IsMotorMoving(myMotor)) {
	//	if (0 == distTraveled) {
	//		// we need to make sure it is not moving for a while (a few seconds) before shutting it down
	//	}
	//}
}

void SetDriveTrainDriveSpeed(DRIVETRAIN &myDriveTrain, float driveSpeed) {
	myDriveTrain.driveSpeed = driveSpeed;
	SetMotorDriveSpeed(myDriveTrain.leftFrontMotor,driveSpeed);
	SetMotorDriveSpeed(myDriveTrain.leftRearMotor,driveSpeed);
	SetMotorDriveSpeed(myDriveTrain.rightFrontMotor,driveSpeed);
	SetMotorDriveSpeed(myDriveTrain.rightRearMotor,driveSpeed);
}

void SetDriveTrainSmoothFactor(DRIVETRAIN &myDriveTrain, int smoothFactor) {
	myDriveTrain.drivingSmoothFactor = smoothFactor;
	SetMotorDrivingSmoothFactor(myDriveTrain.leftFrontMotor,smoothFactor);
	SetMotorDrivingSmoothFactor(myDriveTrain.leftRearMotor,smoothFactor);
	SetMotorDrivingSmoothFactor(myDriveTrain.rightFrontMotor,smoothFactor);
	SetMotorDrivingSmoothFactor(myDriveTrain.rightRearMotor,smoothFactor);
}
/*
long GetDriveTrainPostion(DRIVETRAIN &myDriveTrain) {
return myDriveTrain.actualEncoderValue;
}
*/
// initializes the motor
void DRIVETRAINInit(DRIVETRAIN &myDriveTrain, GYRO &myGyro, int leftFrontMotorID,int leftReartMotorID,int rightFrontMotorID,
int rightReartMotorID, int drivingSmoothFactor, float driveSpeed, DriveTrainMode mode, int parkingBrakeID) {
	MOTORInit(myDriveTrain.leftFrontMotor,leftFrontMotorID,drivingSmoothFactor,driveSpeed);
	MOTORInit(myDriveTrain.leftRearMotor,leftReartMotorID,drivingSmoothFactor,driveSpeed);
	MOTORInit(myDriveTrain.rightFrontMotor,rightFrontMotorID,drivingSmoothFactor,driveSpeed);
	MOTORInit(myDriveTrain.rightRearMotor,rightReartMotorID,drivingSmoothFactor,driveSpeed);
	myDriveTrain.targetEncoderValue = 0;	// if the motor encoder target is zero, the motor will run forever
	UpdateDriveTrainEncoderPosition(myDriveTrain);
	myDriveTrain.prevActualEncoderValue = 0;
	myDriveTrain.prevTime = 0;
	myDriveTrain.drivingSmoothFactor = drivingSmoothFactor;
	SetDriveTrainDriveSpeed(myDriveTrain,driveSpeed);
	myDriveTrain.moveToStallTimeLimit = 0;
	SetDriveTrainMode(myDriveTrain,mode);
	myDriveTrain.parkingBrakeState = ParkingBrakeOff;	// start with parking brake off
	SERVOInit(myDriveTrain.parkingBrakeServo,parkingBrakeID,ParkingBrakeOffPos,true);
	DRIVETRAINTask(myDriveTrain,myGyro);
}

/////////////////////////////////////////////////////////
//
// Test DriveTrain
//


#endif	// _DRIVE_TRAIN_H
