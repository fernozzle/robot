#pragma config(Hubs,  S1, HTMotor,  HTMotor,  HTServo,  none)
#pragma config(Hubs,  S2, HTMotor,  HTMotor,  HTServo,  none)
#pragma config(Sensor, S1,     ,               sensorI2CMuxController)
#pragma config(Sensor, S2,     ,               sensorI2CMuxController)
#pragma config(Motor,  mtr_S1_C1_1,     leftFrontMotorID, tmotorTetrix, PIDControl, reversed, encoder)
#pragma config(Motor,  mtr_S1_C1_2,     leftRearMotorID, tmotorTetrix, PIDControl, reversed, encoder)
#pragma config(Motor,  mtr_S1_C2_1,     tubeReturnMotorID, tmotorTetrix, PIDControl, encoder)
#pragma config(Motor,  mtr_S1_C2_2,     ballGrabberArmMotorID, tmotorTetrix, PIDControl, encoder)
#pragma config(Motor,  mtr_S2_C1_1,     rightFrontMotorID, tmotorTetrix, PIDControl, encoder)
#pragma config(Motor,  mtr_S2_C1_2,     rightRearMotorID, tmotorTetrix, PIDControl, encoder)
#pragma config(Motor,  mtr_S2_C2_1,     liftMotorID,   tmotorTetrix, PIDControl, encoder)
#pragma config(Motor,  mtr_S2_C2_2,     NoMotor,       tmotorTetrix, openLoop)
#pragma config(Servo,  srvo_S1_C3_1,    leftGoalGrabberArmServoID, tServoStandard)
#pragma config(Servo,  srvo_S1_C3_2,    servo2,               tServoNone)
#pragma config(Servo,  srvo_S1_C3_3,    servo3,               tServoNone)
#pragma config(Servo,  srvo_S1_C3_4,    servo4,               tServoNone)
#pragma config(Servo,  srvo_S1_C3_5,    servo5,               tServoNone)
#pragma config(Servo,  srvo_S1_C3_6,    servo6,               tServoNone)
#pragma config(Servo,  srvo_S2_C3_1,    rightGoalGrabberArmServoID, tServoStandard)
#pragma config(Servo,  srvo_S2_C3_2,    liftBrakeServoID,     tServoStandard)
#pragma config(Servo,  srvo_S2_C3_3,    ballGrabberJawsServoID, tServoStandard)
#pragma config(Servo,  srvo_S2_C3_4,    tubeTipperServoID,    tServoStandard)
#pragma config(Servo,  srvo_S2_C3_5,    servo11,              tServoNone)
#pragma config(Servo,  srvo_S2_C3_6,    servo12,              tServoNone)
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//

#include "JoystickDriver.c"	 //Include file to "handle" the Bluetooth messages.
#include "Robot\standardServo.h"		// for standard servo

////////////////////////
//
//	Constants
//
const int LeftGoalGrabberArmOpen = 205;
const int LeftGoalGrabberArmClosed = 86;
const int LeftGoalGrabberArmPlow = 115;
const int RightGoalGrabberArmOpen = 0;
const int RightGoalGrabberArmClosed = 99;
const int RightGoalGrabberArmPlow = 77;
/////////////////////////////////////////////////////////////////////////////////////////////////////
//
//																		initializeRobot
//
// Prior to the start of tele-op mode, you may want to perform some initialization on your robot
// and the variables within your program.
//
// In most cases, you may not have to add any code to this function and it will remain "empty".
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

void initializeRobot()
{
	// Place code here to sinitialize servos to starting positions.
	// Sensors are automatically configured and setup by ROBOTC. They may need a brief time to stabilize.
	return;
}


task main(){

	STANDARD_SERVO leftGoalGrabberArmServo;
	STANDARD_SERVO rightGoalGrabberArmServo;

	initializeRobot();
//
// Test each servo on the robot
//
	int leftServoStartPos = LeftGoalGrabberArmOpen;
	int rightServoStartPos = RightGoalGrabberArmOpen;
	SERVOInit(leftGoalGrabberArmServo,leftGoalGrabberArmServoID,leftServoStartPos);
	SERVOInit(rightGoalGrabberArmServo,rightGoalGrabberArmServoID,rightServoStartPos);

	//waitForStart();		// wait for start of tele-op phase

	while (true){
    getJoystickSettings(joystick);

    if (joy1Btn(Btn8)) {
    	int newServoPos = SERVOGetPos(rightGoalGrabberArmServo) + 5;
    	SERVOSetPos(rightGoalGrabberArmServo,newServoPos);
    }
  	if(joy1Btn(Btn6)) {
    	int newServoPos = SERVOGetPos(rightGoalGrabberArmServo) - 5;
    	SERVOSetPos(rightGoalGrabberArmServo,newServoPos);
    }
    if (joy1Btn(Btn7)) {
    	int newServoPos = SERVOGetPos(leftGoalGrabberArmServo) - 5;
    	SERVOSetPos(leftGoalGrabberArmServo,newServoPos);
  	}
  	if(joy1Btn(Btn5)) {
    	int newServoPos = SERVOGetPos(leftGoalGrabberArmServo) + 5;
    	SERVOSetPos(leftGoalGrabberArmServo,newServoPos);
    }
  	if(joy1Btn(Btn1)) {	// Open
    	SERVOSetPos(leftGoalGrabberArmServo,LeftGoalGrabberArmOpen);
    	SERVOSetPos(rightGoalGrabberArmServo,RightGoalGrabberArmOpen);
    }
  	if(joy1Btn(Btn3)) {	// Plow
    	SERVOSetPos(leftGoalGrabberArmServo,LeftGoalGrabberArmPlow);
    	SERVOSetPos(rightGoalGrabberArmServo,RightGoalGrabberArmPlow);
    }



   	SERVOTask(leftGoalGrabberArmServo);
   	SERVOTask(rightGoalGrabberArmServo);
		/*
		// Deploy and stow spinner with top hat
		if (4 == joystick.joy1_TopHat)
		{
			servo[leftSpinServo] = spinServoStartPos; // stow
			servo[rightSpinServo] = spinServoStartPos;
		}
		if (0 == joystick.joy1_TopHat)
		{
			servo[leftSpinServo]	= leftSpinServoDeployPos; // deploy
			servo[rightSpinServo] = rightSpinServoDeployPos;
		}
		*/
		writeDebugStreamLine("LeftGoalGrabberArmPos: %d   RightGoalGrabberArmPos: %d",SERVOGetPos(leftGoalGrabberArmServo),
												 SERVOGetPos(rightGoalGrabberArmServo));
		wait1Msec(100);
	}
}
