//	syncFunctions.h
//	sychronous driving and turning with timeout

bool driveSync(ROBOT &myRobot, SENSORS &mySensors, int power, long distance, long maxTime) {
	int startTime = nPgmTime;	// timer to limit total time
	ResetDriveTrainEncoderPosition(myRobot.rDriveTrain);	// reset encoder position
	DRIVETRAINMoveToTarget(myRobot.rDriveTrain,power,distance);
	while(IsDriveTrainMoving(myRobot.rDriveTrain))
	{
		DRIVETRAINTask(myRobot.rDriveTrain,mySensors.rsGyro);
		GOALGRABBERTask(myRobot.rGoalGrabber);
		LIFTTask(myRobot.rLift);
		LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);
		BALLSCORERTask(myRobot.rBallScorer,curLiftState);
		if(nPgmTime >= startTime+maxTime)
		{
			return false;	// we took too long
		}
	}
	return true;	// ok
}
//	Drive until <= an ultrasonic distance or >= a max drive distance. Return final ultrasonic value
bool DriveToUSValue(ROBOT &myRobot, SENSORS &mySensors, int power, int targetUSValue, long maxDist, long maxTime, int &finalUSValue) {
	int startTime = nPgmTime;	// timer to limit total time
	finalUSValue = -1;	// default to no final value
	ResetDriveTrainEncoderPosition(myRobot.rDriveTrain);	// reset encoder position
	long driveTrainDist = 0;
	long DriveTrainMaxTicks = maxDist * TetrixTicksPerInch4InchWheel;
	DRIVETRAINMove(myRobot.rDriveTrain,power);

	SENSORS_ULTRASONICTask(mySensors);	// get an updated distance
	int curUltraDist = ReadUltrasonic(mySensors.rsUltra);
	writeDebugStreamLine("Initial US value: %i", curUltraDist);

	bool done = false;
	while(!done)
	{
		SENSORS_ULTRASONICTask(mySensors);	// get an updated distance
		DRIVETRAINMotorTask(myRobot.rDriveTrain);
		LIFTTask(myRobot.rLift);

		curUltraDist = ReadUltrasonic(mySensors.rsUltra);
		driveTrainDist = GetDriveTrainEncoderValue(myRobot.rDriveTrain);
		writeDebugStreamLine("DriveToUSValue: dist(cm): %d targ: %d > drive dist(ticks): %d  elapsed: %d",curUltraDist,targetUSValue,driveTrainDist,nPgmTime-startTime);
		// check if we've passed our destination
		if (curUltraDist <= targetUSValue)
			{
			done = true;
			finalUSValue = curUltraDist;
		}
		if (driveTrainDist >= DriveTrainMaxTicks)
		{
			done = true;	// maximum distance, return now
		}
		if (nPgmTime >= startTime+maxTime)
		{
			return false;	// we took too long
		}
	}
	return true;	// ok
}

bool driveToIRValueSync(ROBOT &myRobot, SENSORS &mySensors, int power, int desiredIRValue, long maxTime, bool stopWhenDone=true) {
	int startTime = nPgmTime;	// timer to limit total time
	ResetDriveTrainEncoderPosition(myRobot.rDriveTrain);	// reset encoder position
	DRIVETRAINMove(myRobot.rDriveTrain,power);

	int initialIRValue = IRSeekerGetACDir(mySensors.rsIRSeeker);
	writeDebugStreamLine("Initial IR value: %i", initialIRValue);

	bool done = false;
	while(!done)
	{
		SENSORS_IRSeekerTask(mySensors);
		DRIVETRAINMotorTask(myRobot.rDriveTrain);
		//GOALGRABBERTask(myRobot.rGoalGrabber);	// goal grabber should be stowed
		LIFTTask(myRobot.rLift);
		LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);
		BALLSCORERTask(myRobot.rBallScorer,curLiftState);

		int curIRValue = IRSeekerGetACDir(mySensors.rsIRSeeker);
		writeDebugStreamLine("Current IR value: %i\tTarget IR value: %i", curIRValue, desiredIRValue);
		// check if we've passed our destination
		if (initialIRValue > desiredIRValue) {
			if (curIRValue <= desiredIRValue)
				done = true;
			} else {
			if (curIRValue >= desiredIRValue)
				done = true;
		}
		if (nPgmTime >= startTime+maxTime)
		{
			return false;	// we took too long
		}
	}
	if (stopWhenDone) {
		DRIVETRAINStop(myRobot.rDriveTrain);
	}
	return true;	// ok
}

// Turns in place
bool turnSync(ROBOT &myRobot, SENSORS &mySensors, int power, float degrees, long maxTime) {
	int startTime = nPgmTime;	// timer to limit total time
	DRIVETRAINTurnToTarget(myRobot.rDriveTrain,mySensors.rsGyro,power,degrees);
	while(IsDriveTrainMoving(myRobot.rDriveTrain))
	{
		DRIVETRAINTask(myRobot.rDriveTrain,mySensors.rsGyro);
		GOALGRABBERTask(myRobot.rGoalGrabber);
		LIFTTask(myRobot.rLift);
		LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);
		BALLSCORERTask(myRobot.rBallScorer,curLiftState);
		if(nPgmTime >= startTime + maxTime)
		{
			writeDebugStreamLine("turnSync> took too long");
			return false;	// we took too long
		}
	}
	return true;
}

// Turns while moving forward (instead of turning in place): applies different power to each motor
bool slalomTurnSync(ROBOT &myRobot, SENSORS &mySensors, int powerLeft, int powerRight, float targetDegrees, long maxTime, bool stopWhenDone=true) {
	// timer to limit total time
	int startTime = nPgmTime;
	GYROReset(mySensors.rsGyro);

	// get motors moving
	DRIVETRAINMoveLeftMotors (myRobot.rDriveTrain,powerLeft );
	DRIVETRAINMoveRightMotors(myRobot.rDriveTrain,powerRight);

	bool done = false;
	while(!done)
	{
		SENSORS_GYROTask(mySensors);
		DRIVETRAINMotorTask(myRobot.rDriveTrain);
		GOALGRABBERTask(myRobot.rGoalGrabber);
		LIFTTask(myRobot.rLift);
		LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);
		BALLSCORERTask(myRobot.rBallScorer,curLiftState);
		if(nPgmTime >= startTime + maxTime)
		{
			return false;	// we took too long
		}

		float curDegrees = GyroGetHeading(mySensors.rsGyro);
		if ((powerLeft > powerRight) == (GetDriveTrainMode(myRobot.rDriveTrain) == DriveTrainModeGoalGrabbing)) {
			// turning right
			writeDebugStreamLine("TURNING CLOCKWISE - Current gyro: %.2f \tTarget gyro: %.2f", curDegrees, targetDegrees);
			if (curDegrees >= targetDegrees) {
				done = true;
			}
		} else {
			// turning left
			writeDebugStreamLine("TURNING COUNTERCLOCKWISE  - Current gyro: %.2f \tTarget gyro: %.2f", curDegrees, targetDegrees);
			if (curDegrees <= targetDegrees) {
				done = true;
			}
		}
	}

	// shut off motors
	if (stopWhenDone) {
		DRIVETRAINStop(myRobot.rDriveTrain);
	}

	return true;
}

/*
// This is common
bool Lift90cmAndPlow(ROBOT &myRobot, SENSORS &mySensors, int MaxTime) {	// move lift to 90cm height and put GG in plow position so we are ready to score into that
//writeDebugStreamLine("Lift90cm and Plow");
GOALGRABBERSetState(myRobot.rGoalGrabber,GoalGrabberPlow);

MoveLift(myRobot.rLift,LiftPos90cm);	// we might as well move the lift to the 90cm position while we are at it
int startTime = nPgmTime;	// restart timer
bool bTimer = true;	// timer hasn't expired yet
while (IsLiftMoving(myRobot.rLift) && bTimer){
GOALGRABBERTask(myRobot.rGoalGrabber);	// need this to complete plow position
LIFTTask(myRobot.rLift);
if(nPgmTime >= startTime+MaxTime)
{
bTimer = false;	// we took too long
}
}
if (!bTimer)	// timer expired, quit and return error
{
LIFTStop(myRobot.rLift);	// stop the lift
//writeDebugStreamLine("Lift90cm> time expired. Stopping lift");
return false;	// error
}
//writeDebugStreamLine("Lift @ 90cm");
return true;	// everything ok if we got this far
}
*/
