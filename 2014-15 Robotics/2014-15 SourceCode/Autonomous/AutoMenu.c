#include "autoMenuFunctions.h"

typedef struct {
	bool
} MENU_CHOICES;

void AutoMenu()		// prompts the user to select autonmous strategy
{
	eraseDisplay();	// erase the display
	//	Common questions
	myAuto.ourTeam = PromptTeam();
	myAuto.autoStartDelay = PromptDelay();
	// Initialize strategy array
	myAuto.curStep = 0;
	for (int stratInx = 0; stratInx < MaxNbrStrat; stratInx++)
	{
		myAuto.step[stratInx] = AUTO_NONE;
	}
	//
	//	Ramp or Parking Area?
	//
	MENU rampParkMenu;
	MENUInit(rampParkMenu,"Park or Ramp?");
	bool bOK = MENUAddChoice(rampParkMenu,"Parking Area",(int) STARTPOS_PARK);	// Parking Area is first choice
	bOK = MENUAddChoice(rampParkMenu,"Ramp",(int) STARTPOS_RAMP);					// Ramp is 2nd choice
	myAuto.startPos = (STARTPOS) MENUGetChoice(rampParkMenu);
	//
	//	Rest of the prompts depend on where we are starting
	//
	if (STARTPOS_RAMP == myAuto.startPos)
	{	// starting on the ramp
		AutoStartOnRamp(myAuto);
	}
	else
	{	// starting in the parking area
		AutoStarParkingZone(myAuto);
	}
	// set up to run 1st strategy
	myAuto.curStep = 0;
	//	erase the display when we are done
	eraseDisplay();
}
