// AutoParkingsZone.h
//	Autonomous actions for this year's challenge
// Parking Zone

#include "syncFunctions.h"

#define EOPD_DISTANCE_STOP 0.95	// EOPD distance at which we should stop so we can score
#define MAX_ALLOWED_TIME 15000  // Maximum allowed time to do any operation in milliseconds
#define IR_VALUE_90DEG_RIGHT 9  // IR value when the beacon is 90deg to the right of the robot -> go past narrow 8 sector to make sure we are far enough
#define DISTANCE_BACKWARD_FROM_CENTER_GOAL -12	// distance to retreat from center goal before going after kickstand
#define START_DRIVE_POWER	MaximumMotorPowerTetrix * 0.80   // start towards goal at reduced power
#define IRSEARCH_DRIVE_POWER	MaximumMotorPowerTetrix * 0.70   // look for IR towards goal at reduced power
#define SLALOM_INSIDE_POWER MaximumMotorPowerTetrix / 2	// inside (slower) speed for slalom turn
#define SLALOM_OUTSIDE_POWER SLALOM_INSIDE_POWER * 2   	// outside (faster) speed for slalom turn

bool g_bORIENTATION_ONE = false;	// true if we are in orientation 1
//
//	Knockout kickstand - this assumes that Orientation 1, 2, or 3 was sucessful and we retreated 12 inches from
//			scoring position on the center goal
//	Use light sensor to look for yellow of center goal
//
bool KnockoutKickstand(ROBOT &myRobot, SENSORS &mySensors) {	// Knocks over kickstand

	//writeDebugStreamLine("Lowering Lift to 60cm");
	MoveLift(myRobot.rLift,LiftPos60cm);	// the wood on the lift will hit the kickstand at 60cm, not at 90cm

	writeDebugStreamLine("Knock out kickstand");
	//	Turn right 45 degrees
	bool bOK = turnSync(myRobot,mySensors,START_DRIVE_POWER,45.0,MAX_ALLOWED_TIME);
	if (!bOK) {
		return false;	// problem, quit
	}


	// drive forward 18"
	long distTarget = 18 * TetrixTicksPerInch4InchWheel;
	//writeDebugStreamLine("Fwd: %d",distTarget);
	bOK = driveSync(myRobot,mySensors,START_DRIVE_POWER,distTarget,MAX_ALLOWED_TIME);
	if (!bOK) {
		return false;	// problem, quit
	}

	//	Turn left 45 degrees
	bOK = turnSync(myRobot,mySensors,START_DRIVE_POWER,-45.0,MAX_ALLOWED_TIME);
	if (!bOK) {
		return false;	// problem, quit
	}

	// drive foward  for 30"
	writeDebugStreamLine("Forward to kickstand");
	distTarget = 30 * TetrixTicksPerInch4InchWheel;
	bOK = driveSync(myRobot,mySensors,MaximumMotorPowerTetrix,distTarget,MAX_ALLOWED_TIME);
	if (!bOK) {
		return false;	// problem, quit
	}

	// back up and hit kickstatnd one more time
#define ExtraKickStandHits 1
	for(int i = 0; i < ExtraKickStandHits; i++){
		distTarget = -16 * TetrixTicksPerInch4InchWheel;
		bOK = driveSync(myRobot,mySensors,MaximumMotorPowerTetrix,distTarget,MAX_ALLOWED_TIME);
		if (!bOK) {
			return false;	// problem, quit
		}
		distTarget = 24 * TetrixTicksPerInch4InchWheel;
		bOK = driveSync(myRobot,mySensors,MaximumMotorPowerTetrix,distTarget,MAX_ALLOWED_TIME);
		if (!bOK) {
			return false;	// problem, quit
		}
	}
	return true;	// everything okay if we got this far
}

bool scoreBallAndRetreat(ROBOT &myRobot, SENSORS &mySensors) {
	LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);
	GetBallScorerTubeToGoal(myRobot.rBallScorer,curLiftState);
	bool isBallScored = false;
	int startTime = nPgmTime;	// restart timer
	while(!isBallScored){
		BALLSCORERTask(myRobot.rBallScorer,curLiftState);
		isBallScored = (BallScorerTubeToGoal == GetBallScorerState(myRobot.rBallScorer));
		if(nPgmTime >= startTime+MAX_ALLOWED_TIME)
		{
			writeDebugStreamLine("scoreBallAndRetreat: took too long");
			return false;
		}
	}
	//	Stow Tube after scoring
	//writeDebugStreamLine("waiting for balls to score");
	wait1Msec(BALL_SCORING_TIME);	// give balls time to roll in
	// Balls are in, retreat at full speed & stow tube
	//writeDebugStreamLine("Scoring Done. Retreat & stow tube");

	BallScorerStowTube(myRobot.rBallScorer,curLiftState);
	ResetDriveTrainEncoderPosition(myRobot.rDriveTrain);
	DRIVETRAINMoveToTarget(myRobot.rDriveTrain, MaximumMotorPowerTetrix, DISTANCE_BACKWARD_FROM_CENTER_GOAL * TetrixTicksPerInch4InchWheel);
	// Keep going until tube is stowed and we are done moving backwards
	startTime = nPgmTime;	// restart timer
	while ((IsDriveTrainMoving(myRobot.rDriveTrain) || !IsBallScorerReadyToScore(myRobot.rBallScorer,false))){	// jaws state not important
		BALLSCORERTask(myRobot.rBallScorer,curLiftState);
		DRIVETRAINTask(myRobot.rDriveTrain,mySensors.rsGyro);
		if(nPgmTime >= startTime+MAX_ALLOWED_TIME)
		{
			return false;
		}
	}
	// we don't want to hang out with lift @ 120cm, so move it down to 90cm
	//writeDebugStreamLine("Lowering Lift to 90cm");
	//MoveLift(myRobot.rLift,LiftPos90cm);
	//	LIFTTask(myRobot.rLift);
	//	wait1Msec(100);
	//	MoveLift(myRobot.rLift,LiftPos90cm);
	//	LIFTTask(myRobot.rLift);
	/*
	while (IsLiftMoving(myRobot.rLift)) {
	LIFTTask(myRobot.rLift);
	}*/
	return true;
}
// Moves forward using the ultrasonic to know when to slow down and the EOPD to stop.
// Also raises lift to 120cm and deploys ball grabber.
//
//          TICKS_DEPLOYBALLGRABBER
//          |                ULTRA_DISTANCE_SLOW
//          |                |                EOPD_DISTANCE_STOP
//          |                |          J     |          J  .|   |.
//    ___   |       ___J     |       ___|     |       ___|  L|___|J
//   |   |  |      |   | ))  |      |   | ==  |      |   |   |   |
//   |___| -->     |___| )) -->     |___| == -->     |___|   |.-.|
//    O O   |  [V_]/O O      |  [V_]/O O      |  [V_]/O O  __|___|__
// `````````````````````````````````````````````````````
bool approachCenterGoalUsingUltraAndEOPD(ROBOT &myRobot, SENSORS &mySensors) {
	//Constants
#define DISTANCE_FORWARD_TO_CENTER_GOAL		38	// we should stop before this
#define TICKS_FORWARD_TO_CENTER_GOAL	 		DISTANCE_FORWARD_TO_CENTER_GOAL * TetrixTicksPerInch4InchWheel
#define TICKS_DEPLOYBALLGRABBER						TICKS_FORWARD_TO_CENTER_GOAL / 2
#define APPROACH_GOAL_POWER 							MaximumMotorPowerTetrix * 0.20	// approach goal at reduced power
#define ULTRA_DISTANCE_SLOW 							24	// utlrasonic distance (in cm) at which we should slow down
#define EOPD_DIST_CHECK										3.0	// if EOPD distance is more than this, then turn another 5 degrees

	// move foward at high speed until Ultrasonic <= ULTRA_DISTANCE_SLOW cm
	LIFTSTATE curLiftState = LiftStowed;	// lift state
	int  startTime = nPgmTime;	// timer to limit total time
	bool isBallGrabberDeployed = false;

STARTULTRADRIVE:
	SENSORS_ULTRASONICTask(mySensors);	// get an updated distance
	int  ultraDist = ReadUltrasonic(mySensors.rsUltra);
	// Make sure the ultrasonic distance is reasonable (< 150) otherwise we aren't lined up on the center goal
	//	we only have to check this if it isn't orientation 1
#define USDRIVE_MAXDIST 150		// maximum distance from center goal we should try and use ultrasonic distance
	if (!g_bORIENTATION_ONE && (ultraDist > USDRIVE_MAXDIST))
	{
		//	Turn 5 degrees right to see if we can find it
		DRIVETRAINStop(myRobot.rDriveTrain);	// we need to turn some more
		writeDebugStreamLine("USdist: %d TOO HIGH, turning right",ultraDist);
		bool bOK = turnSync(myRobot, mySensors, START_DRIVE_POWER, 5, MAX_ALLOWED_TIME);	// turn 5 more degrees
		if (!bOK)
		{
			DRIVETRAINStop(myRobot.rDriveTrain);	// stop drive train
			return false;	// something went wrong. stop and quit
		}
		writeDebugStreamLine("USdist> restart UltraDrive");
		goto STARTULTRADRIVE;	// go back to Ultra drive in case we are very far away. We aren't resetting overall timer, so that will stop us if it takes too long
	}
	ResetDriveTrainEncoderPosition(myRobot.rDriveTrain);
	DRIVETRAINMove(myRobot.rDriveTrain,START_DRIVE_POWER);	// start moving
	long driveTrainDist = GetDriveTrainEncoderValue(myRobot.rDriveTrain);

	bool bUltraDist				= true;	// true while ultra distance > ULTRA_DISTANCE_SLOW
	bool bDriveTrainDist	= true;	// true while driveTrainDist < TICKS_FORWARD_TO_CENTER_GOAL
	bool bTimer						= true;	// true while       nPgmTime < startTime+MAX_ALLOWED_TIME
	while(bUltraDist && bDriveTrainDist && bTimer)
	{
		DRIVETRAINMotorTask(myRobot.rDriveTrain);
		LIFTTask(myRobot.rLift);
		curLiftState = GetCurLiftState(myRobot.rLift);
		BALLSCORERTask(myRobot.rBallScorer,curLiftState);
		SENSORS_ULTRASONICTask(mySensors);	// get an updated distance
		SENSORS_EOPDForwardTask(mySensors);	// we are going to need the EOPD soon so get that too
		ultraDist = ReadUltrasonic(mySensors.rsUltra);
		driveTrainDist = GetDriveTrainEncoderValue(myRobot.rDriveTrain);
		writeDebugStreamLine("UltraDrive: dist(cm): %d  drivetrain dist(ticks): %d  elapsed: %d",ultraDist,driveTrainDist,nPgmTime-startTime);
		if (ultraDist <= ULTRA_DISTANCE_SLOW)
		{
			bUltraDist = false;	// we are close
		}
		else if (ultraDist >= USDRIVE_MAXDIST) // Robot misaligned
		{
			DRIVETRAINStop(myRobot.rDriveTrain);
			MoveLift(myRobot.rLift,LiftPos90cm);
			return false;
		}
		else if(driveTrainDist >= TICKS_FORWARD_TO_CENTER_GOAL)
		{
			//writeDebugStreamLine("UltraDrive: went too far");
			bDriveTrainDist = false;	// we went too far, stop
		}
		else if(nPgmTime >= startTime+MAX_ALLOWED_TIME)
		{
			//writeDebugStreamLine("UltraDrive: took too long");
			bTimer = false;	// we took too long
		}
		// Deploy ball grabber if drive train has traveled far enough
		//writeDebugStreamLine("BallGrabberDeploy Target: %d  current %d",TICKS_DEPLOYBALLGRABBER,driveTrainDist);
		if (!isBallGrabberDeployed && (driveTrainDist >= TICKS_DEPLOYBALLGRABBER))
		{
			isBallGrabberDeployed = true;
			BallScorerDeployBallGrabber(myRobot.rBallScorer,BallGrabberJawsClosed);
		}
	}
	//
	// If bUltraDist = false, then it is time to switch to EOPD to finish up, otherwise something went wrong, so quit and return false
	//
	if ((!bDriveTrainDist) || (!bTimer))	// NOTE: If UltraDist = true and either of the others are false, then there is a bug
	{
		DRIVETRAINStop(myRobot.rDriveTrain);	// stop drive train
		LIFTStop(myRobot.rLift);	// stop lift
		return false;	// something went wrong. stop and quit
	}

	// before we switch to EOPD, see if it has a reasonable value. If it is too high, it means we haven't turned far enough
	//	only try this if not orientation 1
	/*
	if (!g_bORIENTATION_ONE)
	{
	float eopdDist = EOPDGetDist(mySensors.rsEOPDForward);	// get EOPD distance
	if (eopdDist >= EOPD_DIST_CHECK)
	{
	DRIVETRAINStop(myRobot.rDriveTrain);	// we need to turn some more
	writeDebugStreamLine("EOPD distance: %.2f TOO HIGH, turning some more",eopdDist);
	bool bOK = turnSync(myRobot, mySensors, START_DRIVE_POWER, 5, MAX_ALLOWED_TIME);	// turn 5 more degrees
	if (!bOK)
	{
	DRIVETRAINStop(myRobot.rDriveTrain);	// stop drive train
	return false;	// something went wrong. stop and quit
	}
	writeDebugStreamLine("Restarting UltraDrive");
	goto STARTULTRADRIVE;	// go back to Ultra drive in case we are very far away. We aren't resetting overall timer, so that will stop us if it takes too long
	}
	else
	{
	writeDebugStreamLine("EOPD starting distance: %.2f okay, proceed",eopdDist);
	}
	}
	*/

	// Switch to EOPD.
	// Lift & ball scorer might still be moving, so we need to execute their tasks so they can stop properly
	// And we still want to make sure we don't go more than the maximum distance (bDriveTrainDist) & exceed timer (bTimer)
	//writeDebugStreamLine("approachCenterGoalUsingUltraAndEOPD: switching to EOPD");
	bool bEOPDDist = true;
	bool bLiftMoving = IsLiftMoving(myRobot.rLift);
	DRIVETRAINMove(myRobot.rDriveTrain, APPROACH_GOAL_POWER);	// slow down to approach speed
	while(bEOPDDist && bDriveTrainDist && bTimer)	// if lift is done moving, we still have to use EOPD to get to proper distance
	{
		DRIVETRAINMotorTask(myRobot.rDriveTrain);
		LIFTTask(myRobot.rLift);
		curLiftState = GetCurLiftState(myRobot.rLift);
		bLiftMoving = IsLiftMoving(myRobot.rLift);
		BALLSCORERTask(myRobot.rBallScorer, curLiftState);
		SENSORS_EOPDForwardTask(mySensors);	// we are going to need the EOPD soon so get that too
		float eopdDist = EOPDGetDist(mySensors.rsEOPDForward);	// get EOPD distance
		driveTrainDist = GetDriveTrainEncoderValue(myRobot.rDriveTrain);
		writeDebugStreamLine("EOPDDrive: dist: %.2f  drivetrain dist(ticks): %d  timer: %d",eopdDist,driveTrainDist,nPgmTime);

		if (eopdDist <= EOPD_DISTANCE_STOP)
		{
			DRIVETRAINStop(myRobot.rDriveTrain);
			if (bLiftMoving)
			{
				writeDebugStreamLine("Done Driving. keep going until the lift is done");  // we are there, but we need to keep going until the lift is done.
			}
			else
			{
				writeDebugStreamLine("Done Driving. Lift is stopped, we are done");
				break;	// exit loop
			}
		}
		else if(driveTrainDist >= TICKS_FORWARD_TO_CENTER_GOAL)
		{
			writeDebugStreamLine("EOPDDrive: went too far");
			bDriveTrainDist = false;	// we went too far, stop
		}
		else if(nPgmTime >= startTime+MAX_ALLOWED_TIME)
		{
			writeDebugStreamLine("EOPDDrive: took too long");
			bTimer = false;	// we took too long
		}
	}
	//
	//	If bEOPDDist = false, then it is time to score, otherwise something went wrong, so quit and return false
	//
	if ((!bDriveTrainDist) || (!bTimer))
	{
		DRIVETRAINStop(myRobot.rDriveTrain);
		LIFTStop(myRobot.rLift);	// stop lift
		writeDebugStreamLine("Orientation1> Problem after drive train stop");
		return false;	// something went wrong. stop and quit
	}
	DRIVETRAINStop(myRobot.rDriveTrain);	// no matter what, we want the drive train stopped when we exit
	return true;	// everything ok if we got this far
}
//
//	Orienation 1
//
bool Orientation1(ROBOT &myRobot, SENSORS &mySensors) {	// handles orientation 1 of center goal
	bool bOK = true;
	g_bORIENTATION_ONE = true;

	bOK = approachCenterGoalUsingUltraAndEOPD(myRobot, mySensors);
	if (!bOK) return false;

	return true;
}

bool Orientation2(ROBOT &myRobot, SENSORS &mySensors){
	// We are away from the wall, so deploy the ball grabber
	BallScorerDeployBallGrabber(myRobot.rBallScorer,BallGrabberJawsClosed);
	//writeDebugStreamLine("Orientation 2: ball grabber deployed");

	bool bOK = true;

	// Slalom turn left 45 degrees

	bOK = slalomTurnSync(myRobot, mySensors,SLALOM_INSIDE_POWER , SLALOM_OUTSIDE_POWER, -45, MAX_ALLOWED_TIME);
	if (!bOK)
	{
		return false;
	}
	//writeDebugStreamLine("Orientation 2: slalom turn completed");

	// Drive till we're next to IR beacon
	bOK = driveToIRValueSync(myRobot, mySensors, IRSEARCH_DRIVE_POWER, IR_VALUE_90DEG_RIGHT, MAX_ALLOWED_TIME);
	if (!bOK) return false;
	//writeDebugStreamLine("Orientation 2: IR beacon at 90 degrees to the right");

	// Turn right 90 degrees to face center goal
#define TURN_TO_GOAL_OR2 90	// turn 93 degrees to the goal for orientation 2
	bOK = turnSync(myRobot, mySensors, START_DRIVE_POWER, TURN_TO_GOAL_OR2, MAX_ALLOWED_TIME);
	if(!bOK)
	{
		return false;
	}
	//writeDebugStreamLine("Orientation 2: finished turning right 90 degrees");

	bOK = approachCenterGoalUsingUltraAndEOPD(myRobot, mySensors);
	if (!bOK)
	{
		return false;
	}

	return true;	// everything ok if we got this far
}

bool Orientation3(ROBOT &myRobot, SENSORS &mySensors){
#define O3_SLALOM_TURN_ANGLE 55
	bool bOK = true;
	if(!bOK) return false;
	bOK = slalomTurnSync(myRobot,mySensors,SLALOM_INSIDE_POWER,SLALOM_OUTSIDE_POWER,-O3_SLALOM_TURN_ANGLE,MAX_ALLOWED_TIME); // (B)
	if(!bOK) return false;
	//writeDebugStreamLine("Orientation 3: slalom left turn completed");

	// We are away from the wall, so deploy the ball grabber
	BallScorerDeployBallGrabber(myRobot.rBallScorer,BallGrabberJawsClosed);
	//writeDebugStreamLine("Orientation 2: ball grabber deployed");

#define O3_STRAIGHTDRIVE_DIST 9
#define O3_STRAIGHTDRIVE_POWER 100	// drive fast
	bOK = driveSync(myRobot, mySensors,O3_STRAIGHTDRIVE_POWER,O3_STRAIGHTDRIVE_DIST * TetrixTicksPerInch4InchWheel,MAX_ALLOWED_TIME);
	if(!bOK) return false;
	writeDebugStreamLine("Orientation 3: drive straight completed");

	bOK = slalomTurnSync(myRobot,mySensors,SLALOM_OUTSIDE_POWER,SLALOM_INSIDE_POWER,O3_SLALOM_TURN_ANGLE,MAX_ALLOWED_TIME); // (D)
	if(!bOK) return false;
	writeDebugStreamLine("Orientation 3: slalom right turn completed");

#define IR_VALUE_90DEG_RIGHT_O3 9
	bOK = driveToIRValueSync(myRobot, mySensors,IRSEARCH_DRIVE_POWER,IR_VALUE_90DEG_RIGHT_O3,MAX_ALLOWED_TIME);
	if(!bOK) return false;
#define TURN_TO_GOAL_OR3 90	// turn 93 degrees to the goal for orientation 3
	bOK = turnSync(myRobot,mySensors,START_DRIVE_POWER,TURN_TO_GOAL_OR3,MAX_ALLOWED_TIME); // (D)
	if(!bOK) return false;

	bOK = approachCenterGoalUsingUltraAndEOPD(myRobot, mySensors);
	if(!bOK) return false;

	return true;	// everything ok if we got this far
}

bool ParkingZoneToCenterGoal(ROBOT &myRobot, SENSORS &mySensors){
	bool bOK = false;		// true if everything is okay

	// NOTE: We want goal grabber to be closed which is the initial position, so don't do anything with it
	//	The distance to the center goal from the starting position in the middle of the parking zone
	//	lets us know exactly which orientation the center goal is in
	MoveLift(myRobot.rLift,LiftPos120cm);	// start lift moving no matter what
	SENSORS_ULTRASONICTask(mySensors);

	int distance = ReadUltrasonic(mySensors.rsUltra);
	if(distance >= 90 && distance <= 115)
	{ //Orientation 1
		writeDebugStreamLine("D: %d, O: 1", distance);
		bOK = Orientation1(myRobot,mySensors);
		} else {
		// Drive forward up to 1 foot and check Ultrasonic for value
#define DriveToUSPower 50				// drive at 50% power so we don't go too far
#define DriveToUSTargetVal 150	// if Ultra gets below this then it must be orientation 3
#define DriveToUSMaxDist 12			// don't go more than 12 inches
		int finalUsValue = -1;
		bOK = DriveToUSValue(myRobot,mySensors,DriveToUSPower,DriveToUSTargetVal,DriveToUSMaxDist,MAX_ALLOWED_TIME, finalUSValue);
		if (!bOK)
		{
			goto RETURN;	// problem, stop
		}

		if (-1 == finalUSValue) {				// orientation 2
			writeDebugStreamLine("D: %d, O: 2", finalUSValue);
			bOK = Orientation2(myRobot,mySensors);
		}
		else
		{									// orientation 3
			writeDebugStreamLine("D: %d, O: 3", finalUsValue);
			bOK = Orientation3(myRobot,mySensors);
		}
	}
	//	Score!
	if(bOK)
	{
		bOK = scoreBallAndRetreat(myRobot,mySensors);
	}
RETURN:
	return bOK;
}

bool CenterGoalToKickstand(ROBOT &myRobot, SENSORS &mySensors){	// after scoring in center goal, knock over kickstand
	bool bOK = false;		// true if everything is okay
	writeDebugStreamLine("Center goal to Kickstand");
	bOK = KnockoutKickstand(myRobot,mySensors);
	return bOK;
}

bool KickstandToDefOppCenterGoal(ROBOT &myRobot, SENSORS &mySensors){	// after knocking over kickstand, defend opposing team center goal
	bool bOK = false;		// true if everything is okay
	//writeDebugStreamLine("Kickstand to Defend Opposing Team Center Goal");

	return bOK;
}
