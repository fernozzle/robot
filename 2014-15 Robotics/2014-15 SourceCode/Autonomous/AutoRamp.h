// AutoRamp.h
//	Autonomous actions for this year's challenge

#include "syncFunctions.h"

//
// Constants
#define BALL_SCORING_TIME_RAMP 3000
#define BIG_BALL_SCORING_TIME_RAMP 500
#define START_DRIVE_POWER_RAMP MaximumMotorPowerTetrix * 0.70	// start towards goal at reduced power

#define MAX_ALLOWED_TIME_RAMP 15000  // Maximum allowed time to do any operation in milliseconds
#define SLALOM_INSIDE_POWER_RAMP MaximumMotorPowerTetrix / 2	// inside (slower) speed for slalom turn
#define SLALOM_OUTSIDE_POWER_RAMP SLALOM_INSIDE_POWER_RAMP * 2   	// outside (faster) speed for slalom turn
//
// Drive Off Ramp
//
bool DriveOffRamp(ROBOT &myRobot, SENSORS &mySensors)	// drives off the ramp. Returns true if successful
{
	//
	// Variables
	//
	long distTarget = 56 * TetrixTicksPerInch4InchWheel;

	// constants
	const int DEPLOY_LIFT_DISTANCE = 10;
	const int DEPLOY_BALL_GRABBER_DISTANCE = 60 * TetrixTicksPerInch4InchWheel;

	bool hasDeployedLift = false;
	bool hasDeployedBallGrabber = false;

	bool bDriveTrain = true;
	bool bLift = true;

	int startTime = nPgmTime;	// timer to limit total time
	ResetDriveTrainEncoderPosition(myRobot.rDriveTrain);	// reset encoder position
	DRIVETRAINMoveToTarget(myRobot.rDriveTrain,START_DRIVE_POWER_RAMP,distTarget);
	while(bDriveTrain || bLift)
	{
		DRIVETRAINTask(myRobot.rDriveTrain,mySensors.rsGyro);
		LIFTTask(myRobot.rLift);
		LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);
		if(nPgmTime >= startTime+MAX_ALLOWED_TIME_RAMP)			// if we exceed timer, do safety shutdown
		{
			DRIVETRAINStop(myRobot.rDriveTrain);	// stop drive train
			return false;	// we took too long
		}
		int driveTrainDist = GetDriveTrainEncoderValue(myRobot.rDriveTrain);

		// drive train logic
		if (!IsDriveTrainMoving(myRobot.rDriveTrain)) {
			bDriveTrain = false;
		}

		// lift logic
		if (driveTrainDist >= DEPLOY_LIFT_DISTANCE && !hasDeployedLift) {
			MoveLift(myRobot.rLift,LiftPos60cm);	// start lift moving
			hasDeployedLift = true;
		}
		if (hasDeployedLift) {
			if (!IsLiftMoving(myRobot.rLift)) {
				bLift = false;
			}
		}
	}
	// at 4 ft, lift lift to 90cm
	// at 5 ft, deploy ball grabber
	return true;	// everything okay if we got this far
}

bool JustDriveOffRamp(ROBOT &myRobot, SENSORS &mySensors)	// juust drives off ramp and deploys ball grabber
{
	bool bOK = DriveOffRamp(myRobot,mySensors);
	if (!bOK) return false;
	// drive another 12" while deploying ball grabber so we are ready to score
	long distTarget = 12 * TetrixTicksPerInch4InchWheel;
	bool bDriveTrain = true;
	int startTime = nPgmTime;	// timer to limit total time
	ResetDriveTrainEncoderPosition(myRobot.rDriveTrain);	// reset encoder position
	DRIVETRAINMoveToTarget(myRobot.rDriveTrain,START_DRIVE_POWER_RAMP,distTarget);
	BallScorerDeployBallGrabber(myRobot.rBallScorer,BallGrabberJawsClosed);	// deploy ball grabber no matter what
	while(bDriveTrain)
	{
		DRIVETRAINTask(myRobot.rDriveTrain,mySensors.rsGyro);
		LIFTTask(myRobot.rLift);
		LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);
		BALLSCORERTask(myRobot.rBallScorer,curLiftState);
		if(nPgmTime >= startTime+MAX_ALLOWED_TIME_RAMP)			// if we exceed timer, do safety shutdown
		{
			DRIVETRAINStop(myRobot.rDriveTrain);	// stop drive train
			return false;	// we took too long
		}
		// drive train logic
		if (!IsDriveTrainMoving(myRobot.rDriveTrain)) {
			bDriveTrain = false;
		}
	}
	DRIVETRAINStop(myRobot.rDriveTrain);	// no matter what, we want the drive train stopped when we exit
	return true;	// okay if we got this far
}

// Moves forward using the ultrasonic to know when to slow down and the EOPD to stop.
//
//
//          |                ULTRA_DISTANCE_SLOW
//          |                |                EOPD_DISTANCE_STOP
//          |                |          J     |          J  .|   |.
//    ___   |       ___J     |       ___|     |       ___|  L|___|J
//   |   |  |      |   | ))  |      |   | ==  |      |   |   |   |
//   |___| -->     |___| )) -->     |___| == -->     |___|   |.-.|
//    O O   |  [V_]/O O      |  [V_]/O O      |  [V_]/O O  __|___|__
// `````````````````````````````````````````````````````

bool approachRollingGoalUsingUltraAndEOPD(ROBOT &myRobot, SENSORS &mySensors) {
	//Constants
#define HARD_LIMIT_DISTANCE_TO_GOAL		36	// we should stop before this
#define TICKS_HARD_LIMIT_TO_GOAL	 		HARD_LIMIT_DISTANCE_TO_GOAL * TetrixTicksPerInch4InchWheel
#define APPROACH_GOAL_POWER_RAMP 							MaximumMotorPowerTetrix * 0.20	// approach goal at reduced power
#define ULTRA_DISTANCE_SLOW_RAMP 							22	// utlrasonic distance (in cm) at which we should slow down
	// move foward at high speed until Ultrasonic <= ULTRA_DISTANCE_SLOW cm
	LIFTSTATE curLiftState = LiftStowed;	// lift state
	int  startTime = nPgmTime;	// timer to limit total time
	bool isBallGrabberDeployed = false;
	SENSORS_ULTRASONICTask(mySensors);	// get an updated distance
	int  ultraDist = ReadUltrasonic(mySensors.rsUltra);
	// Make sure the ultrasonic distance is reasonable (< 150) otherwise we aren't lined up on the center goal
	//	we only have to check this if it isn't orientation 1
#define USDRIVE_MAXDIST_RAMP 150		// maximum distance from center goal we should try and use ultrasonic distance
	ResetDriveTrainEncoderPosition(myRobot.rDriveTrain);
	DRIVETRAINMove(myRobot.rDriveTrain,START_DRIVE_POWER_RAMP);	// start moving
	long driveTrainDist = GetDriveTrainEncoderValue(myRobot.rDriveTrain);

	bool bUltraDist				= true;	// true while ultra distance > ULTRA_DISTANCE_SLOW
	bool bDriveTrainDist	= true;	// true while driveTrainDist < TICKS_HARD_LIMIT_TO_GOAL
	bool bTimer						= true;	// true while       nPgmTime < startTime+MAX_ALLOWED_TIME
	while(bUltraDist && bDriveTrainDist && bTimer)
	{
		LIFTTask(myRobot.rLift);
		curLiftState = GetCurLiftState(myRobot.rLift);
		BALLSCORERTask(myRobot.rBallScorer,curLiftState);
		GOALGRABBERTask(myRobot.rGoalGrabber);
		DRIVETRAINMotorTask(myRobot.rDriveTrain);
		SENSORS_ULTRASONICTask(mySensors);	// get an updated distance
		SENSORS_EOPDForwardTask(mySensors);	// we are going to need the EOPD soon so get that too
		ultraDist = ReadUltrasonic(mySensors.rsUltra);
		driveTrainDist = GetDriveTrainEncoderValue(myRobot.rDriveTrain);
		writeDebugStreamLine("UltraDrive: dist(cm): %d  drivetrain dist(ticks): %d  elapsed: %d",ultraDist,driveTrainDist,nPgmTime-startTime);
		if (ultraDist <= ULTRA_DISTANCE_SLOW_RAMP)
		{
			bUltraDist = false;	// we are close
		}
		else if(driveTrainDist >= TICKS_HARD_LIMIT_TO_GOAL)
		{
			//writeDebugStreamLine("UltraDrive: went too far");
			bDriveTrainDist = false;	// we went too far, stop
		}
		else if(nPgmTime >= startTime+MAX_ALLOWED_TIME_RAMP)
		{
			//writeDebugStreamLine("UltraDrive: took too long");
			bTimer = false;	// we took too long
		}
	}
	//
	// If bUltraDist = false, then it is time to switch to EOPD to finish up, otherwise something went wrong, so quit and return false
	//
	if ((!bDriveTrainDist) || (!bTimer))	// NOTE: If UltraDist = true and either of the others are false, then there is a bug
	{
		DRIVETRAINStop(myRobot.rDriveTrain);	// stop drive train
		LIFTStop(myRobot.rLift);	// stop lift
		return false;	// something went wrong. stop and quit
	}
	// Switch to EOPD.
	// Lift & ball scorer might still be moving, so we need to execute their tasks so they can stop properly
	// And we still want to make sure we don't go more than the maximum distance (bDriveTrainDist) & exceed timer (bTimer)
	//writeDebugStreamLine("approachCenterGoalUsingUltraAndEOPD: switching to EOPD");
	bool bEOPDDist = true;
#define EOPD_DISTANCE_STOP_RAMP	0.8
	DRIVETRAINMove(myRobot.rDriveTrain, APPROACH_GOAL_POWER_RAMP);	// slow down to approach speed
	while(bEOPDDist && bDriveTrainDist && bTimer)	// if lift is done moving, we still have to use EOPD to get to proper distance
	{
		DRIVETRAINMotorTask(myRobot.rDriveTrain);
		LIFTTask(myRobot.rLift);
		SENSORS_EOPDForwardTask(mySensors);	// we are going to need the EOPD soon so get that too
		float eopdDist = EOPDGetDist(mySensors.rsEOPDForward);	// get EOPD distance
		driveTrainDist = GetDriveTrainEncoderValue(myRobot.rDriveTrain);
		writeDebugStreamLine("EOPDDrive: dist: %.2f  drivetrain dist(ticks): %d  timer: %d",eopdDist,driveTrainDist,nPgmTime);

		if (eopdDist <= EOPD_DISTANCE_STOP_RAMP)
		{
			DRIVETRAINStop(myRobot.rDriveTrain);
			bEOPDDist = false;
		}
		else if(driveTrainDist >= TICKS_HARD_LIMIT_TO_GOAL)
		{
			writeDebugStreamLine("EOPDDrive: went too far");
			bDriveTrainDist = false;	// we went too far, stop
		}
		else if(nPgmTime >= startTime+MAX_ALLOWED_TIME_RAMP)
		{
			writeDebugStreamLine("EOPDDrive: took too long");
			bTimer = false;	// we took too long
		}
	}
	//
	//	If bEOPDDist = false, then it is time to score, otherwise something went wrong, so quit and return false
	//
	if ((!bDriveTrainDist) || (!bTimer))
	{
		DRIVETRAINStop(myRobot.rDriveTrain);
		LIFTStop(myRobot.rLift);	// stop lift
		writeDebugStreamLine("Problem after drive train stop");
		return false;	// something went wrong. stop and quit
	}
	DRIVETRAINStop(myRobot.rDriveTrain);	// no matter what, we want the drive train stopped when we exit
	return true;	// everything ok if we got this far
}

//****************************************************
//		NOT USING THIS

#ifdef junk
bool moveGoalOutOfWay(ROBOT &myRobot, SENSORS &mySensors){
	BallScorerStowTube(myRobot.rBallScorer,LiftStowed);
	while(IsTubeMoving(myRobot.rBallScorer.bsTube)){
		BALLSCORERTask(myRobot.rBallScorer,LiftStowed);
	}
	TubeTipperStow(myRobot.rBallScorer.bsTube);	// go back down
	BallScorerStowBallGrabber(myRobot.rBallScorer);
	wait1MSec(500);
	bool bOK = turnSync(myRobot, mySensors, START_DRIVE_POWER_RAMP, -80, MAX_ALLOWED_TIME_RAMP);
	if (!bOK){
		DRIVETRAINStop(myRobot.rDriveTrain);	// stop drive train
		return false;	// something went wrong. stop and quit
	}
	MoveLift(myRobot.rLift,LiftPos90cm);
	bOK = driveSync(myRobot, mySensors, START_DRIVE_POWER_RAMP, 11*TetrixTicksPerInch4InchWheel, MAX_ALLOWED_TIME_RAMP);
	if (!bOK){
		DRIVETRAINStop(myRobot.rDriveTrain);	// stop drive train
		return false;	// something went wrong. stop and quit
	}
	GOALGRABBERSetState(myRobot.rGoalGrabber,GoalGrabberOpen);
	bOK = driveSync(myRobot, mySensors, START_DRIVE_POWER_RAMP, -11*TetrixTicksPerInch4InchWheel, MAX_ALLOWED_TIME_RAMP);
	if (!bOK){
		DRIVETRAINStop(myRobot.rDriveTrain);	// stop drive train
		return false;	// something went wrong. stop and quit
	}
	bOK = turnSync(myRobot, mySensors, START_DRIVE_POWER_RAMP, 80, MAX_ALLOWED_TIME_RAMP);
	if (!bOK){
		DRIVETRAINStop(myRobot.rDriveTrain);	// stop drive train
		return false;	// something went wrong. stop and quit
	}
	BallScorerDeployBallGrabber(myRobot.rBallScorer,BallGrabberJawsClosed);
	return true;
}
#endif
//***************************************************************************

bool FindJust60cmGoalAndScore(ROBOT &myRobot, SENSORS &mySensors) {
	GOALGRABBERSetState(myRobot.rGoalGrabber,GoalGrabberOpen);
	BallScorerDeployBallGrabber(myRobot.rBallScorer,BallGrabberJawsClosed);
	approachRollingGoalUsingUltraAndEOPD(myRobot,mySensors);
	GOALGRABBERSetState(myRobot.rGoalGrabber,GoalGrabberGrab);
	LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);
	GetBallScorerTubeToGoal(myRobot.rBallScorer,curLiftState);
	bool isBallScored = false;
	int startTime = nPgmTime;	// restart timer
	while(!isBallScored){
		BALLSCORERTask(myRobot.rBallScorer,curLiftState);
		isBallScored = (BallScorerTubeToGoal == GetBallScorerState(myRobot.rBallScorer));
		if(nPgmTime >= startTime+MAX_ALLOWED_TIME_RAMP)
		{
			writeDebugStreamLine("scoreBallAndRetreat: took too long");
			return false;
		}
	}
	wait1Msec(BIG_BALL_SCORING_TIME_RAMP);
	BallScorerStowTube(myRobot.rBallScorer,curLiftState);
	return true;
}
bool driveToParkingZone(ROBOT &myRobot, SENSORS &mySensors) {
	ResetDriveTrainEncoderPosition(myRobot.rDriveTrain);
	DRIVETRAINMoveToTarget(myRobot.rDriveTrain, MaximumMotorPowerTetrix, -8 * TetrixTicksPerInch4InchWheel);
	// Keep going until tube is stowed and we are done moving backwards
	LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);
	int startTime = nPgmTime;	// restart timer
	while ((IsDriveTrainMoving(myRobot.rDriveTrain) || !IsBallScorerReadyToScore(myRobot.rBallScorer,false))){	// jaws state not important
		BALLSCORERTask(myRobot.rBallScorer,curLiftState);
		DRIVETRAINTask(myRobot.rDriveTrain,mySensors.rsGyro);
		if(nPgmTime >= startTime+MAX_ALLOWED_TIME_RAMP)
		{
			return false;
		}
	}
	BallScorerStowBallGrabber(myRobot.rBallScorer);
	SetDriveTrainMode(myRobot.rDriveTrain,DriveTrainModeBallGrabbing);
	slalomTurnSync(myRobot,mySensors,SLALOM_INSIDE_POWER_RAMP,SLALOM_OUTSIDE_POWER_RAMP,45,MAX_ALLOWED_TIME_RAMP);
	return true;
}

#ifdef junk
//***********************************************
//		NOT USING THIS

bool FindRollingGoalAndScore(ROBOT &myRobot, SENSORS &mySensors) {
	GOALGRABBERSetState(myRobot.rGoalGrabber,GoalGrabberOpen);
	BallScorerDeployBallGrabber(myRobot.rBallScorer,BallGrabberJawsClosed);
	MoveTube(myRobot.rBallScorer.bsTube,TubeReturnDeployed,LiftStowed);
	approachRollingGoalUsingUltraAndEOPD(myRobot,mySensors);
	GOALGRABBERSetState(myRobot.rGoalGrabber,GoalGrabberGrab);
	TubeTipperTip(myRobot.rBallScorer.bsTube);	// go up
	wait1Msec(BIG_BALL_SCORING_TIME_RAMP);	// give ball time to roll in

	/*moveGoalOutOfWay(myRobot,mySensors);
	approachRollingGoalUsingUltraAndEOPD(myRobot,mySensors);
	GOALGRABBERSetState(myRobot.rGoalGrabber,GoalGrabberGrab);

	LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);
	GetBallScorerTubeToGoal(myRobot.rBallScorer,curLiftState);
	bool isBallScored = false;
	int startTime = nPgmTime;	// restart timer
	while(!isBallScored){
	BALLSCORERTask(myRobot.rBallScorer,curLiftState);
	isBallScored = (BallScorerTubeToGoal == GetBallScorerState(myRobot.rBallScorer));
	if(nPgmTime >= startTime+MAX_ALLOWED_TIME_RAMP)
	{
	writeDebugStreamLine("scoreBallAndRetreat: took too long");
	return false;
	}
	}
	wait1Msec(BALL_SCORING_TIME_RAMP);
	BallScorerStowTube(myRobot.rBallScorer,curLiftState);
	ResetDriveTrainEncoderPosition(myRobot.rDriveTrain);
	DRIVETRAINMoveToTarget(myRobot.rDriveTrain, MaximumMotorPowerTetrix, -48 * TetrixTicksPerInch4InchWheel);
	// Keep going until tube is stowed and we are done moving backwards
	startTime = nPgmTime;	// restart timer
	while ((IsDriveTrainMoving(myRobot.rDriveTrain) || !IsBallScorerReadyToScore(myRobot.rBallScorer,false))){	// jaws state not important
	BALLSCORERTask(myRobot.rBallScorer,curLiftState);
	DRIVETRAINTask(myRobot.rDriveTrain,mySensors.rsGyro);
	if(nPgmTime >= startTime+MAX_ALLOWED_TIME_RAMP)
	{
	return false;
	}
	}*/
	return true;
}
#endif
//********************************************************
