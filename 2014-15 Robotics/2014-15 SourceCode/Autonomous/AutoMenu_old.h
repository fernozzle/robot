// AutoMenu.h
//	Autonomous Menu for this year's challenge

#include "autoMenuFunctions.h"

//
//	Constants
//
#define MaxNbrStrat 10    	// maximum number of sequential strategies

typedef enum
{
	STARTPOS_RAMP,	// starting on the ramp
	STARTPOS_PARK		// starting in the parking area
} STARTPOS;	// starting position

typedef enum
{
	AUTO_NONE = 0,						// this means do nothing
	AUTO_DriveOffRamp,				// this means drive off the ramp
	AUTO_Score30cmGoal,				// score into the 30 cm goal
	AUTO_Score60cmGoal,				// score into the 60 cm goal
	AUTO_Score90cmGoal,				// score into the 90 cm goal
	AUTO_GoalToParkingZone,		// take the goal we scored into to the parking zone
	AUTO_ParkingZoneToCenterGoal, 	// score into the center goal from the parking zone
	AUTO_CenterGoalToKickstand,		// knock over kickstand after scoring to the center goal
	AUTO_KickstandToDefendCenterGoal,	// after knocking over kickstand, defend opposing team center goal
	AUTO_Lift90cmAndPlow		// move the lift to 90 cm and put in plow position. This is a good place to end up so we can score into the 90cm goal right away
} AUTOSTEP;	// Autonomous strategy step

//	Autonomous Strategy structure
typedef struct
{
	TEAM ourTeam;	// which team (TEAM_RED or TEAM_BLUE)
	int autoStartDelay;	// delay (in seconds) before starting autonomous program
	STARTPOS startPos;	// starting position
	int curStep;									// current step
	AUTOSTEP step[MaxNbrStrat];		// sequence of steps
} AUTOSTRATEGY;

void AutoStartOnRamp(AUTOSTRATEGY &myAuto) {	// prompts the user to select autonmous strategy when on ramp
	{
		MENU rampDriveMenu1;
		MENUInit(rampDriveMenu1,"Drive Off Ramp Step 1");
		bool bOK = MENUAddChoice(rampDriveMenu1,"Nothing",(int) AUTO_NONE);	// first choice is do nothing
		bOK = MENUAddChoice(rampDriveMenu1,"DriveOffRamp",(int) AUTO_DriveOffRamp);	// 2nd choice is drive off ramp
		myAuto.step[myAuto.curStep] = (AUTOSTEP) MENUGetChoice(rampDriveMenu1);	// get choice
		//writeDebugStreamLine("Ramp> strategy 1> %d",(int) myAuto.step[myAuto.curStep]);
		myAuto.curStep++;	// get ready for next strategy
	}
	//
	{
		MENU rampDriveMenu2;
		MENUInit(rampDriveMenu2,"Drive Off Ramp Step 2");
		bool bOK = MENUAddChoice(rampDriveMenu2,"Nothing",(int) AUTO_NONE);	// first choice is do nothing
		bOK = MENUAddChoice(rampDriveMenu2,"Lift90cmAndPlow",(int) AUTO_Lift90cmAndPlow);	// 2nd choice is move lift to 90cm
		myAuto.step[myAuto.curStep] = (AUTOSTEP) MENUGetChoice(rampDriveMenu2);	// get choice
		//writeDebugStreamLine("Ramp> strategy 2> %d",(int) myAuto.step[myAuto.curStep]);
		myAuto.curStep++;	// get ready for next strategy
	}
}

void AutoStarParkingZone(AUTOSTRATEGY &myAuto) {	// prompts the user to select autonmous strategy when in parking zone
	{
		MENU parkingZoneMenu1;
		MENUInit(parkingZoneMenu1,"Parking Zone Step 1");
		bool bOK = MENUAddChoice(parkingZoneMenu1,"Nothing",(int) AUTO_NONE);	// first choice is do nothing
		bOK = MENUAddChoice(parkingZoneMenu1,"Score Center Goal",(int) AUTO_ParkingZoneToCenterGoal);	// 2nd choice is score to center goal
		myAuto.step[myAuto.curStep] = (AUTOSTEP) MENUGetChoice(parkingZoneMenu1);	// get choice
		//writeDebugStreamLine("Parking Zone> step 1> %d",(int) myAuto.step[myAuto.curStep]);
		myAuto.curStep++;	// get ready for next strategy
	}
	//	Prompt for second strategy
	{
		MENU parkingZoneMenu2;
		MENUInit(parkingZoneMenu2,"Parking Zone Step 2");
		bool bOK = MENUAddChoice(parkingZoneMenu2,"Nothing",(int) AUTO_NONE);	// first choice is do nothing
		bOK = MENUAddChoice(parkingZoneMenu2,"Knock over kickstand",(int) AUTO_CenterGoalToKickstand);	// 2nd choice is score to knock over the kickstand
		myAuto.step[myAuto.curStep] = (AUTOSTEP) MENUGetChoice(parkingZoneMenu2);	// get choice
		//writeDebugStreamLine("Parking Zone> step 2> %d",(int) myAuto.step[myAuto.curStep]);
		myAuto.curStep++;	// get ready for next strategy
	}
	//	Prompt for thrid strategy
	{
		MENU parkingZoneMenu3;
		MENUInit(parkingZoneMenu3,"Parking Zone Step 3");
		bool bOK = MENUAddChoice(parkingZoneMenu3,"Nothing",(int) AUTO_NONE);	// first choice is do nothing
		bOK = MENUAddChoice(parkingZoneMenu3,"Def Cntr Goal",(int) AUTO_KickstandToDefendCenterGoal);	// 2nd choice is to move to defend opposing team center goal
		myAuto.step[myAuto.curStep] = (AUTOSTEP) MENUGetChoice(parkingZoneMenu3);	// get choice
		//writeDebugStreamLine("Parking Zone> step 3> %d",(int) myAuto.step[myAuto.curStep]);
		myAuto.curStep++;	// get ready for next strategy
	}
}

void AutoMenu(AUTOSTRATEGY &myAuto)		// prompts the user to select autonmous strategy
{
	eraseDisplay();	// erase the display
	//	Common questions
	myAuto.ourTeam = PromptTeam();
	myAuto.autoStartDelay = PromptDelay();
	// Initialize strategy array
	myAuto.curStep = 0;
	for (int stratInx = 0; stratInx < MaxNbrStrat; stratInx++)
	{
		myAuto.step[stratInx] = AUTO_NONE;
	}
	//
	//	Ramp or Parking Area?
	//
	MENU rampParkMenu;
	MENUInit(rampParkMenu,"Park or Ramp?");
	bool bOK = MENUAddChoice(rampParkMenu,"Parking Area",(int) STARTPOS_PARK);	// Parking Area is first choice
	bOK = MENUAddChoice(rampParkMenu,"Ramp",(int) STARTPOS_RAMP);					// Ramp is 2nd choice
	myAuto.startPos = (STARTPOS) MENUGetChoice(rampParkMenu);
	//
	//	Rest of the prompts depend on where we are starting
	//
	if (STARTPOS_RAMP == myAuto.startPos)
	{	// starting on the ramp
		AutoStartOnRamp(myAuto);
	}
	else
	{	// starting in the parking area
		AutoStarParkingZone(myAuto);
	}
	// set up to run 1st strategy
	myAuto.curStep = 0;
	//	erase the display when we are done
	eraseDisplay();
}
