bool turnSync(ROBOT &myRobot, SENSORS &mySensors, int power, float degrees, long maxTime) {
	int startTime = nPgmTime;	// timer to limit total time
	DRIVETRAINTurnToTarget(myRobot.rDriveTrain,mySensors.rsGyro,power,degrees);
	while(IsDriveTrainMoving(myRobot.rDriveTrain))
	{
		DRIVETRAINTask(myRobot.rDriveTrain,mySensors.rsGyro);
		if(nPgmTime >= startTime+maxTime)
		{
			return false;	// we took too long
		}
	}
	return true;	// ok
}
