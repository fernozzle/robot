//  AutoRamp.h
//  Autonomous actions for this year's challenge

#include "automenu.h"

//
// Drive Off Ramp
//
bool ParkingZoneToCenterGoal(ROBOT &myRobot, SENSORS &mySensors){
    //Constants

    const long DISTANCE_FORWARD_TO_CENTER_GOAL_ORIENTATION_1 = 34;
    const long DISTANCE_BACKWARD_FROM_CENTER_GOAL = 12;
    const long DISTANCE_TO_DRIVE_DIAGONAL_KICKSTAND = 13;
    const long DISTANCE_TO_DRIVE_KNOCK_KICKSTAND = 36;
    const int MAX_ALLOWED_TIME = 10000;

    SENSORS_ULTRASONICTask(mySensors);
    int distance = ReadUltrasonic(mySensors.rsUltra);
    /*
    Look at second page of this link to visualize robot's path!
    https://docs.google.com/document/d/1XpcMWYVNnHDfHUvoMaPBxJcYZISRG_xtDsG9CL10XF0/edit
    */
    if(distance >= 115 && distance <= 150){
        //Orientation 3
        driveForwardSimple(myRobot, mySensors, 15, 0.5); // (A)
        turnLeftSync(myRobot,mySensors,45,MaximumMotorPowerTetrix*0.5,-1); // (B)
        driveForwardAndLift(myRobot, mySensors, 50.9, 0.5); // (C)
        turnRightSync(myRobot,mySensors,45,MaximumMotorPowerTetrix*0.5,-1); // (D)
        driveForwardSimple(myRobot, mySensors, 12, 0.5); // (E)
        turnRightSync(myRobot,mySensors,90,MaximumMotorPowerTetrix*0.5,-1); // (F)
        driveForwardSimple(myRobot, mySensors, 7, 0.5); // (G)
        //dumpBallInCenterGoalAndKnockKickstand(myRobot, mySensors);
    }else if(distance >= 150){  //Orientation 2
        driveForwardSimple(myRobot, mySensors, 15, 0.5); // (A)
        turnLeftSync(myRobot,mySensors,45,MaximumMotorPowerTetrix*0.5,-1); // (B)
        driveForwardAndLift(myRobot, mySensors, 33.9, 0.5); // (C)
        turnRightSync(myRobot,mySensors,90,MaximumMotorPowerTetrix*0.5,-1); // (D)
        driveForwardSimple(myRobot, mySensors, 4.9, 0.5); // (E)
        //dumpBallInCenterGoalAndKnockKickstand(myRobot, mySensors);
    }else if(distance >= 90 && distance <= 115){ //Orientation 1
        driveForwardAndLift(myRobot, mySensors, DISTANCE_FORWARD_TO_CENTER_GOAL_ORIENTATION_1,0.5); // (A)
        //dumpBallInCenterGoalAndKnockKickstand(myRobot, mySensors);
    }
    return true;
}
void driveForwardSimple(ROBOT &myRobot, SENSORS &mySensors, float distance, float power){
    DRIVETRAINMoveToTarget(myRobot.rDriveTrain, MaximumMotorPowerTetrix*power, distance* TetrixTicksPerInch4InchWheel);
    int startTime = nPgmTime;
    while (IsDriveTrainMoving(myRobot.rDriveTrain) && nPgmTime < startTime+MAX_ALLOWED_TIME){
        SENSORSTask(mySensors);
        DRIVETRAINTask(myRobot.rDriveTrain);
        LIFTTask(myRobot.rLift);
    }
}
void driveForwardAndLift(ROBOT &myRobot, SENSORS &mySensors, float distance, float power){
    DRIVETRAINMoveToTarget(myRobot.rDriveTrain, MaximumMotorPowerTetrix*power, distance* TetrixTicksPerInch4InchWheel);
    MoveLift(myRobot.rLift,LiftPos120cm);
    GOALGRABBERSetState(myRobot.rGoalGrabber,GoalGrabberPlow);
    int startTime = nPgmTime;
    bool isBallGrabberDeployed = false;
    while ((IsDriveTrainMoving(myRobot.rDriveTrain) || IsLiftMoving(myRobot.rLift)) && nPgmTime < startTime+MAX_ALLOWED_TIME){
        SENSORSTask(mySensors);
        DRIVETRAINTask(myRobot.rDriveTrain);
        GOALGRABBERTask(myRobot.rGoalGrabber);
        LIFTTask(myRobot.rLift);
        LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);
        BALLSCORERTask(myRobot.rBallScorer,curLiftState);
        long currentDriveTrainPosition = GetDriveTrainEncoderValue(myRobot.rDriveTrain);
        if(!isBallGrabberDeployed && currentDriveTrainPosition >= distance* TetrixTicksPerInch4InchWheel *0.5){
            isBallGrabberDeployed = true;
            BallScorerDeployBallGrabber(myRobot.rBallScorer,BallGrabberJawsClosed);
        }
    }
}
void dumpBallInCenterGoalAndKnockKickstand(ROBOT &myRobot, SENSORS &mySensors){
    LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);
    GetBallScorerTubeToGoal(myRobot.rBallScorer,curLiftState);
    bool isBallScored = false;
    while(!isBallScored){
        BALLSCORERTask(myRobot.rBallScorer,curLiftState);
        isBallScored = (BallScorerTubeToGoal == getBallScorerState(myRobot.rBallScorer));
    }
    wait1MSec(250);
    BallScorerStowTube(myRobot.rBallScorer,curLiftState);
    while(!IsBallScorerReadyToScore(myRobot.rBallScorer,false)){
        BALLSCORERTask(myRobot.rBallScorer,curLiftState);
    }
    ResetDriveTrainEncoderPosition(myRobot.rDriveTrain);
    DRIVETRAINMoveToTarget(myRobot.rDriveTrain, -MaximumMotorPowerTetrix*0.5, -DISTANCE_BACKWARD_FROM_CENTER_GOAL * TetrixTicksPerInch4InchWheel);
    MoveLift(myRobot.rLift,LiftStowed);
    startTime = nPgmTime;
    while ((IsDriveTrainMoving(myRobot.rDriveTrain) || IsLiftMoving(myRobot.rLift)) && nPgmTime < startTime+MAX_ALLOWED_TIME){
        SENSORSTask(mySensors);
        DRIVETRAINTask(myRobot.rDriveTrain);
        GOALGRABBERTask(myRobot.rGoalGrabber);
        LIFTTask(myRobot.rLift);
        LIFTSTATE curLiftState = GetCurLiftState(myRobot.rLift);
        BALLSCORERTask(myRobot.rBallScorer,curLiftState);
    }
    turnRightSync(myRobot,mySensors,45,MaximumMotorPowerTetrix*0.5,-1);
    DRIVETRAINMoveToTarget(myRobot.rDriveTrain, MaximumMotorPowerTetrix*0.5, DISTANCE_TO_DRIVE_DIAGONAL_KICKSTAND*TetrixTicksPerInch4InchWheel);
    turnLeftSync(myRobot,mySensors,45,MaximumMotorPowerTetrix*0.5,-1);
    DRIVETRAINMoveToTarget(myRobot.rDriveTrain, MaximumMotorPowerTetrix*1.0, DISTANCE_TO_DRIVE_KNOCK_KICKSTAND*TetrixTicksPerInch4InchWheel);
}
