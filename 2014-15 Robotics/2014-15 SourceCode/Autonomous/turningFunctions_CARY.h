void turnLeftSync(ROBOT &myRobot, SENSORS &mySensors, float degrees, int power, float tightness=-1) { // -1 tightness means turn in place (left motor goes at -1 times the speed of the right motor)  0 tightness means only right motors activates.  0.9 is a very shallow turn
    DRIVETRAINMoveTank(myRobot.rDriveTrain, power*tightness, power);

    GyroReset(mySensors.rsGyro);
    while (GyroGetHeading(mySensors.rsGyro) > -degrees) {
        GyroTask(mySensors.rsGyro);
        DRIVETRAINTask(myRobot.rDriveTrain);
        LIFTTask(myRobot.rLift);
    }

    DRIVETRAINStop(myRobot.rDriveTrain);
}

void turnRightSync(ROBOT &myRobot, SENSORS &mySensors, float degrees, int power, float tightness=-1) { // -1 tightness means turn in place (left motor goes at -1 times the speed of the right motor)  0 tightness means only right motors activates.  0.9 is a very shallow turn
    DRIVETRAINMoveTank(myRobot.rDriveTrain, power, power*tightness);

    GyroReset(mySensors.rsGyro);
    while (GyroGetHeading(mySensors.rsGyro) < degrees) {
        GyroTask(mySensors.rsGyro);
        DRIVETRAINTask(myRobot.rDriveTrain);
        LIFTTask(myRobot.rLift);
    }
    DRIVETRAINStop(myRobot.rDriveTrain);
}
