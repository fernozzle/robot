// AutoCommon.h
//	Common code for autonomous programming
//
#include "..\library\macrodefs.h"

// Get a clean button press
TNxtButtons getButton() {
	while (kNoButton != nNxtButtonPressed) {} // Wait until nothing is pressed
	while (kNoButton == nNxtButtonPressed) {} // Wait until the user presses something

	TNxtButtons button = nNxtButtonPressed; // Remember which button was pressed
	while (kNoButton != nNxtButtonPressed) {} // Wait for them to release it again
	return button;
}

typedef enum {
	TEAM_RED,		// the red team
	TEAM_BLUE		// the blue team
} TEAM;	// which team we are on

// Ask which team we're on
TEAM PromptTeam() {
	TEAM ourTeam = TEAM_RED;	// default to red team
	eraseDisplay(); // Clear the screen
	displayTextLine(0, "Which team?"); // Prompt the user
	displayTextLine(1, "Left = Blue");
	displayTextLine(2, "Right = Red");

	bool done = false;
	while (!done) { // Repeat until they press a valid button (left or right)
		switch (getButton()) {  // Get a button press
		case kRightButton: // Right arrow button
			ourTeam = TEAM_RED;
			done = true;
			break;

		case kLeftButton: // Left arrow button
			ourTeam = TEAM_BLUE;
			done = true;
			break;
		}
	}
	eraseDisplay(); // Clear the screen again
	return(ourTeam);	// return team
}
// Ask how long to wait before starting
int PromptDelay() {
	bool done = false;
	int startDelay = 0;	// default to no delay
	while (!done) { // Repeat until they have chosen a number
		eraseDisplay(); // Clear the screen
		displayTextLine(0, "Delay: %d", startDelay);  // Display the current delay

		switch (getButton()) {  // Get a button
		case kRightButton: // Right arrow button
			startDelay++; // Increase by a second
			break;
		case kLeftButton: // Left arrow button
			startDelay--; // Decrease by a second, but don't go below 0
			if (startDelay < 0) startDelay = 0;
			break;
		case kEnterButton: // Orange square button
			done = true;  // We are done here.
			break;
		}
	}
	eraseDisplay(); // Clear the screen again
	return(startDelay);
}
//
//	Non-standard menus
//

// Constants
//
#define MaxNbrChoices 16
#define NbrDisplayLines 8
#define NbrChrPerLine 17
#define NbrPixelsPerLine 8
#define NbrLinePixels 100

int CalcPixelRow(int lineNbr)	// calculates the pixel row from the line number
{
	int pixelRow = (NbrDisplayLines * NbrPixelsPerLine) - 1;
	pixelRow = pixelRow - (lineNbr * NbrPixelsPerLine);
	return pixelRow;
}

typedef struct
{
	char     titleText[NbrChrPerLine + 1];
	int      nbrChoices;
	char     choiceTexts[MaxNbrChoices][NbrChrPerLine + 1];
	int      choiceVals[MaxNbrChoices];
	int      curChoice;
	int      firstChoice;
	int      userChoice;
} MENU;

void MENUInit(MENU &myMenu, const char *titleText)	// initialize the menu with the title
{
	strcpy(myMenu.titleText, titleText);
	myMenu.nbrChoices = 0;
	myMenu.curChoice = 0;
	myMenu.firstChoice = 0;
	myMenu.userChoice = -1;
	return;
}

bool MENUAddChoice(MENU &myMenu, const char *choiceText, int choiceValue)	// add a choice to the menu
{
	bool bOK = false;	// true if added menu item okay

	if (myMenu.nbrChoices < MaxNbrChoices)
	{
		strcpy(&myMenu.choiceTexts[myMenu.nbrChoices][0], choiceText);
		myMenu.choiceVals[myMenu.nbrChoices] = choiceValue;
		myMenu.nbrChoices++;
		bOK = true;
	}
	else
	{
		playSound(soundBeepBeep);
		writeDebugStreamLine("Too Many menu choices");
	}
	return bOK;
}

void MENUInvertLine(int lineNbr)
{
	for (int lineInx = CalcPixelRow(lineNbr + 1) + 1; lineInx <= CalcPixelRow(lineNbr); lineInx++)
	{
		nxtInvertLine(0, lineInx, (NbrLinePixels - 1), lineInx);
	}
	return;
}

void MENUDisplay(MENU &myMenu)	// display the menu
{
	int lastChoice = MIN(myMenu.firstChoice + NbrDisplayLines - 2,
	myMenu.nbrChoices - 1);
	eraseDisplay();
	displayTextLine(0, myMenu.titleText);
	for (int choice = myMenu.firstChoice; choice <= lastChoice; choice++)
	{
		displayTextLine(choice - myMenu.firstChoice + 1,
		"%d:%s", choice, &myMenu.choiceTexts[choice][0]);
	}
	MENUInvertLine(myMenu.curChoice - myMenu.firstChoice + 1);
	return;
}

void MENUChangeChoice(MENU &myMenu,int change)	// changes the user's choice
{
	myMenu.curChoice += change;
	if (myMenu.curChoice < 0)
	{
		myMenu.curChoice += myMenu.nbrChoices;
	}
	else if (myMenu.curChoice >= myMenu.nbrChoices)
	{
		myMenu.curChoice -= myMenu.nbrChoices;
	}

	int lineNum = myMenu.curChoice - myMenu.firstChoice + 1;
	if (lineNum >= NbrDisplayLines)
	{
		myMenu.firstChoice = myMenu.curChoice - (NbrDisplayLines - 2);
	}
	else if (lineNum < 1)
	{
		myMenu.firstChoice = myMenu.curChoice;
	}
	MENUDisplay(myMenu);
	return;
}

int MENUGetChoice(MENU &myMenu)	// display the menu and get the user's choice - should be called in initializeRobot in Autonomous since it blocks
{
	MENUDisplay(myMenu);	// display menu
	while (true)	// loop until user presses the Enter Button
	{
		TNxtButtons curBtn = getButton();
		if (kLeftButton == curBtn)
		{
			MENUChangeChoice(myMenu, -1);	// decrease choice
		}
		else if (kRightButton == curBtn)
		{
			MENUChangeChoice(myMenu, -1);	// decrease choice
		}
		else if (kEnterButton == curBtn)
		{
			myMenu.userChoice = myMenu.curChoice;	// user made selection
			break;	// return choice
		}
	}
	return myMenu.choiceVals[myMenu.userChoice];
}
