initializing Gyro
gyro>mux calibration
Initializing IR
initializing EOPD Down
initializing Ultrasonic
initialzing Light Sensor
MUX> Setting Light On
initializing EOPD Forward
LiftInit>curPos 0 m: 0
Starting from ramp
Going to start from ramp
MLInt>diff 1200
MOTORMoveToTarget> target: 1200 > current: 0
MOTORMoveToTarget> power: 54 > pos: 1200
Moving lift to 60cm
Lift Moving: target: 1200 cur: 0
Lift Moving: target: 1200 cur: 0
Lift Moving: target: 1200 cur: 1
Lift Moving: target: 1200 cur: 15
Lift Moving: target: 1200 cur: 35
Lift Moving: target: 1200 cur: 45
Lift Moving: target: 1200 cur: 65
Lift Moving: target: 1200 cur: 88
Lift Moving: target: 1200 cur: 103
Lift Moving: target: 1200 cur: 122
Lift Moving: target: 1200 cur: 158
Lift Moving: target: 1200 cur: 174
Lift Moving: target: 1200 cur: 210
Lift Moving: target: 1200 cur: 230
Lift Moving: target: 1200 cur: 256
Lift Moving: target: 1200 cur: 301
Lift Moving: target: 1200 cur: 321
Lift Moving: target: 1200 cur: 343
Lift Moving: target: 1200 cur: 380
Lift Moving: target: 1200 cur: 404
Lift Moving: target: 1200 cur: 456
Lift Moving: target: 1200 cur: 478
Lift Moving: target: 1200 cur: 507
Lift Moving: target: 1200 cur: 550
Lift Moving: target: 1200 cur: 572
Lift Moving: target: 1200 cur: 600
Lift Moving: target: 1200 cur: 647
Lift Moving: target: 1200 cur: 670
Lift Moving: target: 1200 cur: 704
Lift Moving: target: 1200 cur: 751
Lift Moving: target: 1200 cur: 801
Lift Moving: target: 1200 cur: 822
Lift Moving: target: 1200 cur: 872
Lift Moving: target: 1200 cur: 897
Lift Moving: target: 1200 cur: 945
Lift Moving: target: 1200 cur: 979
Lift Moving: target: 1200 cur: 1002
Lift Moving: target: 1200 cur: 1022
Lift Moving: target: 1200 cur: 1068
Lift Moving: target: 1200 cur: 1113
Lift Moving: target: 1200 cur: 1144
Lift Moving: target: 1200 cur: 1169
Stopping DriveTrain
Going to find rolling goal
deploy ball grabber: stowed, check
deploy ball grabber: jaws closed, check
MOTORMoveToTarget> target: -470 > current: 0
MOTORMoveToTarget> power: -20 > pos: -470
BG arm pos: 0
UltraDrive: dist(cm): 62  drivetrain dist(ticks): 11  elapsed: 51
BG arm pos: 0
UltraDrive: dist(cm): 61  drivetrain dist(ticks): 25  elapsed: 86
BG arm pos: -5
UltraDrive: dist(cm): 61  drivetrain dist(ticks): 30  elapsed: 121
BG arm pos: -12
UltraDrive: dist(cm): 61  drivetrain dist(ticks): 58  elapsed: 156
BG arm pos: -15
UltraDrive: dist(cm): 60  drivetrain dist(ticks): 91  elapsed: 193
BG arm pos: -27
UltraDrive: dist(cm): 60  drivetrain dist(ticks): 126  elapsed: 230
BG arm pos: -41
UltraDrive: dist(cm): 61  drivetrain dist(ticks): 176  elapsed: 266
BG arm pos: -66
UltraDrive: dist(cm): 60  drivetrain dist(ticks): 225  elapsed: 299
BG arm pos: -81
UltraDrive: dist(cm): 60  drivetrain dist(ticks): 271  elapsed: 330
BG arm pos: -107
UltraDrive: dist(cm): 58  drivetrain dist(ticks): 345  elapsed: 364
BG arm pos: -139
UltraDrive: dist(cm): 56  drivetrain dist(ticks): 404  elapsed: 402
BG arm pos: -166
UltraDrive: dist(cm): 55  drivetrain dist(ticks): 458  elapsed: 433
BG arm pos: -208
UltraDrive: dist(cm): 55  drivetrain dist(ticks): 538  elapsed: 464
BG arm pos: -252
UltraDrive: dist(cm): 52  drivetrain dist(ticks): 584  elapsed: 497
BG arm pos: -284
UltraDrive: dist(cm): 50  drivetrain dist(ticks): 634  elapsed: 531
BG arm pos: -340
UltraDrive: dist(cm): 50  drivetrain dist(ticks): 700  elapsed: 564
BG arm pos: -375
UltraDrive: dist(cm): 48  drivetrain dist(ticks): 748  elapsed: 597
BG arm pos: -419
UltraDrive: dist(cm): 46  drivetrain dist(ticks): 793  elapsed: 632
BG arm pos: -462
UltraDrive: dist(cm): 46  drivetrain dist(ticks): 850  elapsed: 662
UltraDrive: dist(cm): 45  drivetrain dist(ticks): 891  elapsed: 695
UltraDrive: dist(cm): 43  drivetrain dist(ticks): 945  elapsed: 727
UltraDrive: dist(cm): 43  drivetrain dist(ticks): 986  elapsed: 759
UltraDrive: dist(cm): 42  drivetrain dist(ticks): 1036  elapsed: 792
UltraDrive: dist(cm): 42  drivetrain dist(ticks): 1107  elapsed: 822
UltraDrive: dist(cm): 40  drivetrain dist(ticks): 1150  elapsed: 853
UltraDrive: dist(cm): 38  drivetrain dist(ticks): 1192  elapsed: 884
UltraDrive: dist(cm): 38  drivetrain dist(ticks): 1243  elapsed: 917
UltraDrive: dist(cm): 37  drivetrain dist(ticks): 1296  elapsed: 949
UltraDrive: dist(cm): 41  drivetrain dist(ticks): 1344  elapsed: 980
UltraDrive: dist(cm): 41  drivetrain dist(ticks): 1393  elapsed: 1016
UltraDrive: dist(cm): 34  drivetrain dist(ticks): 1447  elapsed: 1045
UltraDrive: dist(cm): 37  drivetrain dist(ticks): 1485  elapsed: 1075
UltraDrive: dist(cm): 37  drivetrain dist(ticks): 1536  elapsed: 1108
UltraDrive: dist(cm): 32  drivetrain dist(ticks): 1579  elapsed: 1137
UltraDrive: dist(cm): 32  drivetrain dist(ticks): 1621  elapsed: 1169
UltraDrive: dist(cm): 32  drivetrain dist(ticks): 1684  elapsed: 1200
UltraDrive: dist(cm): 31  drivetrain dist(ticks): 1722  elapsed: 1230
UltraDrive: dist(cm): 31  drivetrain dist(ticks): 1773  elapsed: 1263
UltraDrive: dist(cm): 29  drivetrain dist(ticks): 1817  elapsed: 1293
UltraDrive: dist(cm): 27  drivetrain dist(ticks): 1855  elapsed: 1324
UltraDrive: dist(cm): 27  drivetrain dist(ticks): 1900  elapsed: 1355
UltraDrive: dist(cm): 30  drivetrain dist(ticks): 1954  elapsed: 1386
UltraDrive: dist(cm): 29  drivetrain dist(ticks): 2000  elapsed: 1419
UltraDrive: dist(cm): 29  drivetrain dist(ticks): 2034  elapsed: 1449
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 2076  elapsed: 1479
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 2129  elapsed: 1511
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 2171  elapsed: 1544
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 2227  elapsed: 1576
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 2265  elapsed: 1611
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 2333  elapsed: 1642
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 2379  elapsed: 1672
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 2423  elapsed: 1703
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 2473  elapsed: 1735
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 2534  elapsed: 1768
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 2578  elapsed: 1799
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 2621  elapsed: 1831
UltraDrive: dist(cm): 27  drivetrain dist(ticks): 2664  elapsed: 1864
UltraDrive: dist(cm): 27  drivetrain dist(ticks): 2729  elapsed: 1893
UltraDrive: dist(cm): 27  drivetrain dist(ticks): 2781  elapsed: 1926
UltraDrive: dist(cm): 27  drivetrain dist(ticks): 2828  elapsed: 1958
UltraDrive: dist(cm): 27  drivetrain dist(ticks): 2868  elapsed: 1989
UltraDrive: dist(cm): 27  drivetrain dist(ticks): 2918  elapsed: 2020
UltraDrive: dist(cm): 27  drivetrain dist(ticks): 2982  elapsed: 2053
UltraDrive: dist(cm): 27  drivetrain dist(ticks): 3029  elapsed: 2084
UltraDrive: dist(cm): 27  drivetrain dist(ticks): 3077  elapsed: 2116
UltraDrive: dist(cm): 27  drivetrain dist(ticks): 3116  elapsed: 2147
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 3181  elapsed: 2180
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 3225  elapsed: 2212
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 3262  elapsed: 2246
UltraDrive: dist(cm): 27  drivetrain dist(ticks): 3299  elapsed: 2278
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 3364  elapsed: 2310
UltraDrive: dist(cm): 28  drivetrain dist(ticks): 3395  elapsed: 2343
UltraDrive: dist(cm): 27  drivetrain dist(ticks): 3437  elapsed: 2376
UltraDrive: dist(cm): 26  drivetrain dist(ticks): 3484  elapsed: 2407
UltraDrive: dist(cm): 26  drivetrain dist(ticks): 3551  elapsed: 2440
UltraDrive: dist(cm): 26  drivetrain dist(ticks): 3589  elapsed: 2470
UltraDrive: dist(cm): 26  drivetrain dist(ticks): 3634  elapsed: 2501
UltraDrive: dist(cm): 26  drivetrain dist(ticks): 3678  elapsed: 2532
UltraDrive: dist(cm): 25  drivetrain dist(ticks): 1199  elapsed: 2565
UltraDrive: dist(cm): 25  drivetrain dist(ticks): 3788  elapsed: 2595
UltraDrive: dist(cm): 25  drivetrain dist(ticks): 3825  elapsed: 2628
UltraDrive: dist(cm): 25  drivetrain dist(ticks): 3864  elapsed: 2656
UltraDrive: dist(cm): 25  drivetrain dist(ticks): 3908  elapsed: 2689
UltraDrive: dist(cm): 25  drivetrain dist(ticks): 3959  elapsed: 2721
UltraDrive: dist(cm): 24  drivetrain dist(ticks): 4025  elapsed: 2753
UltraDrive: dist(cm): 24  drivetrain dist(ticks): 4060  elapsed: 2785
UltraDrive: dist(cm): 23  drivetrain dist(ticks): 4112  elapsed: 2816
EOPDDrive: dist: 100.00  drivetrain dist(ticks): 4152  timer: 24667
EOPDDrive: went too far
Stopping DriveTrain
Orientation1> Problem after drive train stop
BallScorerMasterTubeToGoal Starting. LiftState: 2
MOTORMoveToTarget> target: 650 > current: 0
MOTORMoveToTarget> power: 24 > pos: 650
TubeReturnStopped> encoder value: 657
TubeToGoal> tube is deployed, time to tip
MOTORMoveToTarget> target: 1830 > current: 657
MOTORMoveToTarget> power: 19 > pos: 1830
TubeReturnStopped> encoder value: 1833
BallScorerTubeToGoal> Balls in Goal!
TubeToStowStarting
MOTORMoveToTarget> target: 0 > current: 1847
MOTORMoveToTarget> power: -30 > pos: 0
TUBETask> Stowing> difference in encoder values: 14
Stowing> Tube Return encoder value: 1847
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 5
TUBETask> Stowing> difference in encoder values: 17
Stowing> Tube Return encoder value: 1830
TUBETask> Stowing> difference in encoder values: 8
TUBETask> Stowing> difference in encoder values: 8
TUBETask> Stowing> difference in encoder values: 16
Stowing> Tube Return encoder value: 1814
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 3
TUBETask> Stowing> difference in encoder values: 3
TUBETask> Stowing> difference in encoder values: 5
TUBETask> Stowing> difference in encoder values: 7
TUBETask> Stowing> difference in encoder values: 10
TUBETask> Stowing> difference in encoder values: 10
TUBETask> Stowing> difference in encoder values: 16
Stowing> Tube Return encoder value: 1798
TUBETask> Stowing> difference in encoder values: 0
: 0
TUBETask> Stowing> difference in encoder values: 18> difference in encoder values: 19
Stowing> Tube Return encoder value: 1650
TUBETask> Stowing> difference in encoder values: 9
TUBETask> Stowing> difference in encoder values: 17
Stowing> Tube Return encoder value: 1633
TUBETask> Stowing> difference in encoder values: 12
Stowing> Tube Return encoder value: 1621
TUBETask> Stowing> difference in encoencoder value: 1541
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 27
Stowing> Tube Return encoder value: 1514
TUBETask> Stowing> difference in encoder values: 0
Stopping DriveTrain
TUBETask> Stowing> difference in encoder values: 13
Stowing> Tube Return encoder value: 1501
TUBETask> Stowing> dif61
TUBETask> Stowing> difference in encoder values: 16
Stowing> Tube Return encoder value: 1445
TUBETask> Stowing> difference in encoder values: 16
Stowing> Tube Return encoder value: 1429
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 29
Stowing> Tube Return encoder value: 1400
TUBETask> Stowinge: 1367
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 36
Stowing> Tube Return encoder value: 1331
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 30
Stowing> Tube Return encoder value: 1301
TUBETask> Stowing> difference in encoder values: 0
TUBin encoder values: 0
TUBETask> Stowing> difference in encoder values: 14
Stowing> Tube Return encoder value: 1262
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 30
Stowing> Tube Return encoder value: 1232
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder ce in encoder values: 15
Stowing> Tube Return encoder value: 1204
TUBETask> Stowing> difference in encoder values: 15
Stowing> Tube Return encoder value: 1189
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 32
Stowing> Tube Return encoder value: 1157
TUBETask> Stowing> difference in encoder valueserence in encoder values: 0
TUBETask> Stowing> difference in encoder values: 16
Stowing> Tube Return encoder value: 1125
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 26
Stowing> Tube Return encoder value: 1099
TUBETask> Stowing> difference in encoder values: 14
Stowing> Tube Return encoder valuifference in encoder values: 17
Stowing> Tube Return encoder value: 1068
TUBETask> Stowing> difference in encoder values: 15
Stowing> Tube Return encoder value: 1053
TUBETask> Stowing> difference in encoder values: 15
Stowing> Tube Return encoder value: 1038
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 30
Stowing> Tube Return encoder value: 1008
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 20
Stowing> Tube Return encoder value: 988
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 31
Stowing> Tube Return encoder value: 957
TUBETask> Seturn encoder value: 941
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 24
Stowing> Tube Return encoder value: 917
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 16
Stowing> Tube Return encoder value: 901
TUBETask> Stowing> difference in encodence in encoder values: 29
Stowing> Tube Return encoder value: 844
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 15
Stowing> Tube Return encoder value: 829
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 35
Stowing> Tube Return encoder value: 79g> difference in encoder values: 27
Stowing> Tube Return encoder value: 737
TUBETask> Stowing>Task> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 28
Stowing> Tube Return encoder value: 513
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 13
Stowing> Tube Return encoder value: 500
TUBETask> Stowing> difference in encoder values: 11
Stowing> Tube Return encoder value: 489
TUBETask> Stowing> difference in encoder values: 11
Stowing> Tube Return encoder value: 478
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 32
Stowing> Tube Return encoder value: 446
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 15
Stowing> Tube Return encoder value: 431
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 28
Stowing> Tube Return encoder value: 403
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 21
Stowing> Tube Return encoder value: 382
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 27
Stowing> Tube Return encoder value: 355
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 13
Stowing> Tube Return encoder value: 342
TUBETask> Stowing> difference in encoder values: 13
Stowing> Tube Return encoder value: 329
TUBETask> Stowing> difference in encoder values: 14
Stowing> Tube Return encoder value: 315
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 29
Stowing> Tube Return encoder value: 286
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 11
Stowing> Tube Return encoder value: 275
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 26
Stowing> Tube Return encoder value: 249
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 21
Stowing> Tube Return encoder value: 228
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 12
Stowing> Tube Return encoder value: 216
TUBETask> Stowing> difference in encoder values: 12
Stowing> Tube Return encoder value: 204
TUBETask> Stowing> difference in encoder values: 12
Stowing> Tube Return encoder value: 192
TUBETask> Stowing> difference in encoder values: 13
Stowing> Tube Return encoder value: 179
TUBETask> Stowing> difference in encoder values: 13
Stowing> Tube Return encoder value: 166
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 26
Stowing> Tube Return encoder value: 140
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 12
Stowing> Tube Return encoder value: 128
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 21
Stowing> Tube Return encoder value: 107
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 28
Stowing> Tube Return encoder value: 79
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 25
Stowing> Tube Return encoder value: 54
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 24
Stowing> Tube Return encoder value: 30
TUBETask> Stowing> difference in encoder values: 0
TUBETask> Stowing> difference in encoder values: 10
TUBETask> Stowing> difference in encoder values: 22
Stowing> Tube Return encoder value: 8
TUBETask> Stowing> difference in encoder values: 6
TUBETask> Stowing> difference in encoder values: 6
TubeReturnStopped> encoder value: -2
TubeStowedArmDeployed> DONE
MOTORMoveToTarget> target: 390 > current: 0
MOTORMoveToTarget> power: 47 > pos: 390
BG arm pos: 0
TURNING LEFT  - Current gyro: 0.00 	Target gyro: 45.00
Stopping DriveTrain
Stopping DriveTrain
